package main

import (
	"context"
	"fmt"
	"io"
	"math/rand"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"testing"
	"time"

	"github.com/go-test/deep"
	"github.com/oklog/ulid/v2"
)

const (
	fmtBaseBackup     = "prefix/%s/basebackup/%s.tar.gz"
	fmtBaseBackupMeta = "prefix/%s/txlog/%s-meta.tar.gz"
	fmtTxLog          = "prefix/%s/txlog/%s.tar.gz"
	testSeed          = 42
)

var (
	now           = time.Date(2022, 1, 30, 1, 45, 0, 0, time.UTC)
	inOneWeek     = now.AddDate(0, 0, 7)
	oneWeekAgo    = now.AddDate(0, 0, -7)
	twoWeeksAgo   = now.AddDate(0, 0, -14)
	threeWeeksAgo = now.AddDate(0, 0, -21)
	oneMonthAgo   = now.AddDate(0, -1, 0)
	twoMonthsAgo  = now.AddDate(0, -2, 0)

	testEntropy = ulid.Monotonic(rand.New(rand.NewSource(testSeed)), 0)

	inOneWeekBackupID  = ulid.MustNew(ulid.Timestamp(inOneWeek), testEntropy).String()
	threeWeeksBackupID = ulid.MustNew(ulid.Timestamp(threeWeeksAgo), testEntropy).String()
	twoWeeksBackupID   = ulid.MustNew(ulid.Timestamp(twoWeeksAgo), testEntropy).String()
	orphanBackupID     = MustULID(twoMonthsAgo)

	// Has two basebackups whose elements overlap in time.
	overlappingBackupList = []*backupItem{
		{
			Key:          fmt.Sprintf(fmtBaseBackup, threeWeeksBackupID, MustULID(threeWeeksAgo)),
			LastModified: threeWeeksAgo,
			Size:         512 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     threeWeeksBackupID,
				tagKeyIsBaseBackup: "true",
				"test sort tag":    "",
			},
		},
		{
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(twoWeeksAgo)),
			LastModified: twoWeeksAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:         threeWeeksBackupID,
				tagKeyIsBaseBackupMeta: "true",
			},
		},
		{
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(oneWeekAgo)),
			LastModified: oneWeekAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID: threeWeeksBackupID,
				tagKeyIsTxLog:  "true",
			},
		},
		{
			Key:          fmt.Sprintf(fmtBaseBackup, twoWeeksBackupID, MustULID(twoWeeksAgo)),
			LastModified: threeWeeksAgo,
			Size:         512 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     twoWeeksBackupID,
				tagKeyIsBaseBackup: "true",
				"test sort tag":    "",
			},
		},
		{
			Key:          fmt.Sprintf(fmtTxLog, twoWeeksBackupID, MustULID(oneWeekAgo)),
			LastModified: twoWeeksAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:         twoWeeksBackupID,
				tagKeyIsBaseBackupMeta: "true",
			},
		},
		{
			Key:          fmt.Sprintf(fmtTxLog, twoWeeksBackupID, MustULID(inOneWeek)),
			LastModified: oneWeekAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID: twoWeeksBackupID,
				tagKeyIsTxLog:  "true",
			},
		},
	}

	// NOTE: Do not change the order of this list.
	stdBackupList = []*backupItem{
		{
			// key = 0
			Key:          fmt.Sprintf(fmtTxLog, orphanBackupID, MustULID(oneMonthAgo)),
			LastModified: oneMonthAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  orphanBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "c",
			},
		},
		{
			// key = 1
			Key:          fmt.Sprintf(fmtBaseBackup, threeWeeksBackupID, MustULID(threeWeeksAgo)),
			LastModified: threeWeeksAgo,
			Size:         512 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     threeWeeksBackupID,
				tagKeyIsBaseBackup: "true",
				"test sort tag":    "",
			},
		},
		{
			// key = 2
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(twoWeeksAgo)),
			LastModified: twoWeeksAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  threeWeeksBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "a",
			},
		},
		{
			// key = 3
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(oneWeekAgo)),
			LastModified: oneWeekAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  threeWeeksBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "e",
			},
		},
		{
			// key = 4
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(now)),
			LastModified: now,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  threeWeeksBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "d",
			},
		},
		{
			// key = 5
			Key:          fmt.Sprintf(fmtBaseBackup, inOneWeekBackupID, MustULID(inOneWeek)),
			LastModified: inOneWeek,
			Size:         256 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     inOneWeekBackupID,
				tagKeyIsBaseBackup: "true",
				"test sort tag":    "b",
			},
		},
	}

	// NOTE: Do not change the order of this list.
	inconsistentBackupList = []*backupItem{
		{
			// key = 0
			Key:          fmt.Sprintf(fmtTxLog, orphanBackupID, MustULID(oneMonthAgo)),
			LastModified: oneMonthAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  orphanBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "c",
			},
		},
		{
			// key = 1
			Key:          fmt.Sprintf(fmtBaseBackup, threeWeeksBackupID, MustULID(threeWeeksAgo)),
			LastModified: threeWeeksAgo,
			Size:         512 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     threeWeeksBackupID,
				tagKeyIsBaseBackup: "true",
				"test sort tag":    "",
			},
		},
		{
			// key = 2
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(twoWeeksAgo)),
			LastModified: twoWeeksAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  threeWeeksBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "a",
			},
		},
		{
			// key = 3
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(oneWeekAgo)),
			LastModified: oneWeekAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  threeWeeksBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "e",
			},
		},
		{
			// key = 4
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(now)),
			LastModified: now,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  threeWeeksBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "d",
			},
		},
		{
			// key = 5
			Key:          fmt.Sprintf(fmtTxLog, twoWeeksBackupID, MustULID(now)),
			LastModified: now,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID: twoWeeksBackupID,
				tagKeyIsTxLog:  "true",
			},
		},
		{
			// key = 6
			Key:          fmt.Sprintf(fmtBaseBackup, inOneWeekBackupID, MustULID(inOneWeek)),
			LastModified: inOneWeek,
			Size:         256 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     inOneWeekBackupID,
				tagKeyIsBaseBackup: "true",
				"test sort tag":    "b",
			},
		},
	}

	// NOTE: Do not change the order of this list.
	missingBackupIDTagBackupList = []*backupItem{
		{
			// key = 0
			Key:          fmt.Sprintf(fmtTxLog, orphanBackupID, MustULID(oneMonthAgo)),
			LastModified: oneMonthAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  orphanBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "c",
			},
		},
		{
			// key = 1
			Key:          fmt.Sprintf(fmtBaseBackup, threeWeeksBackupID, MustULID(threeWeeksAgo)),
			LastModified: threeWeeksAgo,
			Size:         512 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     threeWeeksBackupID,
				tagKeyIsBaseBackup: "true",
				"test sort tag":    "",
			},
		},
		{
			// key = 2
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(twoWeeksAgo)),
			LastModified: twoWeeksAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  threeWeeksBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "a",
			},
		},
		{
			// key = 3
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(oneWeekAgo)),
			LastModified: oneWeekAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyIsTxLog:   "true",
				"test sort tag": "e",
			},
		},
		{
			// key = 4
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(now)),
			LastModified: now,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:  threeWeeksBackupID,
				tagKeyIsTxLog:   "true",
				"test sort tag": "d",
			},
		},
		{
			// key = 5
			Key:          fmt.Sprintf(fmtBaseBackup, inOneWeekBackupID, MustULID(inOneWeek)),
			LastModified: inOneWeek,
			Size:         256 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     inOneWeekBackupID,
				tagKeyIsBaseBackup: "true",
				"test sort tag":    "b",
			},
		},
	}
)

func Test_byULID_Less(t *testing.T) {
	list := make([]*backupItem, len(stdBackupList))
	copy(list, stdBackupList)

	bb := byULID(list)

	for i := 0; i < len(list); i++ {
		for j := 0; j < len(list); j++ {
			if bb.Less(i, j) != (i < j) {
				if i < j {
					t.Errorf("%s not less than %s", bb[i], bb[j])
				} else {
					t.Errorf("%s not greater than %s", bb[i], bb[j])
				}
			}
		}
	}
}

func Test_backupItem_ULID(t *testing.T) {
	type fields struct {
		Key          string
		LastModified time.Time
		Size         int64
		Link         *url.URL
		Daily        bool
		Weekly       bool
		Monthly      bool
		Tags         map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		want    ulid.ULID
		wantErr bool
	}{
		{
			name:    "valid ulid",
			fields:  fields{Key: "01FTRBP24PQVMGJ74KKCGZ8ZFZ"},
			want:    ulid.MustParse("01FTRBP24PQVMGJ74KKCGZ8ZFZ"),
			wantErr: false,
		},
		{
			name:    "valid ulid, real object name",
			fields:  fields{Key: "prefix/01D78XZ44G0000000000000000/basebackup/01FTRBP24PQVMGJ74KKCGZ8ZFZ.tar.gz"},
			want:    ulid.MustParse("01FTRBP24PQVMGJ74KKCGZ8ZFZ"),
			wantErr: false,
		},
		{
			name:    "valid ulid, real object name",
			fields:  fields{Key: "prefix/01D78XZ44G0000000000000000/basebackup/01FTRBP24PQVMGJ74KKCGZ8ZFZ-meta.tar.gz"},
			want:    ulid.MustParse("01FTRBP24PQVMGJ74KKCGZ8ZFZ"),
			wantErr: false,
		},
		{
			name:    "invalid ulid",
			fields:  fields{Key: "prefix/01D78XZ44G0000000000000000/basebackup/01FTRBP24PQVMGJ74KKGZ8ZFZ.tar.gz"},
			want:    [16]byte{},
			wantErr: true,
		},
		{
			name:    "invalid ulid",
			fields:  fields{Key: "prefix/01D78XZ44G0000000000000000/basebackup/01FTRBP24PQVMGJ74KKGZ8ZFZ-meta.tar.gz"},
			want:    [16]byte{},
			wantErr: true,
		},
		{
			name:    "invalid ulid, don't panic",
			fields:  fields{Key: "a"},
			want:    [16]byte{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := backupItem{
				Key:          tt.fields.Key,
				LastModified: tt.fields.LastModified,
				Size:         tt.fields.Size,
				Link:         tt.fields.Link,
				Daily:        tt.fields.Daily,
				Weekly:       tt.fields.Weekly,
				Monthly:      tt.fields.Monthly,
				Tags:         tt.fields.Tags,
			}
			got, err := b.ULID()
			if (err != nil) != tt.wantErr {
				t.Errorf("backupItem.ULID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("backupItem.ULID() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Insert this list of backup items at the position in the std testing list and
// return a copy.
func insertIntoStdList(i int, items ...*backupItem) []*backupItem {
	list := copyBackupItems(stdBackupList)
	// https://github.com/golang/go/wiki/SliceTricks#insertvector
	return append(list[:i], append(items, list[i:]...)...)
}

func Test_byULID_Section(t *testing.T) {
	list := copyBackupItems(stdBackupList)

	i := 2 // Position where we insert the metadata object.

	metat := threeWeeksAgo.Add(time.Second) // Usually the metadata is stored right after the base backup.
	metaList := insertIntoStdList(i, &backupItem{
		Key:          fmt.Sprintf(fmtBaseBackup, threeWeeksBackupID, MustULID(metat)+"-meta"),
		LastModified: metat,
		Size:         2 * Ki,
		Tags: map[string]string{
			tagKeyBackupID:         threeWeeksBackupID,
			tagKeyIsBaseBackupMeta: "true",
		},
	})

	metat2 := inOneWeek.Add(time.Second)
	metaList2 := copyBackupItems(stdBackupList)
	metaList2 = append(metaList2, &backupItem{
		Key:          fmt.Sprintf(fmtBaseBackup, inOneWeekBackupID, MustULID(metat2)+"-meta"),
		LastModified: metat2,
		Size:         2 * Ki,
		Tags: map[string]string{
			tagKeyBackupID:         inOneWeekBackupID,
			tagKeyIsBaseBackupMeta: "true",
		},
	})

	realBase := &backupItem{
		Key:          "myprefix/01GNA263W6PFZ4H1VMR1Z0RMN0/basebackup/01GNA263W6PFZ4H1VMR54ZRJS4.tar.gz",
		LastModified: time.Date(2022, 12, 27, 15, 11, 9, 514, time.UTC),
		Tags: map[string]string{
			"backup_id":     "01GNA263W6PFZ4H1VMR1Z0RMN0",
			"expiry":        "",
			"is_basebackup": "true",
			"namespace":     "development",
			"pod_name":      "mysuperpod-author",
			"variant":       "tx_log_archiving",
		},
	}

	realTxlog := &backupItem{
		Key:          "myprefix/01GNA263W6PFZ4H1VMR1Z0RMN0/txlog/01GNA269JRGMHV13J3V4KAD3GA.tar.gz",
		LastModified: time.Date(2022, 12, 27, 15, 12, 3, 449, time.UTC),
		Tags: map[string]string{
			"backup_id":     "01GNA263W6PFZ4H1VMR1Z0RMN0",
			"expiry":        "",
			"is_basebackup": "false",
			"is_txlog":      "true",
			"namespace":     "development",
			"pod_name":      "mysuperpod-author",
			"variant":       "tx_log_archiving",
		},
	}

	realMeta := &backupItem{
		Key:          "myprefix/01GNA263W6PFZ4H1VMR1Z0RMN0/basebackup/01GNA26AJ0QRVVCJH35TC6VQ9F-meta.tar.gz",
		LastModified: time.Date(2022, 12, 27, 15, 11, 8, 874, time.UTC),
		Tags: map[string]string{
			"backup_id":          "01GNA263W6PFZ4H1VMR1Z0RMN0",
			"expiry":             "",
			"is_basebackup_meta": "true",
			"namespace":          "development",
			"pod_name":           "mysuperpod-author",
			"variant":            "tx_log_archiving",
		},
	}

	// Data where a txlog is between basebackup and meta file. This can happen
	// in real data!
	realData := byULID{
		realBase,
		realTxlog,
		realMeta,
	}

	tests := []struct {
		name    string
		a       byULID
		t       time.Time
		want    []*backupItem
		wantErr bool
	}{
		{
			name:    "exactly base backup",
			a:       byULID(list),
			t:       threeWeeksAgo,
			want:    stdBackupList[1:2],
			wantErr: false,
		},
		{
			name:    "between txlog and txlog",
			a:       byULID(list),
			t:       oneWeekAgo.AddDate(0, 0, 1), // 6 days ago
			want:    stdBackupList[1:5],
			wantErr: false,
		},
		{
			name:    "between txlog and basebackup",
			a:       byULID(list),
			t:       now.AddDate(0, 0, 1), // Between 'now' and 'inOneWeek'
			want:    stdBackupList[1:5],
			wantErr: false,
		},
		{
			name:    "between basebackup and txlog",
			a:       byULID(list),
			t:       threeWeeksAgo.AddDate(0, 0, 1), // Between 'threeWeeksAgo' and 'twoWeeksAgo'
			want:    stdBackupList[1:3],
			wantErr: false,
		},
		{
			name:    "exactly on txlog",
			a:       byULID(list),
			t:       twoWeeksAgo,
			want:    stdBackupList[1:3],
			wantErr: false,
		},
		{
			name:    "orphan txlogs: right before txlog",
			a:       byULID(list),
			t:       oneMonthAgo.AddDate(0, 0, -1),
			want:    nil,
			wantErr: true,
		},
		{
			name:    "orphan txlogs: right on txlog",
			a:       byULID(list),
			t:       oneMonthAgo,
			want:    nil,
			wantErr: true,
		},
		{
			name:    "orphan txlogs: right after txlog",
			a:       byULID(list),
			t:       oneMonthAgo.AddDate(0, 0, 1),
			want:    nil,
			wantErr: true,
		},
		{
			name:    "exactly on meta information",
			a:       byULID(metaList),
			t:       metat,
			want:    metaList[1:4], // We want the basebackup, the meta object and the next txlog.
			wantErr: false,
		},
		{
			name:    "a bit after meta information, before the next txlog",
			a:       byULID(metaList),
			t:       metat.Add(time.Hour),
			want:    metaList[1:4], // We want the basebackup, the meta object and the next txlog.
			wantErr: false,
		},
		{
			name:    "exactly on basebackup",
			a:       byULID(metaList),
			t:       threeWeeksAgo,
			want:    metaList[1:3], // We want the basebackup and the meta object.
			wantErr: false,
		},
		{
			name:    "exactly on basebackup, no txlogs after that",
			a:       byULID(metaList2),
			t:       metat2,
			want:    metaList2[5:7], // We want the basebackup and the meta object.
			wantErr: false,
		},
		{
			name:    "real data - before base backup",
			a:       realData,
			t:       time.Date(2022, 12, 27, 15, 11, 2, 000, time.UTC),
			want:    nil,
			wantErr: true,
		},
		{
			name: "real data - between base backup & txlog",
			a:    realData,
			t:    time.Date(2022, 12, 27, 15, 11, 5, 000, time.UTC),
			want: []*backupItem{
				realBase, realMeta, realTxlog,
			},
			wantErr: false,
		},
		{
			name: "real data - between txlog & meta",
			a:    realData,
			t:    time.Date(2022, 12, 27, 15, 11, 8, 000, time.UTC),
			want: []*backupItem{
				realBase, realTxlog, realMeta,
			},
			wantErr: false,
		},
		{
			name: "real data - after meta",
			a:    realData,
			t:    time.Date(2022, 12, 27, 15, 11, 10, 000, time.UTC),
			want: []*backupItem{
				realBase, realTxlog, realMeta,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.a.Section(tt.t)
			if (err != nil) != tt.wantErr {
				t.Errorf("byULID.Section() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
				printList(os.Stderr, got)
				printList(os.Stderr, tt.want)
			}
			diff := deep.Equal(list, stdBackupList)
			if diff != nil {
				t.Error(diff)
			}
		})
	}
}

func Test_compileSets(t *testing.T) {
	list := copyBackupItems(stdBackupList)

	wantList := copyBackupItems(stdBackupList)
	wantList[0] = wantList[1]
	wantList[1] = wantList[5]
	wantList = wantList[:2]
	wantList[0].Size = sum([]int64{list[1].Size, list[2].Size, list[3].Size, list[4].Size})
	for _, v := range wantList {
		v.Key = path.Dir(path.Dir(v.Key))
	}

	list2 := copyBackupItems(overlappingBackupList)
	wantList2 := copyBackupItems(overlappingBackupList)
	wantList2[1] = wantList2[3]
	wantList2 = wantList2[:2]
	wantList2[0].Size = sum([]int64{list2[0].Size, list2[1].Size, list2[2].Size})
	wantList2[1].Size = sum([]int64{list2[3].Size, list2[4].Size, list2[5].Size})
	for _, v := range wantList2 {
		v.Key = path.Dir(path.Dir(v.Key))
	}

	list3 := copyBackupItems(inconsistentBackupList)

	list4 := copyBackupItems(missingBackupIDTagBackupList)

	type args struct {
		list []*backupItem
	}
	tests := []struct {
		name string
		args args
		want byULID
	}{
		{
			name: "two basebackups, some txlogs",
			args: args{list: list},
			want: wantList,
		},
		{
			name: "two basebackups overlapping in time",
			args: args{list: list2},
			want: wantList2,
		},
		{
			name: "inconsistent txlog",
			args: args{list: list3},
			want: wantList,
		},
		{
			name: "missing backup id tag",
			args: args{list: list4},
			want: wantList,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := compileSets(tt.args.list)
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
				printList(os.Stderr, got)
			}
		})
	}
}

func Test_byNewestULID_Sort(t *testing.T) {
	ulidOldest := "01GJ06ZS78F4AD9F0AM0ZK259S"
	ulidOld := "01GMB40KBNJHSJVB25J5QDSR67"
	ulidNewest := "01GMNJRHX1NT8DZSA3QXHM5XGK"

	metas := []backupMeta{
		{
			ulid:            ulidOldest,
			firstWALSegment: "does not matter",
		},
		{
			ulid:            ulidOld,
			firstWALSegment: "does not matter",
		},
		{
			ulid:            ulidNewest,
			firstWALSegment: "does not matter",
		},
	}

	sort.Sort(byNewestULID(metas))

	want := []backupMeta{
		{
			ulid:            ulidNewest,
			firstWALSegment: "does not matter",
		},
		{
			ulid:            ulidOld,
			firstWALSegment: "does not matter",
		},
		{
			ulid:            ulidOldest,
			firstWALSegment: "does not matter",
		},
	}

	if !reflect.DeepEqual(metas, want) {
		t.Errorf("got = %v, want = %v", metas, want)
	}
}

func Test_backupItemList_Sort(t *testing.T) {
	list1 := backupItemList(copyBackupItems(stdBackupList))
	list2 := backupItemList(copyBackupItems(stdBackupList))
	sort.Sort(sort.Reverse(byULID(list2)))
	list3 := backupItemList(copyBackupItems(stdBackupList))
	list4 := backupItemList(copyBackupItems(stdBackupList))
	list5 := backupItemList(copyBackupItems(stdBackupList))
	list6 := backupItemList(copyBackupItems(stdBackupList))
	type args struct {
		orderBy string
		order   string
	}
	tests := []struct {
		name    string
		l       *backupItemList
		args    args
		wantErr bool
		want    backupItemList
	}{
		{
			name: "sort descending ulid",
			l:    &list1,
			args: args{
				orderBy: "",
				order:   sortOrderDescending,
			},
			wantErr: false,
			want: backupItemList{
				stdBackupList[5],
				stdBackupList[4],
				stdBackupList[3],
				stdBackupList[2],
				stdBackupList[1],
				stdBackupList[0],
			},
		},
		{
			name: "sort ascending ulid",
			l:    &list2,
			args: args{
				orderBy: "",
				order:   sortOrderAscending,
			},
			wantErr: false,
			want: backupItemList{
				stdBackupList[0],
				stdBackupList[1],
				stdBackupList[2],
				stdBackupList[3],
				stdBackupList[4],
				stdBackupList[5],
			},
		},
		{
			name: "empty argument",
			l:    &list3,
			args: args{
				orderBy: "",
				order:   "",
			},
			wantErr: false,
			want: backupItemList{
				stdBackupList[0],
				stdBackupList[1],
				stdBackupList[2],
				stdBackupList[3],
				stdBackupList[4],
				stdBackupList[5],
			},
		},
		{
			name: "unknown argument",
			l:    &list4,
			args: args{
				orderBy: "",
				order:   "blablabla",
			},
			wantErr: true,
			want: backupItemList{
				stdBackupList[0],
				stdBackupList[1],
				stdBackupList[2],
				stdBackupList[3],
				stdBackupList[4],
				stdBackupList[5],
			},
		},
		{
			name: "sort ascending tag",
			l:    &list5,
			args: args{
				orderBy: "test sort tag",
				order:   sortOrderAscending,
			},
			wantErr: false,
			want: backupItemList{
				stdBackupList[2],
				stdBackupList[5],
				stdBackupList[0],
				stdBackupList[4],
				stdBackupList[3],
				stdBackupList[1],
			},
		},
		{
			name: "sort descending tag",
			l:    &list6,
			args: args{
				orderBy: "test sort tag",
				order:   sortOrderDescending,
			},
			wantErr: false,
			want: backupItemList{
				stdBackupList[3],
				stdBackupList[4],
				stdBackupList[0],
				stdBackupList[5],
				stdBackupList[2],
				stdBackupList[1],
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.l.Sort(tt.args.orderBy, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("backupItemList.Sort() error = %v, wantErr %v", err, tt.wantErr)
			}
			if diff := deep.Equal(*tt.l, tt.want); diff != nil {
				t.Error(diff)
				printList(os.Stderr, *tt.l)
			}
		})

	}
}

func Test_byULID_hasBaseBackup(t *testing.T) {
	list := copyBackupItems(stdBackupList)

	i := 2 // Position where we insert the metadata object.

	metat := threeWeeksAgo.Add(time.Second) // Usually the metadata is stored right after the base backup.
	metaList := insertIntoStdList(i, &backupItem{
		Key:          fmt.Sprintf(fmtBaseBackup, threeWeeksBackupID, MustULID(metat)+"-meta"),
		LastModified: metat,
		Size:         2 * Ki,
		Tags: map[string]string{
			tagKeyBackupID:         threeWeeksBackupID,
			tagKeyIsBaseBackupMeta: "true",
		},
	})

	metaListWithoutBackupEntries := copyBackupItems(metaList)
	// Remove basebackup entries from metaList.
	metaListWithoutBackupEntries = append(metaListWithoutBackupEntries[0:1], list[3:]...)
	metaListWithoutBackupEntries = metaListWithoutBackupEntries[:(len(metaListWithoutBackupEntries) - 1)]

	metat2 := inOneWeek.Add(time.Second)
	metaList2 := copyBackupItems(stdBackupList)
	metaList2 = append(metaList2, &backupItem{
		Key:          fmt.Sprintf(fmtBaseBackup, inOneWeekBackupID, MustULID(metat2)+"-meta"),
		LastModified: metat2,
		Size:         2 * Ki,
		Tags: map[string]string{
			tagKeyBackupID:         "wrong backup id",
			tagKeyIsBaseBackupMeta: "true",
		},
	})
	tests := []struct {
		name string
		a    byULID
		want bool
	}{
		{
			name: "has basebackup with meta",
			a:    byULID(metaList),
			want: true,
		},
		{
			name: "has basebackup without meta",
			a:    byULID(list),
			want: false,
		},
		{
			name: "has basebackup with meta, but non matching ids",
			a:    byULID(metaList2),
			want: false,
		},
		{
			name: "missing basebackup",
			a:    byULID(metaListWithoutBackupEntries),
			want: false,
		},
		{
			name: "real data",
			a: byULID{
				{
					Key:          "myprefix/01GNA263W6PFZ4H1VMR1Z0RMN0/basebackup/01GNA263W6PFZ4H1VMR54ZRJS4.tar.gz",
					LastModified: time.Date(2022, 12, 27, 15, 11, 9, 514, time.UTC),
					Tags: map[string]string{
						"backup_id":     "01GNA263W6PFZ4H1VMR1Z0RMN0",
						"expiry":        "",
						"is_basebackup": "true",
						"namespace":     "development",
						"pod_name":      "mysuperpod-author",
						"variant":       "tx_log_archiving",
					},
				},
				{
					Key:          "myprefix/01GNA263W6PFZ4H1VMR1Z0RMN0/txlog/01GNA269JRGMHV13J3V4KAD3GA.tar.gz",
					LastModified: time.Date(2022, 12, 27, 15, 12, 3, 449, time.UTC),
					Tags: map[string]string{
						"backup_id":     "01GNA263W6PFZ4H1VMR1Z0RMN0",
						"expiry":        "",
						"is_basebackup": "false",
						"is_txlog":      "true",
						"namespace":     "development",
						"pod_name":      "mysuperpod-author",
						"variant":       "tx_log_archiving",
					},
				},
				{
					Key:          "myprefix/01GNA263W6PFZ4H1VMR1Z0RMN0/basebackup/01GNA26AJ0QRVVCJH35TC6VQ9F-meta.tar.gz",
					LastModified: time.Date(2022, 12, 27, 15, 11, 8, 874, time.UTC),
					Tags: map[string]string{
						"backup_id":          "01GNA263W6PFZ4H1VMR1Z0RMN0",
						"expiry":             "",
						"is_basebackup_meta": "true",
						"namespace":          "development",
						"pod_name":           "mysuperpod-author",
						"variant":            "tx_log_archiving",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.a.HasBaseBackup(); got != tt.want {
				t.Errorf("byULID.hasBaseBackup() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_backupItemList_mergeName(t *testing.T) {
	list1 := copyBackupItems(stdBackupList)
	list1 = list1[1:] // Make list start with base backup.
	list2 := copyBackupItems(stdBackupList)
	list2 = append(list2[0:1], list2[2:5]...) // Choose only tx logs.

	tests := []struct {
		name string
		l    backupItemList

		wantErr bool
	}{
		{
			name:    "nil list",
			l:       nil,
			wantErr: true,
		},
		{
			name:    "empty list",
			l:       make(backupItemList, 0),
			wantErr: true,
		},
		{
			name:    "first list elem not txlog",
			l:       list1,
			wantErr: true,
		},
		{
			name:    "success",
			l:       list2,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.l.mergeName()
			if (err != nil) != tt.wantErr {
				t.Errorf("backupItemList.mergeName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				return
			}
			if filepath.Dir(got) != filepath.Dir(tt.l[0].Key) {
				t.Errorf("backupItemList.mergeName() must return key that has same filepath directory as key %q, but got %q", tt.l[0].Key, got)
				return
			}
			gotBase := filepath.Base(got)
			if !strings.HasSuffix(gotBase, fileExtensionTarGz) {
				t.Errorf("backupItemList.mergeName() must have suffix %q, but got %q", fileExtensionTarGz, got)
				return
			}
			ulid := strings.TrimSuffix(gotBase, fmt.Sprintf(".%s", fileExtensionTarGz))
			t.Logf("got new ulid %q", ulid)
			_, err = ULIDTime(ulid)
			if err != nil {
				t.Errorf("backupItemList.mergeName() must have filepath base that starts with a ulid, but got %q: %v", got, err)
				return
			}
			if got == tt.l[0].Key {
				t.Errorf("backupItemList.mergeName() must not return key that is identical to key %q of first list element", tt.l[0].Key)
			}
		})
	}
}

func Test_backupItemList_mergeTags(t *testing.T) {
	stdTags := map[string]string{
		tagKeyBackupID: orphanBackupID,
		tagKeyIsTxLog:  "true",
		tagKeyExpiry:   "test expiry",
	}
	list1 := backupItemList{
		{
			Tags: stdTags,
		},
		{
			Tags: stdTags,
		},
	}
	list2 := backupItemList{
		{
			Tags: stdTags,
		},
		{
			Tags: map[string]string{
				tagKeyBackupID:     orphanBackupID,
				tagKeyIsBaseBackup: "true",
				tagKeyExpiry:       "test expiry",
			},
		},
	}
	list3 := backupItemList{
		{
			Tags: stdTags,
		},
		{
			Tags: map[string]string{
				tagKeyBackupID: inOneWeekBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   "test expiry",
			},
		},
	}
	list4 := backupItemList{
		{
			Tags: stdTags,
		},
		{
			Tags: map[string]string{
				tagKeyBackupID: orphanBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   "test expiry 2",
			},
		},
	}

	tests := []struct {
		name    string
		l       backupItemList
		want    map[string]string
		wantErr bool
	}{
		{
			name:    "success",
			l:       list1,
			want:    stdTags,
			wantErr: false,
		},
		{
			name:    "not a tx log",
			l:       list2,
			want:    nil,
			wantErr: true,
		},
		{
			name:    "conflicting backup id",
			l:       list3,
			want:    nil,
			wantErr: true,
		},
		{
			name:    "conflicting expiry",
			l:       list4,
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.l.mergeTags()
			if (err != nil) != tt.wantErr {
				t.Errorf("backupItemList.mergeTags() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("backupItemList.mergeTags() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_backupItemList_storagePrefix(t *testing.T) {
	testStorPrefix := "testStor"
	tests := []struct {
		name    string
		l       *backupItemList
		want    string
		wantErr bool
	}{
		{
			name: "items with storage prefix",
			l: &backupItemList{
				&backupItem{Key: testStorPrefix + storageNameSeparator + "item1"},
				&backupItem{Key: testStorPrefix + storageNameSeparator + "item2"},
				&backupItem{Key: testStorPrefix + storageNameSeparator + "item3"},
			},
			want:    testStorPrefix + storageNameSeparator,
			wantErr: false,
		},
		{
			name: "items without storage prefix",
			l: &backupItemList{
				&backupItem{Key: "item1"},
				&backupItem{Key: "item2"},
				&backupItem{Key: "item3"},
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "items with inconsistent storage prefixes",
			l: &backupItemList{
				&backupItem{Key: testStorPrefix + storageNameSeparator + "item1"},
				&backupItem{Key: "blablabla" + storageNameSeparator + "item2"},
				&backupItem{Key: testStorPrefix + storageNameSeparator + "item3"},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "items with and without storage prefixes",
			l: &backupItemList{
				&backupItem{Key: testStorPrefix + storageNameSeparator + "item1"},
				&backupItem{Key: "item2"},
				&backupItem{Key: testStorPrefix + storageNameSeparator + "item3"},
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.l.storagePrefix()
			if (err != nil) != tt.wantErr {
				t.Errorf("backupItemList.storagePrefix() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("backupItemList.storagePrefix() = %v, want %v", got, tt.want)
			}
		})
	}
}

type getFromTestdata struct{}

func (getFromTestdata) Get(ctx context.Context, objectName string) (io.ReadCloser, error) {
	return os.Open(filepath.Join("testdata", filepath.Base(objectName)))
}

func Test_backupItem_parseBackupMeta(t *testing.T) {
	tests := []struct {
		name    string
		key     string
		getter  Getter
		want    backupMeta
		wantErr bool
	}{
		{
			name:   "success",
			key:    "01GK4M1VA0FBCCKHNNHE9459H5/basebackup/01GK4M22AP297VS40JEE52RD7D-meta.tar.gz",
			getter: getFromTestdata{},
			want: backupMeta{
				ulid:            "01GK4M1VA0FBCCKHNNHE9459H5",
				firstWALSegment: "000000010000000000000057",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &backupItem{
				Key: tt.key,
			}
			got, err := b.parseBackupMeta(context.Background(), tt.getter)
			if (err != nil) != tt.wantErr {
				t.Errorf("backupItem.parseBackupMeta() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("backupItem.parseBackupMeta() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_filterByBackupID(t *testing.T) {
	type args struct {
		list     []*backupItem
		backupID string
	}
	tests := []struct {
		name string
		args args
		want []*backupItem
	}{
		{
			name: "success",
			args: args{
				list: []*backupItem{
					{
						Key: "test",
						Tags: map[string]string{
							"backup_id": "01GHEBCVT9ANXX98RHN3GX5BZF",
						},
					},
				},
				backupID: "01GHEBCVT9ANXX98RHN3GX5BZF",
			},
			want: []*backupItem{
				{
					Key: "test",
					Tags: map[string]string{
						"backup_id": "01GHEBCVT9ANXX98RHN3GX5BZF",
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := filterByBackupID(tt.args.list, tt.args.backupID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("filterByBackupID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUUID(t *testing.T) {
	tests := []struct {
		name      string
		objectKey string
		want      []byte
		wantErr   bool
	}{

		{
			name:      "not uuid",
			objectKey: "test.json",
			want:      nil,
			wantErr:   true,
		},
		{
			name:      "uuid",
			objectKey: "62d52bff-a624-c012-6006-7343902b09f3",
			want: []byte{
				98, 213, 43, 255, 166, 36, 192, 18, 96, 6, 115, 67, 144, 43, 9, 243,
			},
			wantErr: false,
		},
		{
			name:      "uuid and more",
			objectKey: "62d52bff-a624-c012-6006-7343902b09f3_0.part",
			want: []byte{
				98, 213, 43, 255, 166, 36, 192, 18, 96, 6, 115, 67, 144, 43, 9, 243,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UUID(tt.objectKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("UUID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_byTimeInName_Less(t *testing.T) {
	tests := []struct {
		name  string
		items byTimeFromKey
		want  bool
	}{
		{
			name: "0 less than 1",
			items: byTimeFromKey{
				{
					Key: "backups-stage/2023-04-26-055517.gz",
				},
				{
					Key: "backups-stage/2023-05-04-093720.gz",
				},
			},
			want: true,
		},
		{
			name: "0 equal to 1",
			items: byTimeFromKey{
				{
					Key: "backups-stage/2023-04-26-055517.gz",
				},
				{
					Key: "backups-stage/2023-04-26-055517.gz",
				},
			},
			want: false,
		},
		{
			name: "0 greater than 1",
			items: byTimeFromKey{
				{
					Key: "backups-stage/2023-05-04-093720.gz",
				},
				{
					Key: "backups-stage/2023-04-26-055517.gz",
				},
			},
			want: false,
		},
		{
			name: "0 less than 1 with prefix",
			items: byTimeFromKey{
				{
					Key: "backups-stage/stage2023-04-26-055517.gz",
				},
				{
					Key: "backups-stage/stage2023-05-04-093720.gz",
				},
			},
			want: true,
		},
		{
			name: "0 equal to 1 with prefix",
			items: byTimeFromKey{
				{
					Key: "backups-stage/stage2023-04-26-055517.gz",
				},
				{
					Key: "backups-stage/stage2023-04-26-055517.gz",
				},
			},
			want: false,
		},
		{
			name: "0 greater than 1 with prefix",
			items: byTimeFromKey{
				{
					Key: "backups-stage/stage2023-05-04-093720.gz",
				},
				{
					Key: "backups-stage/stage2023-04-26-055517.gz",
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		if len(tt.items) != 2 {
			t.Fatal("there must be exactly two items")
		}
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.items.Less(0, 1); got != tt.want {
				t.Errorf("byTimeInName.Less() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_backupItem_TimeFromKey(t *testing.T) {

	tests := []struct {
		name    string
		Key     string
		want    time.Time
		wantErr bool
	}{
		{
			name:    "plain",
			Key:     "2023-05-04-093720.gz",
			want:    time.Date(2023, time.May, 4, 9, 37, 20, 0, time.UTC),
			wantErr: false,
		},
		{
			name:    "with prefix",
			Key:     "stage2023-05-04-093720.gz",
			want:    time.Date(2023, time.May, 4, 9, 37, 20, 0, time.UTC),
			wantErr: false,
		},
		{
			name:    "with path",
			Key:     "backups-stage/2023-05-04-093720.gz",
			want:    time.Date(2023, time.May, 4, 9, 37, 20, 0, time.UTC),
			wantErr: false,
		},
		{
			name:    "with path and prefix",
			Key:     "backups-stage/stage2023-05-04-093720.gz",
			want:    time.Date(2023, time.May, 4, 9, 37, 20, 0, time.UTC),
			wantErr: false,
		},
		{
			name:    "without file extension",
			Key:     "2023-05-04-093720",
			want:    time.Time{},
			wantErr: true,
		},
		{
			name:    "invalid date",
			Key:     "2023-13-04-093720.gz",
			want:    time.Time{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := backupItem{
				Key: tt.Key,
			}
			got, err := b.TimeFromKey()
			if (err != nil) != tt.wantErr {
				t.Errorf("backupItem.TimeFromKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("backupItem.TimeFromKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
