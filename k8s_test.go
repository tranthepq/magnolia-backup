package main

import "testing"

func Test_podOrdinal(t *testing.T) {
	type args struct {
		hostname string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "ordinal",
			args:    args{hostname: "magnolia-public-0"},
			want:    "0",
			wantErr: false,
		},
		{
			name:    "ordinal",
			args:    args{hostname: "magnolia-public-124"},
			want:    "124",
			wantErr: false,
		},
		{
			name:    "ordinal, number in between",
			args:    args{hostname: "magnolia-0091824-public-124"},
			want:    "124",
			wantErr: false,
		},
		{
			name:    "no number at the end",
			args:    args{hostname: "magnolia-0091824-public"},
			want:    "",
			wantErr: true,
		},
		{
			name:    "no number at all",
			args:    args{hostname: "magnolia-public"},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := podOrdinal(tt.args.hostname)
			if (err != nil) != tt.wantErr {
				t.Errorf("podOrdinal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("podOrdinal() = %v, want %v", got, tt.want)
			}
		})
	}
}
