package main

import (
	"context"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/go-test/deep"
	"github.com/oklog/ulid/v2"
	"golang.org/x/exp/slices"
	"golang.org/x/sync/semaphore"
)

const (
	shortTestString = "hello"
	longTestString  = "hello this is a test string which is way longer than the other one...(wow...such outstanding testing)"
)

func Test_removeEmptyDirs(t *testing.T) {
	// Prepare test directory structure.
	dir, err := os.MkdirTemp("", "example")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir) // Clean up.

	dirs := []string{
		"a/b/c/d",
		"a/b/c2",
		"a/b/c3",
		"a/b/c3/d",
		"a/b/c3/d/e",
		"a/b/c3/d/e/f",
	}
	for _, v := range dirs {
		if err := os.MkdirAll(filepath.Join(dir, v), 0755); err != nil {
			t.Fatal(err)
		}
	}

	defer func() {
		tree := &TreeNode{Payload: "/"}
		if err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				t.Fatal(err)
				return err
			}
			tree.Add(&TreeNode{
				Payload: path,
			})
			return nil
		}); err != nil {
			t.Fatal(err)
		}
		if err := tree.Print(os.Stderr, ""); err != nil {
			t.Fatal(err)
		}
	}()

	// Make a single file in c2.
	file, err := os.Create(filepath.Join(dir, "a/b/testfile.txt"))
	if err != nil {
		t.Fatal(err)
	}
	if _, err := fmt.Fprintf(file, "Oh hai"); err != nil {
		t.Fatal(err)
	}
	file.Close()

	type args struct {
		dir      string
		callback func(dir string)
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "delete empty dirs",
			args: args{
				dir: filepath.Join(dir, "a"),
				callback: func(dir string) {
					t.Logf("removed dir %s", dir)
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := removeEmptyDirs(tt.args.dir, tt.args.callback); (err != nil) != tt.wantErr {
				t.Errorf("removeEmptyDirs() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

type mockUploadMerge struct {
	t             *testing.T
	mergedObjects []string
	failUpload    bool
	failMerge     bool
	storage       map[string]string
	storageMux    sync.Mutex
	numMergeCalls int
}

func (m *mockUploadMerge) Upload(ctx context.Context, objectName string, in io.Reader, tags map[string]string) (int64, error) {
	if m.failUpload {
		return 0, fmt.Errorf("upload failed")
	}
	content, err := io.ReadAll(in)
	if err != nil {
		return 0, err
	}
	m.storageMux.Lock()
	m.storage[objectName] = string(content)
	m.storageMux.Unlock()
	m.t.Logf("upload content %q into object %q with tags %v", content, objectName, tags)
	return int64(len(content)), nil
}

func (m *mockUploadMerge) Merge(ctx context.Context, srcNames []string, targetName string, tags map[string]string) error {
	defer func() {
		m.numMergeCalls = m.numMergeCalls + 1
	}()
	if m.failMerge {
		return fmt.Errorf("merge failed")
	}
	var sb strings.Builder
	for _, src := range srcNames {
		m.storageMux.Lock()
		content, ok := m.storage[src]
		m.storageMux.Unlock()
		if !ok {
			return fmt.Errorf("unknown source object %q", src)
		}
		sb.WriteString(content)
	}
	m.storageMux.Lock()
	m.storage[targetName] = sb.String()
	m.storageMux.Unlock()
	m.t.Logf("merging sources %q into object %q with tags %v", srcNames, targetName, tags)
	m.mergedObjects = append(m.mergedObjects, targetName)
	return nil
}

type mockReader struct {
	at    int
	bytes []byte
}

func (m *mockReader) Read(p []byte) (int, error) {
	n := 0
	for i := range p {
		if m.at+i < len(m.bytes) {
			p[i] = m.bytes[m.at+i]
			n++
		} else {
			return n, io.EOF
		}
	}
	m.at = m.at + n
	return n, nil
}

func Test_divideUploadObject(t *testing.T) {
	type args struct {
		ctx        context.Context
		objectName string
		r          io.Reader
		upMerger   UploadMerger
		tags       map[string]string
		readLimit  int
	}
	tests := []struct {
		name              string
		args              args
		want              int64
		wantUploadErr     bool
		wantMergeErr      bool
		wantMergedObject  string
		wantMergedContent string
	}{
		{
			name: "non positive read limit",
			args: args{
				ctx:       context.Background(),
				readLimit: 0,
				upMerger: &mockUploadMerge{
					t:       t,
					storage: make(map[string]string),
				},
			},
			want:          0,
			wantUploadErr: true,
			wantMergeErr:  false,
		},
		{
			name: "fail upload",
			args: args{
				ctx:        context.Background(),
				objectName: "test",
				r: &mockReader{
					bytes: []byte("hello"),
				},
				upMerger: &mockUploadMerge{
					t:          t,
					storage:    make(map[string]string),
					failUpload: true,
				},
				tags:      make(map[string]string),
				readLimit: 10,
			},
			want:          0,
			wantUploadErr: true,
			wantMergeErr:  false,
		},
		{
			name: "fail merge",
			args: args{
				ctx:        context.Background(),
				objectName: "test",
				r: &mockReader{
					bytes: []byte("hello"),
				},
				upMerger: &mockUploadMerge{
					t:         t,
					storage:   make(map[string]string),
					failMerge: true,
				},
				tags:      make(map[string]string),
				readLimit: 10,
			},
			want:          int64(len([]byte("hello"))),
			wantUploadErr: false,
			wantMergeErr:  true,
		},
		{
			name: "read limit not hit",
			args: args{
				ctx:        context.Background(),
				objectName: "test",
				r: &mockReader{
					bytes: []byte(shortTestString),
				},
				upMerger: &mockUploadMerge{
					t:       t,
					storage: make(map[string]string),
				},
				tags:      make(map[string]string),
				readLimit: 10,
			},
			want:              int64(len([]byte(shortTestString))),
			wantUploadErr:     false,
			wantMergeErr:      false,
			wantMergedObject:  "test",
			wantMergedContent: shortTestString,
		},
		{
			name: "read limit hit",
			args: args{
				ctx:        context.Background(),
				objectName: "test",
				r: &mockReader{
					bytes: []byte(shortTestString),
				},
				upMerger: &mockUploadMerge{
					t:       t,
					storage: make(map[string]string),
				},
				tags:      make(map[string]string),
				readLimit: 2,
			},
			want:              int64(len([]byte(shortTestString))),
			wantUploadErr:     false,
			wantMergeErr:      false,
			wantMergedObject:  "test",
			wantMergedContent: shortTestString,
		},
		{
			name: "read limit hit",
			args: args{
				ctx:        context.Background(),
				objectName: "test",
				r: &mockReader{
					bytes: []byte(longTestString),
				},
				upMerger: &mockUploadMerge{
					t:       t,
					storage: make(map[string]string),
				},
				tags:      make(map[string]string),
				readLimit: 2,
			},
			want:              int64(len([]byte(longTestString))),
			wantUploadErr:     false,
			wantMergeErr:      false,
			wantMergedObject:  "test",
			wantMergedContent: longTestString,
		},
	}
	backoffFunc := func() backoff.BackOff {
		expBack := backoff.NewExponentialBackOff()
		expBack.MaxElapsedTime = 200 * time.Millisecond
		return expBack
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, uploadErr, mergeErr := divideUploadObject(
				tt.args.ctx, tt.args.objectName, tt.args.r, tt.args.upMerger, tt.args.tags, tt.args.readLimit, backoffFunc,
			)
			if (uploadErr != nil) != tt.wantUploadErr {
				t.Errorf("divideUploadObject() uploadErr = %v, wantUploadErr %v", uploadErr, tt.wantUploadErr)
				return
			}
			if (mergeErr != nil) != tt.wantMergeErr {
				t.Errorf("divideUploadObject() mergeErr = %v, wantMergeErr %v", mergeErr, tt.wantMergeErr)
				return
			}
			if got != tt.want {
				t.Errorf("divideUploadObject() = %v, want %v", got, tt.want)
			}
			mock, ok := tt.args.upMerger.(*mockUploadMerge)
			if !ok {
				t.Fatal("up merger must be of type *mockUploadMerge")
			}
			if tt.wantMergedObject != "" && !slices.Contains(mock.mergedObjects, tt.wantMergedObject) {
				t.Errorf("divideUploadObject() did not merge wanted object %q, but %q", tt.wantMergedObject, mock.mergedObjects)
			}
			if mock.storage[tt.wantMergedObject] != tt.wantMergedContent {
				t.Errorf("divideUploadObject() did not merge wanted content %q, but %q", tt.wantMergedContent, mock.storage[tt.wantMergedObject])
			}
			// If the content is actually split into several different parts.
			if tt.args.readLimit > 0 && len(tt.wantMergedContent) > tt.args.readLimit {
				// Compute the maximally expected number of merge calls (is equal to the number of parts.)
				maxNumMergeCalls := ceilFloat64ToInt(t, float64(len(tt.wantMergedContent))/float64(tt.args.readLimit))
				t.Logf("max expected merge calls: %d; actual merge calls: %d", maxNumMergeCalls, mock.numMergeCalls)
				// Check that the maximally expected number of merge calls is
				// not exceeded.
				if mock.numMergeCalls > maxNumMergeCalls {
					t.Errorf(
						"divideUploadObject() did call merge more often than %d time(s): called merge %d time(s)",
						maxNumMergeCalls, mock.numMergeCalls,
					)
				}
			}
		})
	}
}

func ceilFloat64ToInt(t *testing.T, f float64) int {
	f = math.Ceil(f)
	s := fmt.Sprintf("%.0f", f)
	out, err := strconv.Atoi(s)
	if err != nil {
		t.Fatalf("could not convert string to int: %v", err)
	}
	return out
}

func Test_includeInBasebackup(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "pg_wal file",
			args: args{path: "pg_wal/00000100000000FB"},
			want: false,
		},
		{
			name: "pg_stat_tmp file",
			args: args{path: "pg_stat_tmp/testfile.txt"},
			want: false,
		},
		{
			name: "pg_stat_tmp directory",
			args: args{path: "pg_stat_tmp"},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := includeInBasebackup(tt.args.path); got != tt.want {
				t.Errorf("includeInBasebackup() = %v, want %v", got, tt.want)
			}
		})
	}
}

func fileToStringSlice(t *testing.T, fileName string) []string {
	full, err := os.ReadFile(fileName)
	if err != nil {
		t.Fatalf("could not read file %s: %v", fileName, err)
	}
	return strings.Split(string(full), "\n")
}

func Test_includeInBasebackup_on_List(t *testing.T) {
	fullFileName := "./testdata/testfilelist_full.txt"
	filteredFileName := "./testdata/testfilelist_filtered.txt"

	fullList := fileToStringSlice(t, fullFileName)
	filteredList := fileToStringSlice(t, filteredFileName)

	// https://github.com/golang/go/wiki/SliceTricks#filter-in-place
	n := 0
	for _, v := range fullList {
		if includeInBasebackup(v) {
			fullList[n] = v
			n++
		}
	}
	fullList = fullList[:n]

	if diff := deep.Equal(filteredList, fullList); diff != nil {
		t.Error(diff)
	}
}

func mockObjsFromList(list backupItemList) map[string][]byte {
	out := make(map[string][]byte)
	b := byte('a')
	for _, v := range list {
		out[filepath.Base(v.Key)] = append(out[filepath.Base(v.Key)], b, b)
		b++
	}
	return out
}

func mockTagsFromList(list backupItemList) map[string]map[string]string {
	out := make(map[string]map[string]string)
	for _, v := range list {
		out[filepath.Base(v.Key)] = v.Tags
	}
	return out
}

func Test_txlogMerge(t *testing.T) {
	list1 := backupItemList{
		{
			Key:          fmt.Sprintf(fmtTxLog, orphanBackupID, MustULID(oneMonthAgo)),
			Size:         16 * Mi,
			LastModified: oneMonthAgo,
			Tags: map[string]string{
				tagKeyBackupID: orphanBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   "test expiry",
			},
		},
		{
			Key:          fmt.Sprintf(fmtTxLog, orphanBackupID, MustULID(threeWeeksAgo)),
			Size:         4 * Mi,
			LastModified: threeWeeksAgo,
			Tags: map[string]string{
				tagKeyBackupID: orphanBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   "test expiry",
			},
		},
		{
			Key:          fmt.Sprintf(fmtTxLog, orphanBackupID, MustULID(twoWeeksAgo)),
			Size:         4 * Mi,
			LastModified: twoWeeksAgo,
			Tags: map[string]string{
				tagKeyBackupID: orphanBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   "test expiry",
			},
		},
		{
			Key:          fmt.Sprintf(fmtTxLog, orphanBackupID, MustULID(oneWeekAgo)),
			Size:         4 * Mi,
			LastModified: oneWeekAgo,
			Tags: map[string]string{
				tagKeyBackupID: orphanBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   "test expiry",
			},
		},
	}

	type args struct {
		ctx              context.Context
		storage          ObjectStorage
		prefix           string
		curBackupID      string
		readLimit        int
		compressionLevel int
	}
	tests := []struct {
		name        string
		args        args
		wantObjects map[time.Time][]byte
		wantTags    map[time.Time]map[string]string
	}{
		{
			name: "merge objects 2-4",
			args: args{
				ctx: context.Background(),
				storage: &mockStorage{
					listItems: list1,
					objects:   mockObjsFromList(list1),
					tags:      mockTagsFromList(list1),
					deleteKeys: []string{
						list1[1].Key,
						list1[2].Key,
						list1[3].Key,
					},
					compressionLevel: 1,
				},
				prefix:           "prefix",
				curBackupID:      orphanBackupID,
				readLimit:        256 * Mi,
				compressionLevel: 1,
			},
			wantObjects: map[time.Time][]byte{
				oneMonthAgo:   []byte("aa"),
				threeWeeksAgo: []byte("bbccdd"),
			},
			wantTags: map[time.Time]map[string]string{
				oneMonthAgo: {
					tagKeyBackupID: orphanBackupID,
					tagKeyIsTxLog:  "true",
					tagKeyExpiry:   "test expiry",
				},
				threeWeeksAgo: {
					tagKeyBackupID: orphanBackupID,
					tagKeyIsTxLog:  "true",
					tagKeyExpiry:   "test expiry",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := server{
				syncDirSem: semaphore.NewWeighted(1),
			}
			s.txlogMerge(tt.args.ctx, tt.args.storage, tt.args.prefix, tt.args.curBackupID, tt.args.readLimit, tt.args.compressionLevel)
			mock, ok := tt.args.storage.(*mockStorage)
			if !ok {
				t.Errorf("expected storage to be of type *mockStorage, but is %T", tt.args.storage)
				return
			}
			if len(mock.objects) != len(tt.wantObjects) {
				t.Errorf("want %d objects, but got %d", len(tt.wantObjects), len(mock.objects))
				return
			}
			for key, gotValue := range mock.objects {
				ulID := strings.TrimSuffix(key, fmt.Sprintf(".%s", fileExtensionTarGz))
				ulIDObj, err := ulid.Parse(ulID)
				if err != nil {
					t.Fatalf("could not parse ulid string: %v", err)
				}
				gotT := ulid.Time(ulIDObj.Time())
				foundT := false
				for wantT, wantValue := range tt.wantObjects {
					if gotT.Equal(wantT) {
						if foundT {
							t.Fatalf("found gotT %s twice", gotT)
						}
						foundT = true
						if string(gotValue) != string(wantValue) {
							t.Errorf("unequal object contents for object %s: want = %s, got = %s", key, string(wantValue), string(gotValue))
						} else {
							t.Logf("equal object contents for object %s: want = %s, got = %s", key, string(wantValue), string(gotValue))
						}
						break
					}
				}
			}
			for key, gotTags := range mock.tags {
				ulID := strings.TrimSuffix(key, fmt.Sprintf(".%s", fileExtensionTarGz))
				ulIDObj, err := ulid.Parse(ulID)
				if err != nil {
					t.Fatalf("could not parse ulid string: %v", err)
				}
				gotT := ulid.Time(ulIDObj.Time())
				foundT := false
				for wantT, wantTags := range tt.wantTags {
					if gotT.Equal(wantT) {
						if foundT {
							t.Fatalf("found gotT %s twice", gotT)
						}
						foundT = true
						if !reflect.DeepEqual(gotTags, wantTags) {
							t.Errorf("unequal object tags for object %s: want = %v, got = %v", key, wantTags, gotTags)
						} else {
							t.Logf("equal object tags for object %s: want = %v, got = %v", key, wantTags, gotTags)
						}
						break
					}
				}
			}
		})

	}
}

func Test_compileSyncMap(t *testing.T) {
	f1 := finfo{
		path: "000000010000000000000057",
	}
	f2 := finfo{
		path: "00000001000000000000005D",
	}
	f3 := finfo{
		path: "00000001000000A000000099",
	}
	f4 := finfo{
		path: "00000001000000A00000009C",
	}
	type args struct {
		list        []finfo
		backupMetas []backupMeta
	}
	tests := []struct {
		name string
		args args
		want map[backupMeta][]finfo
	}{
		{
			name: "simple",
			args: args{
				list: []finfo{f1, f2, f3},
				backupMetas: []backupMeta{
					{
						ulid:            "whatever",
						firstWALSegment: "00000001000000000000000A",
					},
				},
			},
			want: map[backupMeta][]finfo{
				{
					ulid:            "whatever",
					firstWALSegment: "00000001000000000000000A",
				}: {f1, f2, f3},
			},
		},
		{
			name: "simple",
			args: args{
				list: []finfo{f1, f2, f3},
				backupMetas: []backupMeta{
					{
						ulid:            "whatever",
						firstWALSegment: "00000001000000000000005A",
					},
				},
			},
			want: map[backupMeta][]finfo{
				{
					ulid:            "whatever",
					firstWALSegment: "00000001000000000000005A",
				}: {f2, f3},
			},
		},
		{
			name: "simple",
			args: args{
				list: []finfo{f1, f2, f3},
				backupMetas: []backupMeta{
					{
						ulid:            "whatever",
						firstWALSegment: "000000010000000000000066",
					},
				},
			},
			want: map[backupMeta][]finfo{
				{
					ulid:            "whatever",
					firstWALSegment: "000000010000000000000066",
				}: {f3},
			},
		},
		{
			name: "simple",
			args: args{
				list: []finfo{f1, f2, f3},
				backupMetas: []backupMeta{
					{
						ulid:            "whatever",
						firstWALSegment: "000000011000000000000066",
					},
				},
			},
			want: map[backupMeta][]finfo{},
		},
		{
			name: "split up",
			args: args{
				list: []finfo{f1, f2, f3, f4},
				backupMetas: []backupMeta{
					{
						ulid:            "whatever",
						firstWALSegment: "00000001000000000000000A",
					},
					{
						ulid:            "whatever 2",
						firstWALSegment: "000000010000000000000060",
					},
				},
			},
			want: map[backupMeta][]finfo{
				{
					ulid:            "whatever",
					firstWALSegment: "00000001000000000000000A",
				}: {f1, f2},
				{
					ulid:            "whatever 2",
					firstWALSegment: "000000010000000000000060",
				}: {f3, f4},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := server{
				backupMetas: &backupMetaList{
					list: tt.args.backupMetas,
				},
			}
			if got := s.compileSyncMap(tt.args.list); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("compileSyncMap() = %v, want %v", got, tt.want)
			}
		})
	}
}
