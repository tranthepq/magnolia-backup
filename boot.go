package main

import (
	"archive/tar"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path"
	"sort"
	"strconv"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/klauspost/compress/gzip"
	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/magnolia-backup/api"
	"gitlab.com/mironet/magnolia-backup/archive"
)

// bootProcess holds the current state of the boot process.
type bootProcess struct {
	Err error // Last error occurred.

	ctx            context.Context // If we cancel this, we're done.
	hostname       string          // Hostname override if not empty.
	cli            *api.Client     // Client used to sync from other instances.
	markerFilePath string          // Path to marker file used to check if we need to bootstrap or not.
	serviceName    string          // K8s service name.
	dataDir        string          // Target data dir in case we sync content from another public node.
	dataSourceURL  *url.URL        // URL to metadata file to fetch remote (backup) data for restoration.
	fileName       string
	endpoint       string
}

func (b bootProcess) Error() string {
	return b.Err.Error()
}

// Part of our FSM.
type bootStateFn func(*bootProcess) bootStateFn

func stateInit(b *bootProcess) bootStateFn {
	// First let's check if our pod ordinal number (if we are a StatefulSet) is > 0.
	if b.hostname == "" {
		hostname, err := os.Hostname()
		if err != nil {
			b.Err = err
			return nil
		}
		b.hostname = hostname
	}
	o, err := podOrdinal(b.hostname)
	if err != nil {
		b.Err = err
		return nil
	}
	ordinal, err := strconv.Atoi(o)
	if err != nil {
		b.Err = err
		return nil
	}

	if ordinal < 1 && b.dataSourceURL == nil {
		// We are the first pod in the StatefulSet. Since we probably have no
		// other public instance to sync from and no static data source, let's
		// just start normally.
		logrus.Infof("we are the first pod in the deployment, not syncing anything ...")
		return nil
	}

	// Check if this is a fresh boot or just a restart.
	_, err = os.Stat(b.markerFilePath)
	switch {
	case err != nil && os.IsNotExist(err):
		// The marker file does not exist. Now check if the target directory is
		// empty.
		empty, err := isEmpty(b.dataDir)
		if err != nil && os.IsNotExist(err) {
			// Target data dir does not exist, create dir recursively and
			// continue.
			if err := os.MkdirAll(b.dataDir, 0700); err != nil {
				b.Err = fmt.Errorf("could not create data dir: %w", err)
				return nil
			}
			return stateFetchBackup
		}
		if err != nil {
			b.Err = fmt.Errorf("error while checking if target dir is empty: %w", err)
			return nil
		}
		if !empty {
			// We are done here as a safety measure.
			logrus.Warnf("target dir %s not empty, not syncing", b.dataDir)
			return nil
		}
		// In this case we start syncing from any good public instance we can
		// find.
		return stateFetchBackup
	case err != nil:
		// Another error happened.
		b.Err = fmt.Errorf("could not stat marker file: %w", err)
		return nil
	default:
		// Normal boot, nothing to do.
		logrus.Infof("marker file %s present, not syncing", b.markerFilePath)
		return nil
	}
}

// Decide where to fetch the backup from.
func stateFetchBackup(b *bootProcess) bootStateFn {
	if b.dataSourceURL == nil {
		// Try to get the backup from another instance via k8s Service
		// definitions.
		return stateFetchBackupK8s(b)
	}
	return stateFetchBackupPITR
}

// Fetch backups from point in time recovery metadata.
func stateFetchBackupPITR(b *bootProcess) bootStateFn {
	// Fetch the metadata file.
	cli := &http.Client{
		Timeout: 0, // Allow for a long time for the download.
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout:   180 * time.Second,
				KeepAlive: 180 * time.Second,
			}).Dial,
			TLSHandshakeTimeout:   30 * time.Second,
			ResponseHeaderTimeout: 30 * time.Second,
			ExpectContinueTimeout: time.Second,
		},
	}
	logrus.Infof("fetching point in time recovery metadata ...")
	req, err := http.NewRequestWithContext(b.ctx, http.MethodGet, b.dataSourceURL.String(), nil)
	if err != nil {
		b.Err = err
		return nil
	}
	var meta PointInTimeRestore
	resp, err := cli.Do(req)
	if err != nil {
		b.Err = fmt.Errorf("error requesting pitr metadata: %w", err)
		return nil
	}
	defer resp.Body.Close()
	if resp.StatusCode < 200 || resp.StatusCode > 399 {
		res, err := httputil.DumpResponse(resp, true)
		if err != nil {
			b.Err = fmt.Errorf("error while dumping response: %w", err)
			return nil
		}
		b.Err = fmt.Errorf("error requesting pitr metadata, dumping response: \n%s", string(res))
		return nil
	}
	if err := json.NewDecoder(resp.Body).Decode(&meta); err != nil {
		b.Err = fmt.Errorf("error parsing pitr metadata: %w", err)
		return nil
	}

	// Now loop through the list and restore to the desired point in time.
	r, w := io.Pipe()
	go func() {
		defer w.Close()
		gzw := gzip.NewWriter(w)
		defer gzw.Close()
		tw := tar.NewWriter(gzw)
		defer tw.Close()

		// Copy each file in the list, extract its contents and forward
		logrus.Infof("downloading backup bundle for point in time %s ...", meta.PointInTime)
		for _, v := range meta.BackupList {
			if err := func() error {
				req, err := http.NewRequestWithContext(b.ctx, http.MethodGet, v.Link.String(), nil)
				if err != nil {
					return err
				}
				resp, err := cli.Do(req)
				if err != nil {
					return err
				}
				defer resp.Body.Close()
				gz, err := gzip.NewReader(resp.Body)
				if err != nil {
					return err
				}
				defer gz.Close()
				tr := tar.NewReader(gz)
				for {
					hdr, err := tr.Next()
					if err == io.EOF {
						break // End of archive.
					}
					if err != nil {
						return err
					}
					if istxlog := v.Tags[tagKeyIsTxLog]; istxlog == "true" {
						// Fixup the path so we make sure the tx log files do not
						// land in the root directory.
						hdr.Name = path.Join("archive", hdr.Name)
					}
					if err := tw.WriteHeader(hdr); err != nil {
						return err
					}
					if _, err := io.Copy(tw, tr); err != nil {
						return err
					}
				}
				return nil
			}(); err != nil {
				w.CloseWithError(err)
				return
			}
		}

		// Now add the recovery file if a point in time is requested while
		// pulling through the tar file.
		logrus.Infof("generating recovery.conf file ...")
		formattedTime := meta.PointInTime.Format(pointInTimeFormat)
		templ := template.Must(template.New("restore").Parse(recoveryTempl))
		data := struct {
			FormattedTime string
		}{
			FormattedTime: formattedTime,
		}
		var recoveryFile bytes.Buffer
		if err := templ.Execute(&recoveryFile, data); err != nil {
			w.CloseWithError(err)
			return
		}
		hdr := &tar.Header{
			Name:    "recovery.conf",
			Mode:    0600,
			Size:    int64(recoveryFile.Len()),
			ModTime: time.Now(),
		}
		if err := tw.WriteHeader(hdr); err != nil {
			w.CloseWithError(err)
			return
		}
		if _, err := io.Copy(tw, &recoveryFile); err != nil {
			w.CloseWithError(err)
			return
		}

		// Done.
	}()
	return stateExtractBackup(r)
}

// The time format we want to recover to, should be UTC.
const pointInTimeFormat = "2006-01-02 15:04:05"

// Template for the recovery.conf file used by PITR.
const recoveryTempl = `restore_command = 'cp archive/%f "%p"'
recovery_target_time = '{{ .FormattedTime }}'
recovery_target_action = 'promote'
`

// Fetch a backup inside a k8s environment, from another instance in this case.
func stateFetchBackupK8s(b *bootProcess) bootStateFn {
	hosts, err := discoverEndpoints(b.ctx, b.serviceName)
	if err != nil {
		b.Err = err
		return nil
	}
	logrus.Debugf("discovered endpoints: %s", strings.Join(hosts, ", "))
	b.endpoint, err = findQuickestEndpoint(b.ctx, hosts, b.cli, "/hello")
	if err != nil {
		b.Err = err
		return nil
	}
	// Try to get a fresh base backup.
	path := fmt.Sprintf("https://%s/download", b.endpoint)
	req, err := b.cli.NewRequest(b.ctx, http.MethodGet, path, nil)
	if err != nil {
		b.Err = err
		return nil
	}
	r, w := io.Pipe()

	go func() {
		defer w.Close()
		logrus.Infof("requesting content from %s", path)
		resp, err := b.cli.Do(b.ctx, req, w)
		if err != nil {
			w.CloseWithError(err)
			return
		}
		b.fileName, b.Err = fileName(resp.Header)
	}()
	return stateExtractBackup(r)
}

func stateFetchBackupMetaK8s(b *bootProcess) bootStateFn {
	return func(b *bootProcess) bootStateFn {
		// Try to get a fresh base backup.
		path := fmt.Sprintf("https://%s/download", b.endpoint)
		req, err := b.cli.NewRequest(b.ctx, http.MethodGet, path, nil)
		// Add query to path.
		if b.fileName == "" {
			return nil // Stop here because file name cannot be empty at this point.
		}
		query := make(url.Values)
		query.Add("meta", backupIDFromFileName(b.fileName))
		req.URL.RawQuery = query.Encode()

		if err != nil {
			b.Err = err
			return nil
		}
		r, w := io.Pipe()
		go func() {
			defer w.Close()
			logrus.Infof("requesting content from %s", path)
			_, err := b.cli.Do(b.ctx, req, w)
			if err != nil {
				w.CloseWithError(err)
			}
		}()
		return stateExtractBackupMeta(r)
	}
}

func stateExtractBackup(r io.Reader) bootStateFn {
	return func(b *bootProcess) bootStateFn {
		// Extract the source into the data dir.
		startTime := time.Now()
		pr := newProgressReader(r, func(n int64) {
			rate := rateString(time.Since(startTime), n)
			logrus.Infof("synced %s (%s) ...", ByteCountBinary(n), rate)
		})
		if err := archive.Untar(pr, b.dataDir); err != nil {
			b.Err = err
			return nil
		}
		// Create (touch) the marker file.
		_, err := os.Stat(b.markerFilePath)
		switch {
		case err != nil && os.IsNotExist(err):
			if _, err := os.Create(b.markerFilePath); err != nil {
				b.Err = fmt.Errorf("could not create marker file: %w", err)
				return nil
			}
			logrus.Infof("created marker file %s", b.markerFilePath)
		case err != nil:
			// Another error happened while stat-ing.
			b.Err = fmt.Errorf("could not stat marker file: %w", err)
			return nil
		default:
			logrus.Infof("marker file %s present after syncing, not touching", b.markerFilePath)
		}
		return stateFetchBackupMetaK8s(b)
	}
}

func stateExtractBackupMeta(r io.Reader) bootStateFn {
	return func(b *bootProcess) bootStateFn {
		// Extract the source into the data dir.
		startTime := time.Now()
		pr := newProgressReader(r, func(n int64) {
			rate := rateString(time.Since(startTime), n)
			logrus.Infof("synced %s (%s) ...", ByteCountBinary(n), rate)
		})
		if err := archive.Untar(pr, b.dataDir); err != nil {
			b.Err = err
		}
		return nil // We're done here.
	}
}

var (
	errNoResponse = fmt.Errorf("no response from any target")
)

// findQuickestEndpoint issues GET requests to a target with the client a few
// times and reports back the addr with the fastest response time. It
// automatically assumes https:// for the scheme and expects addresses in the
// form of 'host:port' or just 'host'.
func findQuickestEndpoint(ctx context.Context, addrs []string, cli *api.Client, target string) (string, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	var max = 10
	var wg sync.WaitGroup

	work := make(chan string)
	results := make(chan string)
	errc := make(chan error, max)

	for i := 0; i < max; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			cli, err := cli.Clone()
			if err != nil {
				errc <- err
				return
			}
			for addr := range work {
				logrus.Debugf("checking if %s is responding", addr)
				req, err := cli.NewRequest(ctx, http.MethodGet, target, nil, api.WithBaseURL("https://"+addr))
				if err != nil {
					errc <- err
					return
				}
				if _, err := cli.Do(ctx, req, nil); err != nil {
					// We just ignore this since there are potentially other
					// requests which are successful.
					logrus.Debugf("error while checking response time of endpoint %s: %s", addr, err)
					continue
				}

				select {
				case results <- addr:
				case <-ctx.Done():
					return
				}
			}
		}()
	}

	go func() {
		defer close(work)
		for _, v := range addrs {
			select {
			case work <- v:
			case <-ctx.Done():
				errc <- ctx.Err()
				return
			}
		}
	}()

	go func() {
		// We do not close results ever, but do close errc. This signals no
		// target has been contacted successfully.
		defer close(errc)
		wg.Wait()
	}()

	select {
	case result := <-results:
		return result, nil
	case err := <-errc:
		if err != nil {
			return "", err
		}
		return "", errNoResponse
	}
}

// discoverEndpoints expects a k8s headless service name and tries to discover
// the DNS names of all pods in that service for a specific (DNS) service name.
func discoverEndpoints(ctx context.Context, serviceName string) ([]string, error) {
	return discoverEndpointsWithResolver(ctx, serviceName, net.DefaultResolver)
}

const (
	contentSyncDNSServiceName  = "contentsync"
	contentSyncDNSServiceProto = "tcp"
)

type srvResolver interface {
	LookupSRV(ctx context.Context, service, proto, name string) (cname string, addrs []*net.SRV, err error)
}

func discoverEndpointsWithResolver(ctx context.Context, serviceName string, resolver srvResolver) ([]string, error) {
	var out []string
	// First find all A records of the service name.
	_, addrs, err := resolver.LookupSRV(ctx, contentSyncDNSServiceName, contentSyncDNSServiceProto, serviceName)
	if err != nil {
		return nil, err
	}
	for _, v := range addrs {
		name := fmt.Sprintf("%s:%d", strings.TrimSuffix(v.Target, "."), v.Port)
		out = append(out, name)
	}
	sort.Strings(out)
	return out, nil
}
