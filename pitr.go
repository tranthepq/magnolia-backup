package main

import "time"

// PointInTimeRestore describes a restored with a desired point in time and the
// sources of the backups necessary to perform the restore.
type PointInTimeRestore struct {
	// List of backup files to download and extract.
	BackupList []*backupItem `json:"backup_list,omitempty"`
	// Desired point in time to restore to. If zero value, all tx logs will be
	// restored.
	PointInTime time.Time `json:"point_in_time,omitempty"`
}
