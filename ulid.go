package main

import (
	"crypto/rand"
	"fmt"
	"io"
	"path"
	"sync"
	"time"

	"github.com/oklog/ulid/v2"
	"gitlab.com/mironet/magnolia-backup/protected"
)

var (
	currentBackupID = protected.NewString("")
)

func newTxLogName(prefix, backupID string, t time.Time) string {
	return path.Join(prefix, backupID, txlogSuffix, MustULID(t))
}

func newBackupName(prefix string, suffix string) (string, error) {
	cur := currentBackupID.Get()
	if cur == "" {
		return "", fmt.Errorf("current backup id is empty")
	}
	return path.Join(prefix, cur, suffix, MustNewULID()), nil
}

var (
	ulidEntropy io.Reader = rand.Reader
	once        sync.Once
	// https://github.com/oklog/ulid#usage <-- We serialize access to the
	// entropy source even if not "really" needed.
	ulidMux sync.Mutex
)

func entropySource() io.Reader {
	once.Do(func() {
		ulidEntropy = ulid.Monotonic(ulidEntropy, 0)
	})
	return ulidEntropy
}

func MustNewULID() string {
	ulidMux.Lock()
	defer ulidMux.Unlock()
	id, err := ulid.New(ulid.Timestamp(time.Now()), entropySource())
	if err != nil {
		panic(err)
	}
	return id.String()
}

func MustULID(t time.Time) string {
	ulidMux.Lock()
	defer ulidMux.Unlock()
	id, err := ulid.New(ulid.Timestamp(t), entropySource())
	if err != nil {
		panic(err)
	}
	return id.String()
}

// ULIDTime converts a ulid string into its time.Time.
func ULIDTime(in string) (time.Time, error) {
	id, err := ulid.ParseStrict(in)
	if err != nil {
		return time.Time{}, err
	}
	return ulid.Time(id.Time()), nil
}

// ObjectNameToTimeRFC3339 converts an object name into a RFC3339 formated time
// string by considering the ulid string contained in the object path's base.
// Returns the zero time, if an error occurs.
func ObjectNameToTimeRFC3339(objectName string) string {
	id, err := ULID(objectName)
	if err != nil {
		// Object name is not ulid. Check whether it's a non ulid time.
		t, err2 := TimeFromKey(objectName)
		if err2 != nil {
			return fmt.Sprintf("cannot extract time from object key: not ulid (%s), not non-ulid (%s)", err, err2)
		}
		return t.Format(time.RFC3339)
	}
	return ulid.Time(id.Time()).Format(time.RFC3339)
}
