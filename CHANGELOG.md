# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [v0.8.4] - 2023-06-07

### Fixed

- Keep bundles indefinitely if keepdays is set to 0.

## [v0.8.3] - 2023-05-24

### Fixed

- Zero time displayed for non ULID object keys in html list output.

## [v0.8.2] - 2023-05-04

### Fixed

- ULID backup name only required in wal mode.
- Must list backup items without tag "variant=tx_log_archiving" (if not in pg_wal mode).
- SimpleLifecycle.Sweep().

## [v0.8.0] - 2023-02-14

### Added

- Azure Blob Storage support.

## [v0.7.4] - 2023-01-25

### Fixed

- Use ComposeObject instead of CopyObject in Tag method of s3 storage.

## [v0.7.3] - 2023-01-19

### Fixed

- Fix: Only list storage objects that have the tag `variant=tx_log_archiving`.

## [v0.7.2] - 2023-01-19

### Fixed

- Don't need to initialize & use backup metas list in read only mode.

## [v0.7.1] - 2023-01-19

### Fixed

- Do not enforce prefix to be set in s3 multi-source storage config.

## [v0.7.0] - 2023-01-19

### Changed

- Use metadata instead of tags in s3 object storage. Use fallback to object
  tagging if the tags could not be fetched from metadata.

### Fixed

- Actually drop opOnDumpErr struct.
- Move dump ports to the correct container in docker compose file.

## [v0.6.2] - 2023-01-17

### Added

- Flag to accept any TLS certificate if secure=true.

## [v0.6.1] - 2023-01-17

### Added

- Documentation on how to test download endpoint.

### Changed

- Simplify getNewDump() call.
- Make retrying of dump on failure a default.
- Remove errNothingToSync (was never thrown, only checked).
- Regenerate mTLS test certs.

### Fixed

- [Issue-20](https://gitlab.com/mironet/magnolia-backup/-/issues/20) Use (&
  cancel) a separate context for file syncs.
- Allow omitting pod_name & namespace environment variables in read-only mode.

## [v0.6.0] - 2023-01-10

### Added

- Add list options to the list command of the storage interface.
- Implement withSuffix & withTags list options in gcs & s3 storages.
- Add parts sweeping routine for s3 storages.

### Changed

- [Issue-17](https://gitlab.com/mironet/magnolia-backup/-/issues/17) Don't use
  fsnotify to sync tx logs. Repeatedly (every 31 seconds) run a routine that
  syncs all pg wal segments in the archive.
- Use metadata files stored in the object storage to assign the wal segments to
  the correct base backup. Only list backup meta files once on server start.
  Afterwards keep book on the backup meta data by adding a meta data entry
  whenever a new basebackup is created.
- Rename parts objects. Such that they have a different prefix than the other
  objects of the same instance. This way we omit the parts "for free" when
  listing objects.
- Use weighted semaphores instead of struct channels to lock/unlock.
- Sweep at most every defined interval.

### Fixed

- Allow only one txlog merge at a time.
- HasBaseBackup() check.
- Ignore objects without ulid key in Section() method. But log a warning if such
  an object is encountered.
- Must retry if dump in cronjob fails.
- Do not refresh the cache every minute.
