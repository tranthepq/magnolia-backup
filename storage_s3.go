package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/hashicorp/go-uuid"
	"github.com/minio/minio-go/v7"
	miniotags "github.com/minio/minio-go/v7/pkg/tags"

	"github.com/oklog/ulid/v2"
	"github.com/sirupsen/logrus"
)

var (
	defaultRegion = "us-east-1" // Default S3 "region".
)

type s3StorConfig struct {
	Endpoint           string `yaml:"endpoint,omitempty"`
	Bucket             string `yaml:"bucket,omitempty"`
	Region             string `yaml:"region,omitempty"`
	AccessKey          string `yaml:"accessKey,omitempty"`
	SecretKey          string `yaml:"secretKey,omitempty"`
	Insecure           string `yaml:"insecure,omitempty"`           // Automatically connect via http instead of https.
	InsecureSkipVerify string `yaml:"insecureSkipVerify,omitempty"` // If connecting via https, do not validate the certificate.
	Prefix             string `yaml:"prefix,omitempty"`
}

func (conf *s3StorConfig) validate() error {
	// Only insecure and insecureSkipVerify are allowed to be empty.
	if conf.Endpoint == "" {
		return fmt.Errorf("endpoint cannot be empty")
	}
	if conf.Bucket == "" {
		return fmt.Errorf("bucket cannot be empty")
	}
	if conf.Region == "" {
		return fmt.Errorf("region cannot be empty")
	}
	if conf.AccessKey == "" {
		return fmt.Errorf("access key cannot be empty")
	}
	if conf.SecretKey == "" {
		return fmt.Errorf("secret key cannot be empty")
	}
	return nil
}

func (first *s3StorConfig) merge(name string, second *s3StorConfig) {
	if second.Endpoint != "" {
		if first.Endpoint != "" {
			logrus.Debugf("overwriting endpoint %s with %s in storage %s", first.Endpoint, second.Endpoint, name)
		}
		first.Endpoint = second.Endpoint
	}
	if second.Bucket != "" {
		if first.Bucket != "" {
			logrus.Debugf("overwriting bucket %s with %s in storage %s", first.Bucket, second.Bucket, name)
		}
		first.Bucket = second.Bucket
	}
	if second.Region != "" {
		if first.Region != "" {
			logrus.Debugf("overwriting region %s with %s in storage %s", first.Region, second.Region, name)
		}
		first.Region = second.Region
	}
	if second.AccessKey != "" {
		if first.AccessKey != "" {
			logrus.Debugf("overwriting access key %s with %s in storage %s", first.AccessKey, second.AccessKey, name)
		}
		first.AccessKey = second.AccessKey
	}
	if second.SecretKey != "" {
		if first.SecretKey != "" {
			logrus.Debugf("overwriting secret key %s with %s in storage %s", first.SecretKey, second.SecretKey, name)
		}
		first.SecretKey = second.SecretKey
	}
	if second.Insecure != "" {
		if first.Insecure != "" {
			logrus.Debugf("overwriting insecure %s with %s in storage %s", first.Insecure, second.Insecure, name)
		}
		first.Insecure = second.Insecure
	}
	if second.InsecureSkipVerify != "" {
		if first.Insecure != "" {
			logrus.Debugf("overwriting insecureSkipVerify %s with %s in storage %s", first.InsecureSkipVerify, second.InsecureSkipVerify, name)
		}
		first.InsecureSkipVerify = second.InsecureSkipVerify
	}
	if second.Prefix != "" {
		if first.Prefix != "" {
			logrus.Debugf("overwriting prefix %s with %s in storage %s", first.Prefix, second.Prefix, name)
		}
		first.Prefix = second.Prefix
	}
}

// s3storage manages access to S3.
type s3storage struct {
	cli                   *minio.Client
	bucketName            string
	prefix                string
	lifecycle             Lifecycler
	region                string
	listCache             *ListCache
	objectTaggingFallback bool
}

// s3StorageOpt is an option for configuring the storage.
type s3StorageOpt func(*s3storage) error

// withObjectTaggingFallback tries to fetch the tags from object tagging if none
// are found in the metadata .
func withObjectTaggingFallback(b bool) s3StorageOpt {
	return func(u *s3storage) error {
		u.objectTaggingFallback = b
		return nil
	}
}

// withS3Lifecycle sets the life cycle manager for this storage.
func withS3Lifecycle(life Lifecycler) s3StorageOpt {
	return func(u *s3storage) error {
		u.lifecycle = life
		return nil
	}
}

// withS3Prefix sets the prefix for uploaded objects.
func withS3Prefix(prefix string) s3StorageOpt {
	return func(u *s3storage) error {
		u.prefix = prefix
		return nil
	}
}

func withS3BucketName(bucket string) s3StorageOpt {
	return func(u *s3storage) error {
		u.bucketName = bucket
		return nil
	}
}

// withS3Region sets the S3 region.
func withS3Region(region string) s3StorageOpt {
	return func(s *s3storage) error {
		s.region = region
		return nil
	}
}

func news3StorFromConf(conf s3StorConfig) (*s3storage, error) {
	// Create a minio client.
	insecure, err := strconv.ParseBool(conf.Insecure)
	if err != nil {
		insecure = false
	}
	skipVerify, err := strconv.ParseBool(conf.InsecureSkipVerify)
	if err != nil {
		skipVerify = false
	}
	cli, err := getMinioClient(conf.Endpoint, conf.Region, conf.AccessKey, conf.SecretKey, !insecure, skipVerify)
	if err != nil {
		return nil, err
	}

	return newS3Storage(cli,
		withS3Prefix(conf.Prefix),
		withS3Region(conf.Region),
		withS3BucketName(conf.Bucket),
		withObjectTaggingFallback(true),
	)
}

// newS3Storage provides a new S3 storage.
func newS3Storage(cli *minio.Client, opts ...s3StorageOpt) (*s3storage, error) {
	u := &s3storage{
		cli: cli,
	}
	u.region = defaultRegion
	for _, opt := range opts {
		if err := opt(u); err != nil {
			return nil, err
		}
	}
	return u, nil
}

// ClearCache clears the cache if present. If no cache has been configured, this
// is a no-op.
func (u *s3storage) ClearCache() {
	if u.listCache != nil {
		u.listCache.Clear()
	}
}

// Prepare the upload target bucket.
func (u *s3storage) Prepare(ctx context.Context) error {
	exists, err := u.cli.BucketExists(ctx, u.bucketName)
	if err != nil {
		if minio.ToErrorResponse(err).StatusCode != http.StatusForbidden {
			return fmt.Errorf("could not check if bucket exists: %w", err)
		}
		// The bucket probably exists, but we might not have the rights to issue
		// a HeadBucket call. This needs ListBucket access for the whole bucket.
		// Let's try to write a small test file to the bucket in question.
		testname, err := uuid.GenerateUUID()
		if err != nil {
			return fmt.Errorf("error generating uuid: %w", err)
		}
		testname = fmt.Sprintf("%s-%s", u.prefix, testname)
		buf := bytes.NewBufferString(fmt.Sprintf("This is a test file created at %s and can safely be deleted.", time.Now()))
		info, err := u.cli.PutObject(ctx, u.bucketName, testname, buf, int64(buf.Len()), minio.PutObjectOptions{})
		if err != nil {
			return fmt.Errorf("could not test for bucket existence by uploading test file: %w", err)
		}
		if err := u.cli.RemoveObject(ctx, u.bucketName, info.Key, minio.RemoveObjectOptions{}); err != nil {
			return fmt.Errorf("bucket exists, but can't remove the testfile, seems to be write-only: %w", err)
		}
		// We're done here. We (probably) have enough access to this bucket.
		return nil
	}
	if exists {
		return nil
	}

	opts := minio.MakeBucketOptions{Region: u.region}
	err = u.cli.MakeBucket(ctx, u.bucketName, opts)
	if err == nil {
		return nil
	}
	resp := minio.ToErrorResponse(err)
	if resp.Code != "BucketAlreadyExists" {
		return fmt.Errorf("error preparing S3 bucket %s: %w", u.bucketName, err)
	}
	// Looks like the name has already been taken. Bucket names in
	// Amazon S3 are globally unique, so we generate a (hopefully)
	// unique name.
	taken := u.bucketName
	u.bucketName = randString(16, hexDigLower) + "-backup"
	// Inform users about the generated name.
	logrus.Warnf("⚠️ bucket name %s already taken in S3, using generated bucket name: %s", taken, u.bucketName)
	if err := u.cli.MakeBucket(ctx, u.bucketName, opts); err != nil {
		// We give up here if we still run into errors.
		return fmt.Errorf("error preparing S3 bucket %s: %w", u.bucketName, err)
	}
	return nil
}

// Get loads the named object from object storage.
func (u *s3storage) Get(ctx context.Context, objectName string) (io.ReadCloser, error) {
	reader, err := u.cli.GetObject(ctx, u.bucketName, objectName, minio.GetObjectOptions{})
	if err != nil {
		return nil, fmt.Errorf("could not get object %s: %w", objectName, err)
	}
	return reader, nil
}

// Upload stuff to S3. objectName should not include .gz file ending and is not
// expected to be compressed already. Use withS3Compression() to set compression
// while uploading.
func (u *s3storage) Upload(ctx context.Context, objectName string, src io.Reader, tags map[string]string) (int64, error) {
	// Clear the cache after this operation, even if it fails
	if u.listCache != nil {
		defer u.listCache.Clear()
	}
	var err error
	tags, err = appendLifecycleTags(ctx, u.lifecycle, tags)
	if err != nil {
		return 0, fmt.Errorf("could not append lifecycle tags: %w", err)
	}

	// TODO: Drop tags if the resulting metadata would exceed 2KB.

	// Start the upload.
	logrus.Infof("uploading backup %s to S3 with tags: %s", objectName, tags)
	n, err := u.multiPartUpload(ctx, objectName, minio.PutObjectOptions{
		UserMetadata: tags,
	}, src)
	if err != nil {
		return n, fmt.Errorf("error while uploading: %w", err)
	}
	if u.lifecycle != nil {
		logrus.Infof("starting backup sweep cycle ...")
		if err := u.lifecycle.Sweep(ctx, time.Now()); err != nil {
			return 0, fmt.Errorf("could not recycle old backups: %w", err)
		}
		logrus.Infof("sweep cycle done")
	} else {
		logrus.Warnf("no lifecycle set, keeping backups forever (!)")
	}

	return n, nil
}

func partPrefix(prefix string) string {
	return filepath.Join(
		filepath.Dir(prefix),
		"parts",
		filepath.Base(prefix),
	)
}

func partName(prefix, uid string, i int) string {
	return filepath.Join(
		partPrefix(prefix),
		fmt.Sprintf("%s_%d.part", uid, i),
	)
}

// Because the total file size is not known in advance, we need to upload it in
// multiple parts, buffering each part in a small buffer. See here for details:
// https://github.com/minio/minio-go/issues/848
func (u *s3storage) multiPartUpload(ctx context.Context, objectName string, opts minio.PutObjectOptions, in io.Reader) (int64, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	const bufSize = 16 * Mi // Should be over 5 MiB (minimum upload size if not last part).
	var (
		srcs         = make([]minio.CopySrcOptions, 0)
		parts        = make([]minio.ObjectInfo, 0)
		buf          = make([]byte, bufSize)
		total  int64 = 0
		reader       = new(bytes.Reader)
	)
	uid, err := uuid.GenerateUUID()
	if err != nil {
		return 0, fmt.Errorf("could not generate uuid during multipart upload: %w", err)
	}
	for i := 0; ; i++ {
		n, err := io.ReadFull(in, buf)
		if err == io.EOF {
			break
		}
		if err != nil && err != io.ErrUnexpectedEOF {
			return 0, err
		}

		if logrus.IsLevelEnabled(logrus.TraceLevel) {
			u.cli.TraceOn(logrus.StandardLogger().WriterLevel(logrus.TraceLevel))
		}

		name := partName(u.prefix, uid, i)
		logrus.Debugf("uploading part %s of object %s (%s, total %s)", name, objectName, ByteCountBinary(int64(n)), ByteCountBinary(total))
		reader.Reset(buf[:n])
		part, err := u.cli.PutObject(
			ctx,
			u.bucketName,
			name,
			reader,
			int64(n),
			opts,
		)
		if err != nil {
			return 0, err
		}
		// Save part information for server-side concatenation later.
		srcs = append(srcs, minio.CopySrcOptions{
			Bucket: u.bucketName,
			Object: name,
		})
		// Save part information for deletion after successful upload.
		parts = append(parts, minio.ObjectInfo{
			ETag:         part.ETag,
			Key:          part.Key,
			LastModified: part.LastModified,
			Size:         part.Size,
			VersionID:    part.VersionID,
		})
		select {
		case <-ctx.Done():
			return 0, ctx.Err()
		default:
			total += int64(n)
		}
	}

	// Now we're finished, start concatenating uploaded parts.
	startTime := time.Now()
	dst := minio.CopyDestOptions{
		Bucket: u.bucketName,
		Object: objectName,
		Progress: newEmptyProgressReader(func(n int64) {
			rate := rateString(time.Since(startTime), n)
			logrus.Debugf("composed %s (%s) of object %s ...", ByteCountBinary(n), rate, objectName)
		}),
	}

	logrus.Debugf("composing target object out of uploaded parts ...")
	if _, err := u.cli.ComposeObject(ctx, dst, srcs...); err != nil {
		return 0, fmt.Errorf("error during composing object: %w", err)
	}

	removec := make(chan minio.ObjectInfo)
	go func() {
		defer close(removec)
		defer logrus.Debugf("done removing parts")
		for _, v := range parts {
			logrus.Debugf("removing part %s", v.Key)
			select {
			case removec <- v:
			case <-ctx.Done():
				return
			}
		}
	}()

	logrus.Debugf("removing parts ...")
	for err := range u.cli.RemoveObjects(ctx, u.bucketName, removec, minio.RemoveObjectsOptions{}) {
		return 0, fmt.Errorf("error removing uploaded parts after composing: %w", err.Err)
	}

	return total, nil
}

func (u *s3storage) Delete(ctx context.Context, delc <-chan string) <-chan error {
	if u.listCache != nil {
		defer u.listCache.Clear()
	}
	out := make(chan error)
	removec := make(chan minio.ObjectInfo)
	deleteKeys := make([]string, 0)
	go func() {
		defer close(removec)
		for name := range delc {
			deleteKeys = append(deleteKeys, name)
			select {
			case removec <- minio.ObjectInfo{
				Key: name,
			}:
			case <-ctx.Done():
				out <- ctx.Err()
			}
		}
	}()
	go func() {
		defer close(out)
		hadError := false
		for err := range u.cli.RemoveObjects(ctx, u.bucketName, removec, minio.RemoveObjectsOptions{}) {
			select {
			case out <- err.Err:
				hadError = true
			case <-ctx.Done():
				return
			}
		}

		numDeletes := len(deleteKeys)
		if hadError {
			// Here we know that at least one error occured during
			// RemoveObjects(). We also know that removec must have already been
			// closed. Therefore it's safe to loop through the deleteKeys slice
			// which was filled inside the other go routine.
			// For each object that should have been deleted.
			for _, key := range deleteKeys {
				// We check whether it is still available.
				obj, getErr := u.cli.GetObject(ctx, u.bucketName, key, minio.GetObjectOptions{})
				objInfo, statErr := obj.Stat()
				// If it is ...
				if getErr == nil && statErr == nil && objInfo.Err == nil {
					// ... it has not been deleted.
					numDeletes = numDeletes - 1
				}
			}
		}

		logrus.Infof("deleted %d objects from s3 storage", numDeletes)

	}()
	return out
}

func fileExtensionFromObjKey(k string) (string, error) {
	k = filepath.Base(k)
	parts := strings.Split(k, ".")
	if len(parts) == 0 {
		return "", fmt.Errorf("object key base %q cannot be split using separator \".\"", k)
	}
	return parts[len(parts)-1], nil
}

func (u *s3storage) ListBundles(ctx context.Context) (backupItemList, error) {
	listOpts, err := ApplyListOptions(OnlyBundles{})
	if err != nil {
		return nil, err
	}
	return u.list(ctx, listOpts)
}

func (u *s3storage) List(ctx context.Context, opts ...ListOpt) (backupItemList, error) {
	listOpts, err := ApplyListOptions(opts...)
	if err != nil {
		return nil, err
	}
	return u.list(ctx, listOpts)
}

func (u *s3storage) list(ctx context.Context, listOpts ListOpts) (backupItemList, error) {
	var list, bundleList []*backupItem
	if u.listCache != nil {
		if listOpts.returnBundles {
			if bundleList := u.listCache.GetBundles(); bundleList != nil {
				return bundleList, nil
			}
		} else {
			if list := u.listCache.Get(); list != nil {
				// If we have a cached list, filter it according to the
				// requested suffix and tags.
				n := 0
				for _, elem := range list {
					if listOpts.matchingSuffix(elem.Key) &&
						listOpts.matchingTags(elem.Tags) {
						list[n] = elem
						n++
					}
				}
				list = list[:n]
				return list, nil
			}
		}
	}

	prefix := u.prefix
	if listOpts.withPrefix != "" {
		prefix = listOpts.withPrefix
	}

	recurse := true
	objects := u.cli.ListObjects(ctx, u.bucketName, minio.ListObjectsOptions{
		Prefix:       prefix,
		Recursive:    recurse,
		WithMetadata: true,
	})
	for obj := range objects {
		if obj.Err != nil {
			logrus.Errorf("error retrieving object: %s", obj.Err)
			return nil, obj.Err
		}
		if !strings.HasPrefix(obj.Key, prefix) {
			// Ignore objects that do not have the correct prefix.
			continue
		}
		if !listOpts.matchingSuffix(obj.Key) {
			// Ignore objects that do not have the correct suffix (if the suffix
			// must be considered).
			continue
		}
		ext, err := fileExtensionFromObjKey(obj.Key)
		if err != nil {
			return nil, fmt.Errorf("could not get file extension from object key: %w", err)
		}
		if strings.HasPrefix(ext, fileExtensionPart) {
			// Ignore objects with a file extension that starts with "part".
			continue
		}

		// Fetch tags from metadata.
		tags, err := u.tagsFromObjInfo(ctx, obj)
		if err != nil {
			if minio.ToErrorResponse(err).StatusCode == http.StatusNotFound {
				// If the object was deleted since the call to
				// u.cli.ListObjects(), we continue with the next object.
				logrus.Warn(err)
				continue
			}
			return nil, fmt.Errorf("could not get tags from object info: %w", err)
		}

		if !listOpts.matchingTags(tags) {
			// Ignore list elements that don't match all tags from the list
			// options.
			continue
		}

		item := &backupItem{
			Key:          obj.Key,
			LastModified: obj.LastModified,
			Size:         obj.Size,
			Tags:         tags,
			BucketName:   u.bucketName,
		}
		if val, ok := tags["is_bundle"]; ok && val == "true" {
			// Ignore backup bundle metadata objects as return value for the
			// "regular" list command. But still store them in a separate list.
			bundleList = append(bundleList, item)
			continue
		}
		// Apply tags now if relevant for backups.
		if daily, ok := tags[backupDaily]; ok {
			item.Daily = daily == "true"
		}
		if weekly, ok := tags[backupWeekly]; ok {
			item.Weekly = weekly == "true"
		}
		if monthly, ok := tags[backupMonthly]; ok {
			item.Monthly = monthly == "true"
		}
		if err := addDownloadLink(ctx, u.cli, u.bucketName, item); err != nil {
			logrus.Error(err)
			continue
		}
		list = append(list, item)
	}
	sort.Sort(byULID(list))
	countBackups(list)
	// Don't update the cache if it the list elements have no tagging or have
	// been filtered by a suffix or tags.
	if u.listCache != nil && listOpts.withSuffix == "" &&
		len(listOpts.withTags) == 0 && listOpts.withPrefix == "" {
		u.listCache.Update(list)
		u.listCache.UpdateBundles(bundleList)
	}
	if listOpts.returnBundles {
		return bundleList, nil
	} else {
		return list, nil
	}
}

func (u *s3storage) tagsFromObjInfo(ctx context.Context, info minio.ObjectInfo) (map[string]string, error) {
	tags := make(map[string]string)

	// If there is no user metadata for the current object, stat it. This is
	// required because not all s3 implementations provide metadata under list
	// operations (https://github.com/minio/minio-go/issues/895).
	if len(info.UserMetadata) == 0 {
		var err error
		info, err = u.cli.StatObject(ctx, u.bucketName, info.Key, minio.GetObjectOptions{})
		if err != nil {
			if minio.ToErrorResponse(err).StatusCode == http.StatusNotFound {
				return tags, err
			}
			return tags, fmt.Errorf("could not stat object %s", info.Key)
		}
	}

	for k, v := range info.UserMetadata {
		if k == "content-type" {
			continue
		}
		tags[strings.TrimPrefix(strings.ToLower(k), "x-amz-meta-")] = v
	}

	// If no tags were found in metadata, try to fetch tags from object
	// tagging. (But only if activated.)
	if len(tags) == 0 && u.objectTaggingFallback {
		// Fetch object tagging and retry in case of failure (except if
		// status is NotFound).
		var tt *miniotags.Tags
		const retryTimeout = 15 * time.Minute
		rCtx, rCancel := context.WithTimeout(ctx, retryTimeout)
		bo := backoff.WithContext(backoff.NewExponentialBackOff(), rCtx)
		err := backoff.Retry(func() error {
			var err error
			tt, err = u.cli.GetObjectTagging(ctx, u.bucketName, info.Key, minio.GetObjectTaggingOptions{})
			if err != nil {
				if minio.ToErrorResponse(err).StatusCode == http.StatusNotFound {
					// If the object was deleted since the call to
					// u.cli.ListObjects(), we backoff permanently.
					return backoff.Permanent(err)
				}
				logrus.Errorf("s3 get object tagging error: %v: retrying...", err)
				return err
			}
			return nil
		}, bo)
		rCancel()
		if err != nil {
			if minio.ToErrorResponse(err).StatusCode == http.StatusNotFound {
				return nil, err
			}
			return nil, fmt.Errorf("could not fetch object tags for object %s: %w", info.Key, err)
		}
		tags = tt.ToMap()
		if len(tags) > 0 {
			// TODO: Find a way to log less?
			logrus.Debugf("could not fetch tags from metadata for object %s: fetched them from object tagging", info.Key)
		}
	}
	return tags, nil
}

func (u *s3storage) Link(ctx context.Context, objectName string) (string, error) {
	params := make(url.Values)
	params.Set("response-content-disposition", fmt.Sprintf(`attachment; filename="%s"`, objectName))
	url, err := u.cli.PresignedGetObject(ctx, u.bucketName, objectName, time.Hour*24*7, params)
	if err != nil {
		return "", err
	}
	return url.String(), nil
}

// addDownloadLink adds a presigned download link valid for 24 hours.
func addDownloadLink(ctx context.Context, cli *minio.Client, bucketName string, item *backupItem) error {
	params := make(url.Values)
	params.Set("response-content-disposition", fmt.Sprintf(`attachment; filename="%s"`, item.Key))

	u, err := cli.PresignedGetObject(ctx, bucketName, item.Key, time.Hour*24, params)
	if err != nil {
		return fmt.Errorf("error building the download link: %w", err)
	}

	item.Link = u
	return nil
}

func (u *s3storage) Merge(ctx context.Context, srcNames []string, targetName string, objectTags map[string]string) error {
	if u.listCache != nil {
		defer u.listCache.Clear()
	}

	// Append lifecycle tags.
	var err error
	objectTags, err = appendLifecycleTags(ctx, u.lifecycle, objectTags)
	if err != nil {
		return fmt.Errorf("could not append lifecycle tags: %w", err)
	}

	// Define copy sources.
	srcs := make([]minio.CopySrcOptions, 0)
	for _, name := range srcNames {
		srcs = append(srcs, minio.CopySrcOptions{
			Bucket: u.bucketName,
			Object: name,
		})
	}

	// Define copy Destination.
	startTime := time.Now()
	dst := minio.CopyDestOptions{
		Bucket: u.bucketName,
		Object: targetName,
		Progress: newEmptyProgressReader(func(n int64) {
			rate := rateString(time.Since(startTime), n)
			logrus.Debugf("merged %s (%s) of object %s ...", ByteCountBinary(n), rate, targetName)
		}),
	}

	// Compose the object from its parts.
	logrus.Debugf("merging target object %q from parts %q...", targetName, srcNames)
	if _, err := u.cli.ComposeObject(ctx, dst, srcs...); err != nil {
		return fmt.Errorf("error during object merge: %w", err)
	}
	logrus.Debugf("done merging target object %q from parts %q", targetName, srcNames)

	// Put object tags for the composed object.
	err = u.Tag(ctx, targetName, objectTags)
	if err != nil {
		return fmt.Errorf("could not put object tags for the composed object %q: %w", targetName, err)
	}

	// Remove the object parts.
	removec := make(chan minio.ObjectInfo)
	go func() {
		defer close(removec)
		defer logrus.Debugf("done removing parts")
		for _, objectName := range srcNames {
			logrus.Debugf("removing part %s", objectName)
			select {
			case removec <- minio.ObjectInfo{
				Key: objectName,
			}:
			case <-ctx.Done():
				return
			}
		}
	}()
	logrus.Debugf("removing parts ...")
	for err := range u.cli.RemoveObjects(ctx, u.bucketName, removec, minio.RemoveObjectsOptions{}) {
		return fmt.Errorf("error removing uploaded parts after composing: %w", err.Err)
	}
	return nil
}

func (u *s3storage) Lifecycle() (Lifecycler, error) {
	if u.lifecycle == nil {
		return nil, errNotFound
	}
	return u.lifecycle, nil
}

func (u *s3storage) Tag(ctx context.Context, objectName string, objectTags map[string]string) error {
	// First fetch all existing tags (defined in s3 object's metadata).
	tags, err := u.tagsFromObjInfo(ctx, minio.ObjectInfo{Key: objectName})
	if err != nil {
		return fmt.Errorf("could not get tags from object info: %w", err)
	}

	// Set the new objects tags (overwriting the existing tags in case of a
	// conflict).
	for k, v := range objectTags {
		tags[k] = v
	}

	// Set the new tags on the objects (i.e. copy the object to update it's
	// metadata).
	srcOpts := minio.CopySrcOptions{
		Bucket: u.bucketName,
		Object: objectName,
	}
	dstOpts := minio.CopyDestOptions{
		Bucket:          u.bucketName,
		Object:          objectName,
		UserMetadata:    tags,
		ReplaceMetadata: true,
	}
	_, outErr := u.cli.ComposeObject(ctx, dstOpts, srcOpts)
	return outErr
}

// var keepTagsTrueVal = []string{
// 	tagKeyIsBaseBackup,
// 	tagKeyIsBaseBackupMeta,
// 	tagKeyIsTxLog,
// }

// var keepTags = append(keepTagsTrueVal, []string{
// 	tagKeyNamespace,
// 	tagKeyPodName,
// 	tagKeyBackupID,
// 	tagKeyExpiry,
// 	backupDaily,
// 	backupWeekly,
// 	backupMonthly,
// 	variantTagName,
// }...)

func (u *s3storage) listSweepParts(ctx context.Context, partSweepDelay time.Duration, isFromMultipartUpload bool) []string {
	sweepKeys := make([]string, 0)

	var prefix string
	switch isFromMultipartUpload {
	case true:
		prefix = partPrefix(u.prefix)
	case false:
		prefix = u.prefix
	}

	objects := u.cli.ListObjects(ctx, u.bucketName, minio.ListObjectsOptions{
		Prefix:       prefix,
		Recursive:    true,
		WithMetadata: true,
	})
	for obj := range objects {
		if obj.Err != nil {
			logrus.Errorf("error retrieving object: %s", obj.Err)
			continue
		}
		if !strings.HasPrefix(obj.Key, prefix) {
			// Ignore objects that do not have the correct prefix.
			continue
		}
		ext, err := fileExtensionFromObjKey(obj.Key)
		if err != nil {
			logrus.Errorf("could not get file extension from object key: %v", err)
			continue
		}
		if !strings.HasPrefix(ext, fileExtensionPart) {
			// Ignore objects WITHOUT a file extension that starts with "part".

			// Only log something for non tag.gz-files. Otherwise we'd have many
			// logs for the regular backup items.
			if !strings.HasSuffix(obj.Key, fileExtensionTarGz) {
				logrus.Debugf("s3 parts sweep: ignored object %s: not a part file", obj.Key)
			}
			continue
		}

		// Fetch object tagging and retry in case of failure (except if status
		// is NotFound).
		haveTags, err := u.tagsFromObjInfo(ctx, obj)
		if err != nil {
			logrus.Errorf("could not get tags from object info: %v", err)
			continue
		}

		if haveTags[variantTagName] != txLogArchiving {
			// Ignore objects that don't have the variant tag set to
			// tx_log_archiving.
			logrus.Debugf("s3 parts sweep: ignored object %s: not of variant %s", obj.Key, txLogArchiving)
			continue
		}

		wantTags := tagsFromEnv(os.Environ())
		haveAllWantTags := true
		for wantKey, wantValue := range wantTags {
			if haveTags[wantKey] != wantValue {
				haveAllWantTags = false
				logrus.Debugf("s3 parts sweep: ignored object %s: tag %s mismatch %s != %s", obj.Key, wantKey, haveTags[wantKey], wantValue)
				break
			}
		}
		if !haveAllWantTags {
			// Ignore objects that don't have all the tags we want (according to
			// the environment variables).
			continue
		}

		var t time.Time
		var timeName string
		switch isFromMultipartUpload {
		case true:
			_, err := UUID(obj.Key)
			if err != nil {
				logrus.Warnf("could not get uuid for object with key %s: %v", obj.Key, err)
				continue
			}
			t = obj.LastModified
			timeName = "mod"
		case false:
			objULID, err := ULID(obj.Key)
			if err != nil {
				logrus.Warnf("could not get ulid for object with key %s: %v", obj.Key, err)
				continue
			}
			t = ulid.Time(objULID.Time())
			timeName = "create"
		}
		if time.Since(t) <= partSweepDelay {
			// Ignore objects that have a modification time which refers
			// to a time within the part sweep delay.
			logrus.Debugf("s3 parts sweep: ignored object %s: %s time %s not old enough (%s)", obj.Key, timeName, t, partSweepDelay)
			continue
		}

		sweepKeys = append(sweepKeys, obj.Key)
	}
	return sweepKeys
}

func (u *s3storage) SweepParts(ctx context.Context, partSweepDelay time.Duration) {
	sweepKeys := make([]string, 0)
	// List all parts originating from a multipart upload that must be swept.
	sweepKeys = append(sweepKeys, u.listSweepParts(ctx, partSweepDelay, true)...)
	// List all parts originating from a regular upload or merge-Upload that
	// must be swept.
	sweepKeys = append(sweepKeys, u.listSweepParts(ctx, partSweepDelay, false)...)

	// Delete all objects that must be swept.
	if len(sweepKeys) > 0 {
		delCh := make(chan string)
		go func() {
			defer close(delCh)
			for _, sweepKey := range sweepKeys {
				select {
				case delCh <- sweepKey: // Send for deletion.
					logrus.Infof("sweeping part %v", sweepKey)
				case <-ctx.Done():
					return
				}
			}
		}()
		for err := range u.Delete(ctx, delCh) {
			if err != nil {
				logrus.Errorf("could not sweep part: %v", err)
			}
		}
	} else {
		logrus.Infof("found no parts to sweep")
	}
}
