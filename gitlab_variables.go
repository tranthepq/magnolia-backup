package main

import (
	"fmt"
	"sort"
)

// Transform to weird gitlab syntax:
// https://docs.gitlab.com/ee/ci/triggers/#making-use-of-trigger-variables
func gitlabVariablesParam(vars map[string]string) map[string]string {
	params := make(map[string]string)
	keys := make([]string, 0)
	for k := range vars {
		keys = append(keys, k)
	}
	sort.Strings(keys) // Try to be consistent with the sorting of gitlab variables.
	for _, k := range keys {
		params[fmt.Sprintf("variables[%s]", k)] = vars[k]
	}
	return params
}
