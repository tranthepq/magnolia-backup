package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/klauspost/compress/gzip"
)

// newGzipReader transparently compresses data from in.
func newGzipReader(in io.Reader, level int) io.Reader {
	r, w := io.Pipe()
	go func() {
		defer w.Close()
		gz, err := gzip.NewWriterLevel(w, level)
		if err != nil {
			w.CloseWithError(err)
		}
		defer gz.Close()
		if _, err := io.Copy(gz, in); err != nil {
			w.CloseWithError(err)
		}
	}()
	return r
}

type progressReader struct {
	io.Reader       // Embedding reader.
	total     int64 // Total bytes read so far.
	// Called with the total bytes read from the embedded reader. This will be
	// called no more than twice a second and also right when reaching EOF.
	progressFunc func(int64)
	ticker       *time.Ticker
}

const (
	progressTickInterval time.Duration = time.Millisecond * 500
)

func newProgressReader(r io.Reader, f func(int64)) *progressReader {
	if f == nil {
		f = func(int64) {}
	}
	pr := &progressReader{
		Reader:       r,
		progressFunc: f,
		ticker:       time.NewTicker(progressTickInterval),
	}
	return pr
}

func (p *progressReader) Read(b []byte) (int, error) {
	n, err := p.Reader.Read(b)
	p.total += int64(n)
	select {
	case <-p.ticker.C:
		p.progressFunc(p.total)
	default:
		if err == io.EOF {
			p.progressFunc(p.total) // Last call to progress func.
		}
	}
	return n, err
}

// emptyReader does not do anything. It is used to show progress of operations
// expressed by an io.Reader.
type emptyReader struct{}

func (emptyReader) Read(b []byte) (int, error) {
	return len(b), nil
}

func newEmptyProgressReader(f func(int64)) *progressReader {
	return newProgressReader(emptyReader{}, f)
}

func isEmpty(name string) (bool, error) {
	f, err := os.Open(name)
	if err != nil {
		return false, err
	}
	defer f.Close()

	_, err = f.Readdirnames(1)
	if err == io.EOF {
		return true, nil
	}
	return false, err // Either not empty or error.
}

type divideReader struct {
	in         io.Reader
	readLimit  int
	endReached bool
	partNumber int
}

// hasNext blocks until all io.Readers previously returned by next() have
// returned with EOF.
func (d *divideReader) hasNext() bool {
	return !d.endReached
}

// next returns an io.Reader which is limited to the reading of the next n bytes
// (where n = divideReader.readLimit).
func (d *divideReader) next() io.Reader {
	defer func() {
		d.partNumber = d.partNumber + 1
	}()

	// Create the next (output) reader and a writer used to write to it.
	r, w := io.Pipe()
	// Create the limit reader.
	limr := io.LimitReader(d.in, int64(d.readLimit))

	go func() {
		defer w.Close()
		n, err := io.Copy(w, limr)
		if err != nil {
			w.CloseWithError(err)
			return
		}
		if n != int64(d.readLimit) {
			d.endReached = true
		}
	}()

	return r
}

func (d *divideReader) nextObjectPartName(objectName string) string {
	return fmt.Sprintf("%s.%s%d", objectName, fileExtensionPart, d.partNumber)
}

func newDivideReader(in io.Reader, readLimit int) *divideReader {
	return &divideReader{
		in:         in,
		readLimit:  readLimit,
		partNumber: 1,
	}
}

type fileInfo struct {
	size int64
	os.FileInfo
}

func (f fileInfo) Size() int64 {
	return f.size
}

type byteRange struct {
	offset int64
	length int64
}

func newRange(total, chunkSize int64) []byteRange {
	if chunkSize <= 0 {
		panic("newRange(): chunkSize should be > 0")
	}
	if total < 0 {
		panic("newRange(): total should be >= 0")
	}
	max := total / chunkSize
	rem := total % chunkSize

	var out = make([]byteRange, max)
	var i int64
	for ; i < max; i++ {
		out[i] = byteRange{
			offset: i * chunkSize,
			length: chunkSize,
		}
	}
	if rem > 0 {
		out = append(out, byteRange{
			offset: i * chunkSize,
			length: rem,
		})
	}
	return out
}
