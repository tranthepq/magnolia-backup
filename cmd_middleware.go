package main

import (
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// Times cobra commands. It outputs the total wall clock time taken for the
// command execution to stderr.
func timeCmdMiddleware(cmd *cobra.Command) *cobra.Command {
	if !cmd.Runnable() {
		return cmd
	}
	run := cmd.Run
	runE := cmd.RunE

	switch {
	case run != nil:
		cmd.Run = func(c *cobra.Command, a []string) {
			start := time.Now()
			defer func() {
				logrus.Infof("👉 request took %s", time.Since(start))
			}()
			run(c, a)
		}
	case runE != nil:
		cmd.RunE = func(c *cobra.Command, a []string) error {
			start := time.Now()
			defer func() {
				logrus.Infof("👉 request took %s", time.Since(start))
			}()
			return runE(c, a)
		}
	}

	return cmd
}
