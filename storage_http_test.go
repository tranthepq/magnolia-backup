package main

import (
	"context"
	"io"
	"net/http"
	"strings"
	"sync"
	"testing"
)

func Test_backupIDFromFileName(t *testing.T) {
	type args struct {
		fileName string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := backupIDFromFileName(tt.args.fileName); got != tt.want {
				t.Errorf("backupIDFromFileName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_fileName(t *testing.T) {
	hdrs := make(http.Header)
	hdrs.Set(headerKeyContentDisposition, `attachment; filename=test_basebackup_id2_basebackup_second_id2-meta.tar.gz`)

	wrongHdr := make(http.Header)
	wrongHdr["content-disposition"] = []string{`attachment; filename=test_basebackup_id2_basebackup_second_id2-meta.tar.gz`}

	type args struct {
		header http.Header
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "normal case",
			args:    args{header: hdrs},
			want:    "test_basebackup_id2_basebackup_second_id2-meta.tar.gz",
			wantErr: false,
		},
		{
			name:    "failure case",
			args:    args{header: wrongHdr},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := fileName(tt.args.header)
			if (err != nil) != tt.wantErr {
				t.Errorf("fileName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("fileName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_httpDownloader_Upload(t *testing.T) {
	type fields struct {
		w                http.ResponseWriter
		metaW            http.ResponseWriter
		contentType      string
		initiatedObjects map[string]bool
	}
	type args struct {
		ctx        context.Context
		objectName string
		src        io.Reader
		tags       map[string]string
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		want        int64
		wantErr     bool
		wantContent string
		wantHeader  string
	}{
		{
			name: "upload meta file",
			fields: fields{
				w: nil,
				metaW: &metaResponseBuffer{
					header: make(http.Header),
				},
				initiatedObjects: make(map[string]bool),
			},
			args: args{
				ctx:        context.Background(),
				objectName: "test" + metaFileSuffix,
				src: &mockReader{
					bytes: []byte("hello"),
				},
				tags: nil,
			},
			want:        int64(len("hello")),
			wantErr:     false,
			wantContent: "hello",
			wantHeader:  `attachment; filename="test-meta.tar.gz"`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &httpDownloader{
				w:                tt.fields.w,
				metaW:            tt.fields.metaW,
				contentType:      tt.fields.contentType,
				initiatedObjects: tt.fields.initiatedObjects,
				mu:               sync.Mutex{},
			}
			got, err := d.Upload(tt.args.ctx, tt.args.objectName, tt.args.src, tt.args.tags)
			if (err != nil) != tt.wantErr {
				t.Errorf("httpDownloader.Upload() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("httpDownloader.Upload() = %v, want %v", got, tt.want)
			}
			b := d.fetchMetaResponseBuffer()
			if !strings.EqualFold(strings.TrimSpace(string(b.content)), tt.wantContent) {
				t.Errorf("expected content %s, but got %s", tt.wantContent, string(b.content))
			}
			if tt.wantHeader != "" {
				hdr := b.header.Get(headerKeyContentDisposition)
				if hdr != tt.wantHeader {
					t.Errorf("unexpected header, got = %s, want = %s", hdr, tt.wantHeader)
				}
			}
		})
	}
}
