package main

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

// Uploader uploads stuff to a target location.
type Uploader interface {
	Upload(ctx context.Context, objectName string, in io.Reader, tags map[string]string) (int64, error) // Upload stuff reading from io.Reader.
}

// Preparer prepares upload location if needed (and supported).
type Preparer interface {
	Prepare(ctx context.Context) error
}

// OnlyBundles lists only bundles.
type OnlyBundles struct{}

var _ ListOpt = OnlyBundles{}

func (o OnlyBundles) ApplyToList(opts *ListOpts) error {
	opts.returnBundles = true
	return nil
}

// WithPrefix lists only elements with the given prefix. If this option is used,
// the prefix defined in the storage configuration is ignored during the list
// call.
type WithPrefix struct {
	Prefix string
}

var _ ListOpt = WithPrefix{}

func (o WithPrefix) ApplyToList(opts *ListOpts) error {
	opts.withPrefix = o.Prefix
	return nil
}

// WithSuffix lists only elements with the given suffix.
type WithSuffix struct {
	Suffix string
}

var _ ListOpt = WithSuffix{}

func (o WithSuffix) ApplyToList(opts *ListOpts) error {
	opts.withSuffix = o.Suffix
	return nil
}

// WithTags lists only elements with all given tags.
type WithTags struct {
	Tags map[string]string
}

var _ ListOpt = WithTags{}

func (o WithTags) ApplyToList(opts *ListOpts) error {
	opts.withTags = o.Tags
	return nil
}

type ListOpts struct {
	returnBundles bool
	withSuffix    string
	withPrefix    string
	withTags      map[string]string

	// Set to true if also objects without the tag "variant=tx_log_archiving"
	// should be listed.
	withoutTxLogArchivingVariant bool
}

func (o ListOpts) matchingSuffix(objectKey string) bool {
	if o.withSuffix != "" &&
		!strings.HasSuffix(objectKey, o.withSuffix) {
		return false
	}
	return true
}

func (o ListOpts) matchingTags(tags map[string]string) bool {
	matchesAllTags := true
	if len(o.withTags) > 0 {
		for key, val := range o.withTags {
			v, ok := tags[key]
			if !ok || val != v {
				matchesAllTags = false
				break
			}
		}
	}
	// If the tag "variant=tx_log_archiving" must also be matched, do it.
	if !o.withoutTxLogArchivingVariant &&
		tags[variantTagName] != txLogArchiving {
		matchesAllTags = false
	}
	return matchesAllTags
}

type ListOpt interface {
	ApplyToList(*ListOpts) error
}

func ApplyListOptions(opts ...ListOpt) (ListOpts, error) {
	// Determine whether we run in pg_wal mode. If we are not in pg_wal mode, we
	// must also list backup items that don't have the tag
	// "variant=tx_log_archiving".
	vip, ok := vipers["dump"]
	usePgWal := true
	if ok {
		usePgWal = vip.GetBool("use-pg-wal")
	}
	listOpts := ListOpts{
		withoutTxLogArchivingVariant: !usePgWal,
	}

	for _, opt := range opts {
		if opt == nil {
			continue
		}
		err := opt.ApplyToList(&listOpts)
		if err != nil {
			return listOpts, fmt.Errorf("could not apply list option of type %T: %w", opt, err)
		}
	}
	return listOpts, nil
}

// Lister should be able to list backups on the target storage.
type Lister interface {
	List(context.Context, ...ListOpt) (backupItemList, error)
}

// Deleter should delete incoming object names from the backup target until chan
// is closed.
type Deleter interface {
	Delete(context.Context, <-chan string) <-chan error
}

// Getter gets stuff from the object storage.
type Getter interface {
	Get(ctx context.Context, objectName string) (io.ReadCloser, error) // Download stuff writing to out (streaming).
}

// Linker returns a presigned direct download link, usable for a limited amount
// of time via a simple HTTP GET call.
type Linker interface {
	Link(ctx context.Context, objectName string) (string, error)
}

// If the object storage renames uploaded objects we can get the changed name if
// the object storage implements this.
type FixObjectNamer interface {
	FixObjectName(objectName string) string
}

// Tagger tags the given object with the given tags. Should the given tags
// conflict with already existing tags on the object, the given tags overwrite
// the existing ones.
type Tagger interface {
	Tag(ctx context.Context, objectName string, tags map[string]string) error
}

// ObjectStorage represents an object storage target.
type Merger interface {
	Merge(ctx context.Context, srcNames []string, targetName string, tags map[string]string) error
}

type UploadMerger interface {
	Uploader
	Merger
}

type LifecycleGetter interface {
	Lifecycle() (Lifecycler, error)
}

// ObjectStorage represents an object storage target.
type ObjectStorage interface {
	Preparer
	Lister
	Deleter
	Getter
	Linker
	UploadMerger
	LifecycleGetter
	Tagger
}

var errNotPart = fmt.Errorf("not a part")

func objectNameFromParts(parts []string) (string, error) {
	objectName := ""
	if len(parts) < 1 {
		return "", fmt.Errorf("could not find object name: no parts given")
	}
	for i, part := range parts {
		if i == 0 {
			pp := strings.Split(part, ".")
			if len(pp) < 2 {
				return "", fmt.Errorf("given name %d (%q): %w", i, part, errNotPart)
			}
			last := pp[len(pp)-1]
			if !strings.HasPrefix(last, fileExtensionPart) {
				return "", fmt.Errorf("given name %d (%q): %w", i, part, errNotPart)
			}
			objectName = strings.Join(pp[:len(pp)-1], ".")
		}
		if !strings.HasPrefix(part, objectName) {
			return "", fmt.Errorf("given name %d (%q), is not part of object %q", i, part, objectName)
		}
	}
	return objectName, nil
}

func DeleteOlderBundles(ctx context.Context, deleter Deleter, bundleList []*backupItem, olderThan time.Duration) error {
	if olderThan == 0 {
		logrus.Info("retention duration is set to 0: keeping bundles indefinitely")
		return nil
	}

	// Send all bundles that are older than the given duration off for deletion.
	tooOld := make(chan string)
	go func() {
		defer close(tooOld)
		for _, bundle := range bundleList {
			if time.Since(bundle.LastModified) > olderThan {
				logrus.Infof("removing old backup bundle %s, last modified on %s", bundle.Key, bundle.LastModified)
				select {
				case tooOld <- bundle.Key:
				case <-ctx.Done():
					return
				}
			}
		}
	}()

	// Remove older backups from object storage.
	for err := range deleter.Delete(ctx, tooOld) {
		return fmt.Errorf("error while removing old backup bundle: %w", err)
	}
	return nil
}
