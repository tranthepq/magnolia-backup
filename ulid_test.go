package main

import (
	"fmt"
	"reflect"
	"regexp"
	"sort"
	"testing"
	"time"
)

const (
	// See here for details: https://github.com/ulid/spec#encoding
	b32c = `[0123456789ABCDEFGHJKMNPQRSTVWXYZ]{26}`
)

func Test_newBackupName(t *testing.T) {
	currentBackupID.Set(MustNewULID())
	type args struct {
		prefix string
		suffix string
	}
	tests := []struct {
		name    string
		args    args
		want    *regexp.Regexp
		wantErr bool
	}{
		{
			name: "simple case",
			args: args{
				prefix: "prod",
				suffix: "basebackup",
			},
			want: regexp.MustCompile(fmt.Sprintf("^prod/%s/basebackup/%s$", b32c, b32c)),
		},
		{
			name: "missing prefix",
			args: args{
				prefix: "",
				suffix: "basebackup",
			},
			want: regexp.MustCompile(fmt.Sprintf("^%s/basebackup/%s$", b32c, b32c)),
		},
		{
			name: "missing suffix",
			args: args{
				prefix: "prod",
				suffix: "",
			},
			want: regexp.MustCompile(fmt.Sprintf("^prod/%s/%s$", b32c, b32c)),
		},
		{
			name: "missing prefix and suffix",
			args: args{
				prefix: "",
				suffix: "",
			},
			want: regexp.MustCompile(fmt.Sprintf("^%s/%s$", b32c, b32c)),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := newBackupName(tt.args.prefix, tt.args.suffix)
			if !tt.want.MatchString(got) {
				t.Errorf("newBackupName() = %v, does not match regexp %s", got, tt.want)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("newBackupName() err = %v, but wantErr %t", err, tt.wantErr)
			}
		})
	}
}

func Test_CheckCorrectUseOfULID(t *testing.T) {
	n := 10000
	var unsorted []string
	for i := 0; i < n; i++ {
		unsorted = append(unsorted, MustNewULID())
	}

	sorted := make([]string, len(unsorted))
	copy(sorted, unsorted)
	sort.Strings(sorted)

	if !reflect.DeepEqual(unsorted, sorted) {
		t.Errorf("ulid not monotonic")

		for k, v := range sorted {
			t.Logf("%d %s %s", k, v, unsorted[k])
		}
	}
}

func TestObjectNameToTimeRFC3339(t *testing.T) {
	tests := []struct {
		name       string
		objectName string
		want       string
	}{
		{
			name:       "ulid",
			objectName: "01GYV5N2ZB64JX6H2RFQ87648W/basebackup/01GYV5N2ZC0YEPK8KZKC2K876P.tar.gz",
			want:       time.Date(2023, time.April, 25, 3, 0, 0, 108, time.UTC).Local().Format(time.RFC3339),
		},
		{
			name:       "non-ulid",
			objectName: "backups-stage/stage2023-05-22-142516.gz",
			want:       time.Date(2023, time.May, 22, 14, 25, 16, 0, time.UTC).Format(time.RFC3339),
		},
		// {
		// 	name:       "neither",
		// 	objectName: "whatever_bla_bla_bla_ole_ole_ole",
		// 	want:       "",
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ObjectNameToTimeRFC3339(tt.objectName); got != tt.want {
				t.Errorf("ObjectNameToTimeRFC3339() = %v, want %v", got, tt.want)
			}
		})
	}
}
