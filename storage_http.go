package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	contentTypeTar = "application/tar"

	headerKeyContentDisposition = "Content-Disposition"
	headerKeyContentType        = "Content-Type"

	headerValueContentDispositionPrefix = "attachment; filename="

	httpDownloaderFileNameSeparator = "_"
)

// httpDownloader fetches a new (base) backup according to the command and
// streams a tarball to the HTTP client requesting it.
type httpDownloader struct {
	w                http.ResponseWriter
	metaW            http.ResponseWriter
	contentType      string
	initiatedObjects map[string]bool
	mu               sync.Mutex
}

type metaResponseList struct {
	l   []*metaResponseBuffer
	mux sync.Mutex
}

func (m *metaResponseList) metaResponseFor(backupID string) (*metaResponseBuffer, error) {
	m.mux.Lock()
	defer m.mux.Unlock()

	if backupID == "" {
		return nil, fmt.Errorf("backup id cannot be empty")
	}

	// For each element in the list.
	for i, elem := range m.l {
		// Get the filename.
		fileName, err := fileName(elem.Header())
		if err != nil {
			return nil, fmt.Errorf("could not get file name from meta response buffer %d: %w", i, err)
		}
		// Check wheter the file name contains the backup id.
		if strings.Contains(fileName, fmt.Sprintf("%s%s", backupID, httpDownloaderFileNameSeparator)) {
			//  If it does, we return the respective element.
			return elem, nil
		}
	}

	// No element of the meta response list can respond for the given backup id.
	return nil, errNotFound
}

func (m *metaResponseList) add(in *metaResponseBuffer, deleteAfter time.Duration) error {
	m.mux.Lock()
	defer m.mux.Unlock()
	if in == nil {
		return fmt.Errorf("given meta response buffer cannot be nil")
	}

	// Fetch the filename from the meta response buffer. We need it for the
	// deletion routine below.
	fileName, err := fileName(in.Header())
	if err != nil {
		return fmt.Errorf("could not get file name from meta response buffer: %w", err)
	}

	// Make sure the list is initialized.
	if len(m.l) == 0 {
		m.l = make([]*metaResponseBuffer, 0)
	}

	// Append the meta response buffer to the list.
	m.l = append(m.l, in)

	// Start the deletion routine. It will delete the given meta response buffer
	// from the list after the given duration.
	go func() {
		// Create a new timer and wait for it to expire.
		if deleteAfter == 0 {
			deleteAfter = 10 * time.Minute
		}
		timer := time.NewTimer(deleteAfter)
		<-timer.C
		logrus.Infof("delete timer for meta response buffer for %s expired", fileName)
		// Try to delete the meta respose.
		err := m.delete(fileName)
		if err != nil {
			// It is possible that the meta response has already been deleted.
			// In that case we are also satisfied.
			if !errors.Is(err, errNotFound) {
				// If it could not be deleted for another reason, we log that
				// error but take no further actions.
				logrus.Errorf("could not delete meta response buffer for %s: %v", fileName, err)
			}
		}
	}()

	return nil
}

func (m *metaResponseList) delete(file string) error {
	m.mux.Lock()
	defer m.mux.Unlock()

	if file == "" {
		return fmt.Errorf("delete file name cannot be empty")
	}

	// We cannot delete from an empty list.
	if len(m.l) == 0 {
		return errNotFound
	}

	// Create a new list that contains anything but the element that should be
	// deleted.
	newList := make([]*metaResponseBuffer, 0, len(m.l)-1)
	deleteIdx := -1
	for i, elem := range m.l {
		elemName, err := fileName(elem.Header())
		if err != nil {
			return fmt.Errorf("could not get file name for element %d: %w", i, err)
		}
		if file == elemName {
			deleteIdx = i
		} else {
			newList = append(newList, elem)
		}
	}

	// If we did not find the element that should have been deleted, we return.
	if deleteIdx == -1 {
		return errNotFound
	}

	// We set the list that does not contain the "deleted" element. The
	// "deleted" element is no longer referenced and will thus be taken care of
	// by the garbage collection.
	m.l = newList
	return nil
}

type metaResponseBuffer struct {
	header  http.Header
	content []byte
}

// backupIDFromFileName returns an empty string if it is not possible to find
// the backup id inside the given filename.
func backupIDFromFileName(fileName string) string {
	backupID := ""
	parts := strings.Split(fileName, httpDownloaderFileNameSeparator)
	for i := range parts {
		if parts[i] == basebackupSuffix && i > 0 {
			backupID = parts[i-1]
		}
	}
	return backupID
}

// fileName returns the file name that is located in the header of this meta
// response buffer.
func fileName(header http.Header) (string, error) {
	if header == nil {
		return "", fmt.Errorf("header cannot be nil")
	}
	headerVal := header.Get(headerKeyContentDisposition)
	if !strings.HasPrefix(headerVal, headerValueContentDispositionPrefix) {
		return "", fmt.Errorf("expected header value %s to have prefix %s", headerVal, headerValueContentDispositionPrefix)
	}
	return strings.TrimPrefix(headerVal, headerValueContentDispositionPrefix), nil
}

// respond responds to the given response writer using the buffered header and
// content.
func (m metaResponseBuffer) respond(w http.ResponseWriter) (int, error) {
	for key, values := range m.header {
		for _, value := range values {
			w.Header().Add(key, value)
		}
	}
	return w.Write(m.content)
}

// Header is part of the http.ResponseWriter interface. It buffers the header.
func (m *metaResponseBuffer) Header() http.Header {
	if m.header == nil {
		m.header = make(http.Header)
	}
	return m.header
}

// Write is part of the http.ResponseWriter interface. It buffers the content.
func (m *metaResponseBuffer) Write(in []byte) (int, error) {
	if m.content == nil {
		m.content = make([]byte, 0)
	}
	m.content = append(m.content, in...)
	return len(in), nil
}

// WriteHeader is part of the http.ResponseWriter interface. It will never be
// called.
func (m *metaResponseBuffer) WriteHeader(statusCode int) {
	// Ignore input. Will never be called.
}

func newHTTPDownloader(w http.ResponseWriter) *httpDownloader {
	return &httpDownloader{
		w: w,
		metaW: &metaResponseBuffer{
			header: make(http.Header),
		},
		initiatedObjects: make(map[string]bool),
	}
}

func (h *httpDownloader) fetchMetaResponseBuffer() *metaResponseBuffer {
	return h.metaW.(*metaResponseBuffer)
}

// Upload sends content from in to the http requestor.
func (h *httpDownloader) Upload(ctx context.Context, objectName string, src io.Reader, tags map[string]string) (int64, error) {
	if h.contentType == "" {
		h.contentType = contentTypeTar
	}

	// Get the object name from the (possible) part.
	isPart := true
	objectNameWithoutPart, err := objectNameFromParts([]string{objectName})
	if err != nil {
		// If it faild because the object is not a part, note it and continue.
		if errors.Is(err, errNotPart) {
			isPart = false
			objectNameWithoutPart = objectName
		} else {
			return 0, fmt.Errorf("could not get object name from parts: %w", err)
		}
	}

	// Initially we assume that the response headers must be set.
	setHeaders := true

	// If the object is a part.
	if isPart {
		// We check whether it is the first time that we see a part of this
		// object by checking in the "initiated objects" map.
		h.mu.Lock()
		isContained := h.initiatedObjects[objectNameWithoutPart]
		h.mu.Unlock()

		// Afterwards we need to take a note in the "initiated objects" map that
		// we (at least now) saw a part of this object.
		h.mu.Lock()
		h.initiatedObjects[objectNameWithoutPart] = true
		h.mu.Unlock()

		// If we aready saw a part for this object.
		if isContained {
			// We don't need to set the headers again.
			setHeaders = false
		}

		// We don't want the part extension in the header.
		objectName = objectNameWithoutPart
	}

	// Either write to "direct" response writer or to buffered response writer
	// (for meta file).
	writer := h.w
	if strings.HasSuffix(objectNameWithoutPart, metaFileSuffix) {
		writer = h.metaW
	}

	if setHeaders {
		logrus.Infof("http downloader: writing header for file %s", objectNameWithoutPart)
		writer.Header().Set(headerKeyContentDisposition, fmt.Sprintf("%s%q", headerValueContentDispositionPrefix, strings.ReplaceAll(objectName, string(filepath.Separator), httpDownloaderFileNameSeparator)))
		writer.Header().Set(headerKeyContentType, contentTypeTar)
	}
	return io.Copy(writer, src)
}

func (h *httpDownloader) Merge(ctx context.Context, srcNames []string, targetName string, tags map[string]string) error {
	// We don't expect any further object parts for this object. Thus we can
	// delete this object from the "initiated objects" map if it is in there.
	h.mu.Lock()
	_, ok := h.initiatedObjects[targetName]
	if ok {
		delete(h.initiatedObjects, targetName)
	}
	h.mu.Unlock()

	return nil
}
