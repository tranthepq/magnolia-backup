package api

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/http/cookiejar"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"golang.org/x/net/publicsuffix"
	"golang.org/x/time/rate"
)

var (
	defaultUserAgent              = "mironet-http-client"
	defaultMediaType              = "application/json"
	backoffDefaultInitialInterval = backoff.DefaultInitialInterval
)

// Client is a generic HTTP API Client.
type Client struct {
	BaseURL   *url.URL // Base URL.
	UserAgent string   // User-Agent to use in HTTP request headers.

	client            *http.Client
	authorization     string           // Authorization: header.
	additionalHeaders http.Header      // Those headers are added automatically for each request.
	lim               RateLimiter      // A rate limiter to be used with all requests going through the client.
	metric            DurationObserver // Measures request duration from start to reading the last byte of the response.
	counter           RequestCounter   // Counts requests.
	notify            backoff.Notify   // Function to call on each retry.
	verbosity         int              // Log level mode.
	log               *log.Logger      // We log to here. If nil we log to os.Stderr with the default logger.
	retryOn           RetryOnFunc      // If f returns true, we should retry.
	marshal           Marshaler        // JSON as a default, can be replaced if needed.
	unmarshal         Unmarshaler      // JSON as a default, can be replaced if needed.
	mediaType         string           // The media type used for every request with this client.

	backoffInitialInterval time.Duration // Initial backoff retry interval.
	backoffMaxInterval     time.Duration // Max interval the backoff scales up to.
	backoffMaxElapsedTime  time.Duration // Max time we wait for the whole process to complete.
}

// Logging modes.
const (
	ModeInfo = iota // This is the default.
	ModeDebug
	ModeTrace
)

// Marshaler encodes API requests.
type Marshaler func(v interface{}) ([]byte, error)

// Unmarshaler decodes API responses.
type Unmarshaler func(data []byte, v interface{}) error

// Clone returns a new copy of the client with the options applied.
func (c *Client) Clone(opts ...Opt) (*Client, error) {
	cli, err := New()
	if err != nil {
		return nil, err
	}

	cli.BaseURL = c.BaseURL
	cli.UserAgent = c.UserAgent

	cli.authorization = c.authorization
	cli.additionalHeaders = c.additionalHeaders
	cli.client = c.client
	//cli.lim = c.lim
	cli.metric = c.metric
	cli.counter = c.counter
	cli.notify = c.notify
	cli.verbosity = c.verbosity
	cli.log = c.log
	cli.retryOn = c.retryOn
	cli.marshal = c.marshal
	cli.unmarshal = c.unmarshal
	cli.mediaType = c.mediaType

	cli.backoffInitialInterval = c.backoffInitialInterval
	cli.backoffMaxElapsedTime = c.backoffMaxElapsedTime
	cli.backoffMaxInterval = c.backoffMaxInterval

	for _, opt := range opts {
		if err := opt(cli); err != nil {
			return nil, err
		}
	}

	return cli, nil
}

// Opt is an option for the client.
type Opt func(c *Client) error

// WithBaseURL sets the base url for the client.
func WithBaseURL(rawurl string) Opt {
	return func(c *Client) error {
		u, err := url.Parse(rawurl)
		if err != nil {
			return err
		}
		c.BaseURL = u
		return nil
	}
}

// WithVerbosity sets the logging verbosity.
func WithVerbosity(verbosity int) Opt {
	return func(c *Client) error {
		c.verbosity = verbosity
		return nil
	}
}

// WithInsecure controls if TLS server certs should be verified. Default is
// false (= secure mode, TLS will be checked).
func WithInsecure(insecure bool) Opt {
	return func(c *Client) error {
		trsp, ok := c.client.Transport.(*http.Transport)
		if !ok {
			return fmt.Errorf("could not set insecure on non-standard HTTP client")
		}
		if trsp.TLSClientConfig == nil {
			trsp.TLSClientConfig = &tls.Config{
				InsecureSkipVerify: insecure,
			}
			return nil
		}
		trsp.TLSClientConfig.InsecureSkipVerify = insecure
		return nil
	}
}

// WithLogger sets the logger for this client to use. Default is to log to
// stdout with the standard logger.
func WithLogger(l *log.Logger) Opt {
	return func(c *Client) error {
		c.log = l
		return nil
	}
}

// WithHTTPClient sets the http.Client on the API client.
func WithHTTPClient(cli *http.Client) Opt {
	return func(c *Client) error {
		c.client = cli
		return nil
	}
}

// WithMaxConnsPerHost limits the maximum connections opened per target host.
// This resets the internal HTTP client.
func WithMaxConnsPerHost(n int) Opt {
	return func(c *Client) error {
		trsp, ok := c.client.Transport.(*http.Transport)
		if !ok {
			return fmt.Errorf("could not set MaxConnsPerHost on non-default transport")
		}
		trsp.MaxConnsPerHost = n
		return nil
	}
}

// DurationObserver observes how long requests take. This is intended primarily
// for Prometheus.
type DurationObserver interface {
	Start()
	ObserveDuration()
}

// RequestCounter counts requests. Primarily for use with Prometheus.
type RequestCounter func(*http.Response)

// WithDurationObserver sets a metric to be observed.
func WithDurationObserver(o DurationObserver) Opt {
	return func(c *Client) error {
		c.metric = o
		return nil
	}
}

// WithRequestCounter sets the request counter. All requests, even retries are
// counted.
func WithRequestCounter(f RequestCounter) Opt {
	return func(c *Client) error {
		c.counter = f
		return nil
	}
}

// WithAuthorization sets the Authorization header for this client. The header
// is injected on each request after setting it.
func WithAuthorization(key string) Opt {
	return func(c *Client) error {
		c.authorization = fmt.Sprintf("Bearer %s", key)
		return nil
	}
}

// WithBasicAuth sets basic auth headers for the request. Mutually exclusive
// with WithAuthorization.
func WithBasicAuth(username, password string) Opt {
	return func(c *Client) error {
		auth := username + ":" + password
		basic := base64.StdEncoding.EncodeToString([]byte(auth))
		c.authorization = "Basic " + basic
		return nil
	}
}

// RateLimiter rate-limits stuff.
type RateLimiter interface {
	Wait(context.Context) error // Call should block until token is available.
}

// WithRateLimiter sets an existing rate limiter on the client.
func WithRateLimiter(lim RateLimiter) Opt {
	return func(c *Client) error {
		c.lim = lim
		return nil
	}
}

// WithRateLimit sets the rate limit explicitly.
func WithRateLimit(hertz float64, burst int) Opt {
	return func(c *Client) error {
		ω := rate.Limit(hertz)
		c.lim = rate.NewLimiter(ω, burst)
		return nil
	}
}

// WithRetryOn sets the func deciding on which status code we should retry the
// original request.
func WithRetryOn(f RetryOnFunc) Opt {
	return func(c *Client) error {
		c.retryOn = f
		return nil
	}
}

// WithBackoffInitialInterval sets the initial backoff interval.
func WithBackoffInitialInterval(d time.Duration) Opt {
	return func(c *Client) error {
		c.backoffInitialInterval = d
		return nil
	}
}

// WithBackoffMaxInterval sets the maximum backoff interval.
func WithBackoffMaxInterval(d time.Duration) Opt {
	return func(c *Client) error {
		c.backoffMaxInterval = d
		return nil
	}
}

// WithBackoffMaxElapsedTime sets the maximum time before giving up.
func WithBackoffMaxElapsedTime(d time.Duration) Opt {
	return func(c *Client) error {
		c.backoffMaxElapsedTime = d
		return nil
	}
}

// WithMarshaler set the data marshaler for HTTP requests.
func WithMarshaler(f Marshaler) Opt {
	return func(c *Client) error {
		c.marshal = f
		return nil
	}
}

// WithUnmarshaler set the data unmarshaler for HTTP responses.
func WithUnmarshaler(f Unmarshaler) Opt {
	return func(c *Client) error {
		c.unmarshal = f
		return nil
	}
}

// RetryOnFunc received an HTTP response and an error in case the request
// failed. If it returns true, the request is retried later based on the
// client's backoff policy.
type RetryOnFunc func(*http.Response, error) bool

// RetryOn429 retries on HTTP 429 status code only and is the default.
var RetryOn429 = func(resp *http.Response, _ error) bool {
	return resp != nil && resp.StatusCode == http.StatusTooManyRequests
}

// ReturnCode creates functions to use with WithRetryOn.
func ReturnCode(want []int) RetryOnFunc {
	return func(resp *http.Response, e error) bool {
		if resp == nil {
			return false
		}
		for _, v := range want {
			if v == resp.StatusCode {
				return true
			}
		}
		return false
	}
}

// NotifyFunc gets called on each retry with the next interval before the client retries.
type NotifyFunc func(error, time.Duration)

// WithNotifyFunc sets the function to call on each request retry.
func WithNotifyFunc(f NotifyFunc) Opt {
	return func(c *Client) error {
		c.notify = backoff.Notify(f)
		return nil
	}
}

// WithAdditionalHeaders sets additional headers for each request. Existing
// headers are not overwritten.
func WithAdditionalHeaders(h http.Header) Opt {
	return func(c *Client) error {
		c.additionalHeaders = h
		return nil
	}
}

// WithMediaType sets the content and accepted media type (Content-type and
// Accept headers).
func WithMediaType(mt string) Opt {
	return func(c *Client) error {
		c.mediaType = mt
		return nil
	}
}

// See https://blog.cloudflare.com/the-complete-guide-to-golang-net-http-timeouts/
func newHTTPClient() *http.Client {
	c := &http.Client{
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).Dial,
			TLSHandshakeTimeout:   30 * time.Second,
			ResponseHeaderTimeout: 30 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			IdleConnTimeout:       90 * time.Second, // This is the default.
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: false,
			},
		},
	}
	return c
}

// New returns a new HTTP client instance.
func New(opts ...Opt) (*Client, error) {
	cookieJar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return nil, fmt.Errorf("error creating cookie jar: %v", err)
	}

	c := newHTTPClient()
	c.Jar = cookieJar

	cli := &Client{
		client:                 c,
		UserAgent:              defaultUserAgent,
		lim:                    rate.NewLimiter(rate.Inf, 0), // Infinite rate limiter as a default.
		retryOn:                RetryOn429,
		log:                    log.New(os.Stderr, "", log.LstdFlags),
		backoffInitialInterval: backoffDefaultInitialInterval,
		backoffMaxElapsedTime:  backoff.DefaultMaxElapsedTime,
		backoffMaxInterval:     backoff.DefaultMaxInterval,
		marshal:                json.Marshal,
		unmarshal:              json.Unmarshal,
		mediaType:              defaultMediaType,
	}

	for _, opt := range opts {
		if err := opt(cli); err != nil {
			return nil, err
		}
	}

	return cli, nil
}

type nopSeekCloser struct {
	io.ReadSeeker
}

// Close is a no-op.
func (n nopSeekCloser) Close() error {
	return nil
}

// ParseBasicAuth returns the username and password from a basic authentication
// string and ok if there was no error.
func ParseBasicAuth(auth string) (username, password string, ok bool) {
	const prefix = "Basic "
	if len(auth) < len(prefix) {
		return
	}
	decoded, err := base64.StdEncoding.DecodeString(auth[len(prefix):])
	if err != nil {
		return
	}
	creds := strings.Split(string(decoded), ":")
	if len(creds) != 2 {
		return
	}
	return creds[0], creds[1], true
}

// Client returns the configured HTTP client.
func (c *Client) Client() *http.Client {
	return c.client
}

// Authorization returns the raw authorization string used for HTTP calls.
func (c *Client) Authorization() string {
	return c.authorization
}

// NewRequest prepares a new request for sending over to the server. Specify
// relative urls always without the preceding slash. 'body' is encoded and sent
// as the request body. If body is an io.Reader its content is copied without
// parsing.
func (c *Client) NewRequest(ctx context.Context, method, urlStr string, body interface{}, opts ...Opt) (*http.Request, error) {
	for _, opt := range opts {
		if err := opt(c); err != nil {
			return nil, err
		}
	}

	var u *url.URL
	{
		rel, err := url.Parse(urlStr)
		if err != nil {
			return nil, err
		}
		u = c.BaseURL.ResolveReference(rel)
	}

	var (
		req *http.Request
		err error
	)

	if body != nil {
		// If body is an io.Reader we can use it directly, so don't encode.
		if v, ok := body.(io.Reader); !ok {
			buf, e := c.marshal(body)
			if e != nil {
				return nil, e
			}
			req, err = http.NewRequest(method, u.String(), bytes.NewReader(buf))
		} else {
			if _, ok := v.(io.Closer); ok {
				if v2, ok := v.(io.ReadSeeker); ok {
					// If the request body is a seeker and a closer, prevent
					// http.Client from closing it early.

					v = nopSeekCloser{v2}
				}
			}
			req, err = http.NewRequest(method, u.String(), v)
		}
	} else {
		req, err = http.NewRequest(method, u.String(), nil)
	}
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", c.mediaType)
	req.Header.Add("Accept", c.mediaType)
	req.Header.Add("User-Agent", c.UserAgent)

	if c.authorization != "" {
		req.Header.Add("Authorization", c.authorization)
	}

	// Mix in additional headers.
	for k, vv := range c.additionalHeaders {
		for _, v := range vv {
			req.Header.Add(k, v)
		}
	}

	return req, nil
}

// Do actually sends the request and stores the response body in v. If `v` is an
// `io.Writer` the contents of the response body are copied without parsing.
func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*http.Response, error) {
	var resp *http.Response
	var err error

	var op func() error
	var bo backoff.BackOff = func() backoff.BackOff {
		b := backoff.NewExponentialBackOff()
		b.InitialInterval = c.backoffInitialInterval
		b.MaxInterval = c.backoffMaxInterval
		b.MaxElapsedTime = c.backoffMaxElapsedTime
		return b
	}()

	switch body := req.Body.(type) {
	case io.ReadSeeker:
		op = func() error {
			resp, err = c.do(ctx, req, v)
			if c.retryOn != nil && c.retryOn(resp, err) {
				if _, err := body.Seek(0, 0); err != nil {
					return backoff.Permanent(err)
				}
				return err
			}
			return backoff.Permanent(err)
		}
	default:
		op = func() error {
			resp, err = c.do(ctx, req, v)
			if c.retryOn != nil && c.retryOn(resp, err) {
				return err
			}
			return backoff.Permanent(err)
		}
	}
	if c.retryOn == nil {
		bo = &backoff.StopBackOff{}
	}
	bo = backoff.WithContext(bo, ctx)

	// Now do the request with the assigned retry backoff policy.
	err = backoff.RetryNotify(op, bo, c.notify)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *Client) do(ctx context.Context, req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.doRequestWithClient(ctx, c.client, req)
	if err != nil {
		return nil, err
	}
	err = checkResponse(resp)
	if err != nil {
		return resp, err
	}

	defer func() {
		// Drain the body in case there's something in it, but we're not
		// interested.
		io.Copy(ioutil.Discard, resp.Body)
		// Always close the body from the response.
		resp.Body.Close()
	}()

	if v == nil {
		return resp, nil
	}

	if w, ok := v.(io.Writer); ok {
		_, err = io.Copy(w, resp.Body)
		if err != nil {
			return nil, fmt.Errorf("error while reading body from response: %w", err)
		}

		return resp, nil
	}

	// Unmarshal into v.
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %w", err)
	}
	if err := c.unmarshal(data, v); err != nil {
		return nil, fmt.Errorf("error while decoding response body: %w", err)
	}

	return resp, nil
}

// doRequestWithClient sends to request over the wire.
func (c *Client) doRequestWithClient(ctx context.Context, client *http.Client, req *http.Request) (*http.Response, error) {
	if c.metric != nil {
		c.metric.Start()
		defer c.metric.ObserveDuration()
	}
	if err := c.lim.Wait(ctx); err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if c.verbosity == ModeTrace {
		dump, err := httputil.DumpRequestOut(req, true)
		if err != nil {
			return nil, fmt.Errorf("error dumping outgoing request: %w", err)
		}

		c.log.Println("HTTP client request:")
		c.log.Print(string(dump))
	}
	resp, err := c.client.Do(req)
	if err != nil {
		// This is a Layer 1 to 4 error, not HTTP error.
		// It could also be the context that has been closed.
		return nil, fmt.Errorf("error while sending request via HTTP: %v", err)
	}
	if c.counter != nil {
		c.counter(resp)
	}
	if c.verbosity == ModeTrace {
		dump, err := httputil.DumpResponse(resp, true)
		if err != nil {
			return nil, fmt.Errorf("error dumping response: %w", err)
		}
		c.log.Println("HTTP server response:")
		c.log.Print(string(dump))
	}
	return resp, nil
}

// ErrorResponse holds the response in case of an API error.
type ErrorResponse struct {
	Response *http.Response
	RawBody  []byte
}

func (e ErrorResponse) Error() string {
	return fmt.Sprintf("error from API: return status = %s", e.Response.Status)
}

// CheckResponse checks the HTTP status code of the response and if it's in the
// 200s the request is considered error-free. In case it's not, the body will be
// read and parsed.
func checkResponse(r *http.Response) error {
	if c := r.StatusCode; c >= 200 && c <= 299 {
		return nil
	}
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return fmt.Errorf("error reading response body: %w", err)
	}
	return &ErrorResponse{RawBody: data, Response: r}
}
