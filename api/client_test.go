package api

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
	"time"
)

func Test_checkResponse(t *testing.T) {
	dummyBody := ioutil.NopCloser(bytes.NewBufferString(`{ "message": "uiuiuiuiui" }`))
	notJSONBody := ioutil.NopCloser(bytes.NewBufferString("{ uiuiuiuiuiui"))

	u, err := url.Parse("localhost")
	if err != nil {
		t.Fatal(err)
	}

	type args struct {
		r *http.Response
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "success",
			args: args{r: &http.Response{
				StatusCode: 202,
			}},
			wantErr: false,
		},
		{
			name: "error",
			args: args{r: &http.Response{
				StatusCode: 403,
				Body:       dummyBody,
				Request: &http.Request{
					Method: http.MethodGet,
					URL:    u,
				},
			}},
			wantErr: true,
		},
		{
			name: "error with non-JSON response",
			args: args{r: &http.Response{
				StatusCode: 401,
				Body:       notJSONBody,
				Request: &http.Request{
					Method: http.MethodGet,
					URL:    u,
				},
			}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var err error
			if err = checkResponse(tt.args.r); (err != nil) != tt.wantErr {
				t.Errorf("checkResponse() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err != nil {
				t.Log(err)
			}
		})
	}
}

const (
	defaultTestBaseURL = "http://localhost:9876"
)

func newClient(t *testing.T, opt ...Opt) *Client {
	opt = append([]Opt{WithBaseURL(defaultTestBaseURL)}, opt...)
	c, err := New(opt...)
	if err != nil {
		t.Fatal(err)
	}
	return c
}

func newRequest(ctx context.Context, t *testing.T, cli *Client, method, urlStr string, body interface{}, opts ...Opt) *http.Request {
	req, err := cli.NewRequest(ctx, method, urlStr, body, opts...)
	if err != nil {
		t.Fatal(err)
	}
	return req
}

func retryHandler(t *testing.T, n int, next http.HandlerFunc) http.HandlerFunc {
	i := 0
	var start time.Time
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() { i++ }()
		if i < n {
			if !start.IsZero() {
				t.Logf("stalling, time since last request: %s", time.Since(start))
			} else {
				t.Log("stalling")
			}
			start = time.Now()
			// Always read the body so we can test for it.
			defer r.Body.Close()
			m, err := io.Copy(ioutil.Discard, r.Body)
			if err != nil {
				t.Fatal(err)
			}
			t.Logf("%d bytes discarded", m)
			w.WriteHeader(http.StatusTooManyRequests)
			return
		}
		next(w, r)
	}
}

func init() {
	// Set the default initial interval very low for testing.
	backoffDefaultInitialInterval = time.Millisecond * 5
}

func TestClient_Do(t *testing.T) {

	cli1 := newClient(t)
	req1 := newRequest(context.TODO(), t, cli1, http.MethodGet, "/getstuff", nil)
	req2 := newRequest(context.TODO(), t, cli1, http.MethodPost, "/poststuff", bytes.NewReader([]byte("This is a test body.")))

	var req3body = struct {
		Schni string `json:"schni,omitempty"`
		Schna string `json:"schna,omitempty"`
	}{
		Schni: "Testing",
		Schna: "Testing2",
	}
	req3 := newRequest(context.TODO(), t, cli1, http.MethodPost, "/poststuff", req3body)

	content := []byte("This is a temp file's content.")
	tmpfile, err := ioutil.TempFile("", "TestClient_Do")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())
	defer tmpfile.Close()

	if _, err := tmpfile.Write(content); err != nil {
		t.Fatal(err)
	}
	if err := tmpfile.Sync(); err != nil {
		t.Fatal(err)
	}
	t.Logf("tmp file %s prepared", tmpfile.Name())
	req4 := newRequest(context.TODO(), t, cli1, http.MethodPost, "/poststuff", tmpfile)

	cli2 := newClient(t, WithBasicAuth("username", "p@sswUrd"))
	req5 := newRequest(context.TODO(), t, cli2, http.MethodGet, "/getstuff", nil)

	cli3 := newClient(t, WithAuthorization("schniiiiiiiiiiiiiiiiiii"), WithVerbosity(ModeTrace))
	req6 := newRequest(context.TODO(), t, cli3, http.MethodGet, "/getstuff-bearer", nil)

	var expectBytesHandler = func(expect int) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			n, err := io.Copy(ioutil.Discard, r.Body)
			defer r.Body.Close()
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			exp := int64(expect)
			if n != exp {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "read = %d bytes, expected = %d bytes", n, exp)
			}
		}
	}

	var checkAuthHeaderHandler = func(want string) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			got := r.Header.Get("Authorization")
			if got == want {
				return
			}
			t.Errorf("got authorization header = %s, want = %s", got, want)
			w.WriteHeader(http.StatusInternalServerError)
		}
	}

	var responseBody200Handler = func(body string, next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			if next != nil {
				next(w, r)
			}
			if _, err := w.Write([]byte(body)); err != nil {
				t.Fatal(err)
			}
		}
	}

	type args struct {
		ctx context.Context
		req *http.Request
		v   interface{}
	}
	tests := []struct {
		name    string
		cli     *Client
		args    args
		handler http.HandlerFunc
		wantErr bool
	}{
		{
			name: "get",
			cli:  newClient(t),
			args: args{
				req: req1,
				v:   nil,
			},
			handler: func(w http.ResponseWriter, r *http.Request) {
				t.Logf("new request from %s", r.UserAgent())
			},
			wantErr: false,
		},
		{
			name: "get, with backoff",
			cli:  newClient(t),
			args: args{
				req: req1,
				v:   nil,
			},
			handler: retryHandler(t, 5, func(w http.ResponseWriter, r *http.Request) {
				n, err := io.Copy(ioutil.Discard, r.Body)
				defer r.Body.Close()
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				fmt.Fprintf(w, "%d bytes read from request body", n)
			}),
			wantErr: false,
		},
		{
			name: "post io.Reader, with backoff",
			cli:  newClient(t),
			args: args{
				req: req2,
				v:   nil,
			},
			handler: retryHandler(t, 5, expectBytesHandler(20)),
			wantErr: false,
		},
		{
			name: "post struct, with backoff",
			cli:  newClient(t),
			args: args{
				req: req3,
				v:   nil,
			},
			handler: retryHandler(t, 5, expectBytesHandler(38)),
			wantErr: false,
		},
		{
			name: "post file content, with backoff",
			cli:  newClient(t),
			args: args{
				req: req4,
				v:   nil,
			},
			handler: retryHandler(t, 5, expectBytesHandler(30)),
			wantErr: false,
		},
		{
			name: "with basic authorization",
			cli:  cli2,
			args: args{
				req: req5,
				v:   nil,
			},
			handler: checkAuthHeaderHandler("Basic dXNlcm5hbWU6cEBzc3dVcmQ="),
			wantErr: false,
		},
		{
			name: "with bearer authorization",
			cli:  cli3,
			args: args{
				req: req6,
				v:   nil,
			},
			handler: checkAuthHeaderHandler("Bearer schniiiiiiiiiiiiiiiiiii"),
			wantErr: false,
		},
		{
			name: "read response body",
			cli:  cli3,
			args: args{
				req: req6,
				v:   &testLogWriter{t},
			},
			handler: responseBody200Handler(`This can be any string and does not have to be valid JSON because "v" is an io.Writer.`, nil),
			wantErr: false,
		},
		{
			name: "read json response body",
			cli:  cli3,
			args: args{
				req: req6,
				v:   &struct{ Message string }{},
			},
			handler: responseBody200Handler(`{ "message": "This should better be valid JSON" }`, nil),
			wantErr: false,
		},
		{
			name: "read illegal json response body",
			cli:  cli3,
			args: args{
				req: req6,
				v:   &struct{ Message string }{},
			},
			handler: responseBody200Handler(`{ "message": "This should better be valid JSON, but it's not" `, nil),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := httptest.NewServer(tt.handler)
			defer srv.Close()

			var err error
			tt.args.req.URL, err = url.Parse(srv.URL)
			if err != nil {
				t.Fatal(err)
			}
			if tt.args.ctx == nil {
				tt.args.ctx = context.TODO()
			}
			_, err = tt.cli.Do(tt.args.ctx, tt.args.req, tt.args.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("Client.Do() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

type testLogWriter struct {
	t *testing.T
}

func (w *testLogWriter) Write(p []byte) (n int, err error) {
	w.t.Log(string(p))
	return len(p), nil
}

func TestClient_CancelledContext(t *testing.T) {
	srv := httptest.NewServer(retryHandler(t, 10, func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))
	defer srv.Close()

	ctx, cancel := context.WithCancel(context.Background())
	// Cancel now. This is the point of this test.
	cancel()

	cli := newClient(t, WithBaseURL(srv.URL))
	req := newRequest(ctx, t, cli, http.MethodGet, "", nil)

	resp, err := cli.Do(ctx, req, nil)
	if err == nil {
		t.Errorf("context timeout expected, got no error, resp = %v", resp)
		return
	}
	if !errors.Is(err, context.Canceled) {
		t.Errorf("error got = %v, want = %v", err, context.Canceled)
		return
	}
	t.Logf("got expected error: %s", err)
}

func TestParseBasicAuth(t *testing.T) {
	type args struct {
		auth string
	}
	tests := []struct {
		name         string
		args         args
		wantUsername string
		wantPassword string
		wantOk       bool
	}{
		{
			name:         "valid username/password",
			args:         args{auth: "Basic dXNlcm5hbWU6cEBzc3dVcmQ="},
			wantUsername: "username",
			wantPassword: "p@sswUrd",
			wantOk:       true,
		},
		{
			name:         "empty username",
			args:         args{auth: "Basic OnBAc3N3VXJk"},
			wantUsername: "",
			wantPassword: "p@sswUrd",
			wantOk:       true,
		},
		{
			name:         "invalid basic auth string",
			args:         args{auth: "Basic huuuuuuuuuuuuuuiiiiiii"},
			wantUsername: "",
			wantPassword: "",
			wantOk:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotUsername, gotPassword, gotOk := ParseBasicAuth(tt.args.auth)
			if gotUsername != tt.wantUsername {
				t.Errorf("ParseBasicAuth() gotUsername = %v, want %v", gotUsername, tt.wantUsername)
			}
			if gotPassword != tt.wantPassword {
				t.Errorf("ParseBasicAuth() gotPassword = %v, want %v", gotPassword, tt.wantPassword)
			}
			if gotOk != tt.wantOk {
				t.Errorf("ParseBasicAuth() gotOk = %v, want %v", gotOk, tt.wantOk)
			}
		})
	}
}

type oddTransport struct{}

func (oddTransport) RoundTrip(*http.Request) (*http.Response, error) {
	return nil, nil // Bogus roundtripper.
}

func TestWithInsecure(t *testing.T) {
	type wantFunc func(opt Opt) error

	var checkTransportTLSConfig = func(want bool) wantFunc {
		return func(opt Opt) error {
			cli, err := New(opt)
			if err != nil {
				return err
			}
			trsp, ok := cli.client.Transport.(*http.Transport)
			if !ok {
				return fmt.Errorf("client has non-default transport")
			}
			if trsp.TLSClientConfig == nil {
				return fmt.Errorf("client transport tls config is nil: %v", trsp)
			}
			got := trsp.TLSClientConfig.InsecureSkipVerify
			if got != want {
				return fmt.Errorf("client transport tls insecure = %v, want = %v", got, want)
			}
			return nil
		}
	}

	var checkTransportTLSConfigClient = func(want bool, cli *Client) wantFunc {
		return func(opt Opt) error {
			trsp, ok := cli.client.Transport.(*http.Transport)
			if !ok {
				return fmt.Errorf("client has non-default transport")
			}
			if trsp.TLSClientConfig == nil {
				return fmt.Errorf("client transport tls config is nil: %v", trsp)
			}
			got := trsp.TLSClientConfig.InsecureSkipVerify
			if got != want {
				return fmt.Errorf("client transport tls insecure = %v, want = %v", got, want)
			}
			return nil
		}
	}
	cli1 := newClient(t)

	cli1.client.Transport = oddTransport{}

	type args struct {
		insecure bool
	}
	tests := []struct {
		name    string
		args    args
		want    wantFunc
		wantErr bool
	}{
		{
			name: "set insecure on default http transport",
			args: args{insecure: true},
			want: checkTransportTLSConfig(true),
		},
		{
			name: "unset insecure on default http transport",
			args: args{insecure: false},
			want: checkTransportTLSConfig(false),
		},
		{
			name:    "unexpected transport",
			args:    args{insecure: false},
			want:    checkTransportTLSConfigClient(false, cli1),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := WithInsecure(tt.args.insecure)
			if err := tt.want(got); (err != nil) != tt.wantErr {
				t.Errorf("WithInsecure() err = %v", err)
			}
		})
	}
}
