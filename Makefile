IMAGE_NAME=gitlab.com/mironet/magnolia-backup
COMPOSE_FILE=docker/docker-compose.yml
AZ_COMPOSE_FILE=docker/docker-compose-az.yml
GCS_COMPOSE_FILE=docker/docker-compose-gcs.yml
RESTORE_COMPOSE_FILE=docker/docker-compose-restore.yml
MULTI_COMPOSE_FILE=docker/docker-compose-multi.yml
DOCKER_FILE=docker/Dockerfile
DOCKER_FILE_POSTGRES=docker/Dockerfile.postgres
TARGET=mgnlbackup

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


all: clean build build-docker ## Clean and rebuild.

build: ## build the test program
	go build -o $(TARGET) -ldflags "-X main.version=$$(git describe --always)"

build-docker: export DOCKER_BUILDKIT = 1
build-docker: ## Build the Docker image.
	docker build --build-arg APP_VERSION=$$(git describe --always) -t $(IMAGE_NAME) -f $(DOCKER_FILE) .
	docker build -t test-postgres -f $(DOCKER_FILE_POSTGRES) .

down: ## Remove running Docker containers.
	docker-compose -f $(COMPOSE_FILE) down

down-gcs: ## Remove running Docker containers.
	docker-compose -f $(GCS_COMPOSE_FILE) down

down-multi: ## Remove running Docker containers.
	docker-compose -f $(MULTI_COMPOSE_FILE) down

clean-multi: down ## Clean up multistorage.
	docker volume rm -f docker_minio-data1 || exit 0
	docker volume rm -f docker_postgres-data1 || exit 0
	docker volume rm -f docker_postgres-archive1 || exit 0
	docker volume rm -f docker_minio-data2 || exit 0
	docker volume rm -f docker_postgres-data2 || exit 0
	docker volume rm -f docker_postgres-archive2 || exit 0

clean: down ## Clean up.
	rm -vf $(TARGET)
	docker volume rm -f docker_minio-data || exit 0
	docker volume rm -f docker_postgres-data || exit 0
	docker volume rm -f docker_postgres-archive || exit 0
	docker volume rm -f docker_postgres-scratch || exit 0

clean-restore: ## Clean up Restore (if still running)
	rm -vf $(TARGET)
	docker-compose -f $(RESTORE_COMPOSE_FILE) down -v || exit 0

clean-image: down ## Clean docker image too.
	docker image rm $(IMAGE_NAME) || exit 0

up: ## run docker image for manual testing.
	docker-compose -f $(COMPOSE_FILE) up

up-az: ## run docker image for manual azure testing.
	docker-compose -f $(AZ_COMPOSE_FILE) up

up-gcs: ## run docker image for manual testing.
	docker-compose -f $(GCS_COMPOSE_FILE) up

up-multi: ## run docker image for multisource testing.
	docker-compose -f $(MULTI_COMPOSE_FILE) up

up-restore: ## restore bundle for local testing.
	docker-compose -f $(RESTORE_COMPOSE_FILE) up

start: ## run docker images in background.
	docker-compose -f $(COMPOSE_FILE) up -d postgres minio

start-gcs: ## run docker images in background.
	docker-compose -f $(GCS_COMPOSE_FILE) up -d postgres

logs: ## show logs
	docker-compose -f $(COMPOSE_FILE) logs -f

list-deps: ## list all imported packages
	go list -f '{{ join .Imports "\n" }}' ./...

gen-certs: ## generate self-signed CA and client certs for mTLS development.
	docker run -v $$(pwd)/testdata/tls:/certs -e SSL_DNS=localhost,pod-1.localhost -e CA_EXPIRE=3600 -e SSL_EXPIRE=1800 paulczar/omgwtfssl
	sudo chown -R 70:70 testdata/tls

test: ## run all tests.
	go vet $$(go list ./... | grep -v /vendor/)
	AZURE_STORAGE_ACCOUNT_NAME=magnoliabackuptest1 AZURE_STORAGE_ACCOUNT_KEY=c3VwZXJzZWNyZXQK \
		go test -race $$(go list ./... | grep -v /vendor/) -count 1