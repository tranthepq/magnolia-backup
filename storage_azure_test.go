package main

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	azto "github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blockblob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/sas"
	"github.com/go-test/deep"
	"gopkg.in/dnaeon/go-vcr.v3/cassette"
	"gopkg.in/dnaeon/go-vcr.v3/recorder"
)

func getAzureCred(t *testing.T) *azblob.SharedKeyCredential {
	accountName, accountKey := os.Getenv("AZURE_STORAGE_ACCOUNT_NAME"), os.Getenv("AZURE_STORAGE_ACCOUNT_KEY")
	if accountName == "" {
		t.Fatalf("could not find AZURE_STORAGE_ACCOUNT_NAME env var")
	}
	if accountKey == "" {
		t.Fatalf("could not find AZURE_STORAGE_ACCOUNT_KEY env var")
	}
	cred, err := azblob.NewSharedKeyCredential(accountName, accountKey)
	if err != nil {
		t.Fatal(err)
	}
	return cred
}

func getAzureAccountURL(t *testing.T) string {
	accountName := os.Getenv("AZURE_STORAGE_ACCOUNT_NAME")
	if accountName == "" {
		t.Fatalf("could not find AZURE_STORAGE_ACCOUNT_NAME env var")
	}
	return fmt.Sprintf(azAccountURLTpl, accountName)
}

func getAzureBlobClient(t *testing.T) *azblob.Client {
	cred := getAzureCred(t)
	cli, err := azblob.NewClientWithSharedKeyCredential(getAzureAccountURL(t), cred, nil)
	if err != nil {
		t.Fatal(err)
	}
	return cli
}

const (
	pathFixtures = "testdata/fixtures"
)

type recorderStopFunc func() error

func getAzureBlobClientWithVCR(t *testing.T, filename string, matcher cassette.MatcherFunc) (*azblob.Client, recorderStopFunc) {
	rec, err := recorder.New(filepath.Join(pathFixtures, filename))
	if err != nil {
		t.Fatal(err)
	}
	if matcher != nil {
		rec.SetMatcher(matcher)
	}
	// Add a hook which removes Authorization headers from all requests
	hook := func(i *cassette.Interaction) error {
		delete(i.Request.Headers, "Authorization")
		return nil
	}
	rec.AddHook(hook, recorder.AfterCaptureHook)

	cred := getAzureCred(t)
	opts := &azblob.ClientOptions{}
	opts.Transport = rec.GetDefaultClient()
	cli, err := azblob.NewClientWithSharedKeyCredential(getAzureAccountURL(t), cred, opts)
	if err != nil {
		t.Fatal(err)
	}
	return cli, rec.Stop
}

var (
	testContainerName       = os.Getenv("AZURE_STORAGE_CONTAINER_NAME")
	explorationTestsEnabled = os.Getenv("AZURE_STORAGE_EXPLORATION_TESTS") // Only run these if env var is set.
)

func Test_AzureCreateContainer(t *testing.T) {
	if explorationTestsEnabled != "enabled" {
		return
	}

	cli := getAzureBlobClient(t)
	resp, err := cli.CreateContainer(context.Background(), testContainerName, nil)
	if err != nil {
		if !bloberror.HasCode(err, bloberror.ContainerAlreadyExists) {
			t.Fatal(err)
		}
		t.Logf("container %s already exists", testContainerName)
		return
	}
	t.Logf("created container, response: %v", resp)
}

func Test_AzureUpload(t *testing.T) {
	if explorationTestsEnabled != "enabled" {
		return
	}

	cli := getAzureBlobClient(t)
	blobName := "superblob.txt"
	metadata := map[string]*string{
		"schni":    azto.Ptr("schna"),
		"schnappi": azto.Ptr("schnuepp"),
	}
	r := strings.NewReader(randString(5*Mi, alpha+digit))
	resp, err := cli.UploadStream(context.Background(), testContainerName, blobName, r, &azblob.UploadStreamOptions{
		Metadata: metadata,
		HTTPHeaders: &blob.HTTPHeaders{
			BlobContentType: azto.Ptr("application/x-tar"),
			//BlobContentEncoding: azto.Ptr("gzip"),
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}

// TODO(mathias.seiler): This is just me finding out how we could server-side
// copy from multiple blobs to a new single blob. It feels uncomfortable doing
// it this way, but that's the best I could come up with for now.
func Test_AzureRangeURL(t *testing.T) {
	if explorationTestsEnabled != "enabled" {
		return
	}

	cli := getAzureBlobClient(t)
	sourceName := "superblob.txt"
	targetName := "target.txt"

	startTime := time.Now().Add(-5 * time.Minute) // Allow for clock skew.
	// We need to get SAS URLs for every "chunk" we're uploading to block blob.
	// A chunk cannot be larger than 100 MiB according to Microsoft
	// documentation
	// (https://learn.microsoft.com/en-us/rest/api/storageservices/put-block-from-url#remarks).
	// blockblob.MaxStageBlockBytes says 4 GiB. We probably want to be smaller
	// than that anyway to show some progress along the way.
	scli := cli.ServiceClient().NewContainerClient(testContainerName).NewBlobClient(sourceName)
	prop, err := scli.GetProperties(context.Background(), nil)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("source is %s", ByteCountBinary(*prop.ContentLength))
	source, err := scli.GetSASURL(sas.BlobPermissions{
		Read: true,
	}, startTime.AddDate(0, 0, 7), &blob.GetSASURLOptions{
		StartTime: &startTime,
	})
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("SAS URL: %s", source)

	// We prepare the url for a new blob, the target blob.
	target := cli.ServiceClient().NewContainerClient(testContainerName).NewBlockBlobClient(targetName)

	// Every transferred block needs to be commited at the end.
	var blocklist []string
	// Block IDs are weird. They all must be base64 encoded (url encoding) and
	// need to have the same *length* before enconding. The original string
	// which has been base64 encoded cannot be larger than 64 bytes (not
	// chars!). ULID is probably an ok choice here.
	blockid := base64.URLEncoding.EncodeToString([]byte(MustULID(startTime)))
	blocklist = append(blocklist, blockid)

	// Now call PutFromURL (why on earth is it called "Stage..." instead of the
	// REST API call "Put..." ?). If it does not exist, the blob is created with
	// a length of 0.
	resp, err := target.StageBlockFromURL(context.Background(), blockid, source, &blockblob.StageBlockFromURLOptions{
		// This is where the magic happens. We need to split the original object
		// into sections of ranges and call PutFromURL on each section. The last
		// "chunk" has probably a different size than the other chunks. When
		// copying Azure actually requests this byte range from the origin
		// server. In our case that's Azure itself which supports Range headers.
		// TODO(mathias.seiler): Find out how to make this retryable.
		Range: blob.HTTPRange{
			Offset: 0,
			Count:  50,
		},
	})
	if err != nil {
		t.Fatal(err)
		return
	}
	_ = resp

	// After all chunks have been transferred, commit the new blob ...
	_, err = target.CommitBlockList(context.Background(), blocklist, nil)
	if err != nil {
		t.Fatal(err)
	}

	// ... and set a few useful headers.
	_, err = target.SetHTTPHeaders(context.Background(), blob.HTTPHeaders{
		BlobContentType:     azto.Ptr("application/text"),
		BlobContentEncoding: azto.Ptr("plain"),
	}, nil)
	if err != nil {
		t.Fatal(err)
	}
}

func Test_azureStorage_Merge(t *testing.T) {
	testingNoDelete = true    // Disable deletion after successful merge for testing.
	azureMergeChunkSize = 128 // We use small chunk sizes for testing (so go-vcr can record sensibly).
	cli1, stop1 := getAzureBlobClientWithVCR(t, "merge", func(r *http.Request, i cassette.Request) bool {
		u, err := url.ParseRequestURI(i.URL)
		if err != nil {
			t.Fatal(err)
		}
		// We only match the request path, not the whole url because block IDs
		// will be different each time an upload happens.
		return r.Method == i.Method && r.URL.RawPath == u.RawPath
	})

	type fields struct {
		containerName string
		prefix        string
		lifecycle     Lifecycler
		cli           *azblob.Client
	}
	type args struct {
		ctx        context.Context
		srcNames   []string
		targetName string
		tags       map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		stop    recorderStopFunc
		wantErr bool
	}{
		{
			name: "two blobs into one",
			fields: fields{
				containerName: testContainerName,
				cli:           cli1,
			},
			args: args{
				ctx: context.Background(),
				srcNames: []string{
					"de-bello-gallico1.txt",
					"de-bello-gallico2.txt",
				},
				targetName: "de-bello-gallico3.txt",
				tags:       map[string]string{"origin": "cluster-A", "backup_id": "testing"},
			},
			stop: stop1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.stop != nil {
				defer tt.stop()
			}
			as := &azureStorage{
				containerName: tt.fields.containerName,
				prefix:        tt.fields.prefix,
				lifecycle:     tt.fields.lifecycle,
				cli:           tt.fields.cli,
			}
			if err := as.Merge(tt.args.ctx, tt.args.srcNames, tt.args.targetName, tt.args.tags); (err != nil) != tt.wantErr {
				t.Errorf("azureStorage.Merge() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_azureStorage_Upload(t *testing.T) {
	cli1, stop1 := getAzureBlobClientWithVCR(t, "upload", func(r *http.Request, i cassette.Request) bool {
		u, err := url.ParseRequestURI(i.URL)
		if err != nil {
			t.Fatal(err)
		}
		// We only match the request path, not the whole url because block IDs
		// will be different each time an upload happens.
		return r.Method == i.Method && r.URL.RawPath == u.RawPath
	})

	type fields struct {
		containerName string
		prefix        string
		lifecycle     Lifecycler
		cli           *azblob.Client
	}
	type args struct {
		ctx        context.Context
		objectName string
		src        io.Reader
		tags       map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		stop    recorderStopFunc
		want    int64
		wantErr bool
	}{
		{
			name: "upload of random chars",
			fields: fields{
				containerName: testContainerName,
				cli:           cli1,
			},
			args: args{
				ctx:        context.Background(),
				objectName: "random-chars.bin",
				src:        io.LimitReader(rand.Reader, 1*Mi),
				tags:       map[string]string{"origin": "cluster-A", "backup_id": "testing"},
			},
			stop:    stop1,
			want:    1 * Mi,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.stop != nil {
				defer tt.stop()
			}
			as := &azureStorage{
				containerName: tt.fields.containerName,
				prefix:        tt.fields.prefix,
				lifecycle:     tt.fields.lifecycle,
				cli:           tt.fields.cli,
			}
			got, err := as.Upload(tt.args.ctx, tt.args.objectName, tt.args.src, tt.args.tags)
			if (err != nil) != tt.wantErr {
				t.Errorf("azureStorage.Upload() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("azureStorage.Upload() = %v, want %v", got, tt.want)
			}
		})
	}
}

const (
	timeFormatISO8601 = "2006-01-02 15:04:05 -0700 MST"
)

func timeMustParse(layout, in string) time.Time {
	t, err := time.Parse(layout, in)
	if err != nil {
		panic(err)
	}
	return t
}

func Test_azureStorage_List(t *testing.T) {
	cli1, stop1 := getAzureBlobClientWithVCR(t, "list", nil)

	type fields struct {
		containerName string
		prefix        string
		lifecycle     Lifecycler
		cli           *azblob.Client
	}
	type args struct {
		ctx  context.Context
		opts []ListOpt
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		stop    recorderStopFunc
		want    backupItemList
		wantErr bool
	}{
		{
			name: "simple list",
			fields: fields{
				containerName: "9fe4200c9f3630e1-backup",
				cli:           cli1,
			},
			args: args{
				ctx:  context.Background(),
				opts: nil,
			},
			stop: stop1,
			want: backupItemList{
				&backupItem{
					Key:          "myprefix/01GRRVM0YPT64BBWJPCTZAQA2N/basebackup/01GRRVM0YPT64BBWJPCW3CDCMV.tar.gz",
					LastModified: timeMustParse(timeFormatISO8601, "2023-02-08 15:52:14 +0000 GMT"),
					Size:         3876542,
					Tags: map[string]string{
						"backup_id":     "01GRRVM0YPT64BBWJPCTZAQA2N",
						"is_basebackup": "true",
						"namespace":     "development",
						"pod_name":      "mysuperpod-author",
						"variant":       "tx_log_archiving",
					},
					BucketName: "9fe4200c9f3630e1-backup",
				},
				&backupItem{
					Key:          "myprefix/01GRRVM0YPT64BBWJPCTZAQA2N/txlog/01GRRVMDC913KAQVGZZ2H7CPW3.tar.gz",
					LastModified: timeMustParse(timeFormatISO8601, "2023-02-08 15:53:00 +0000 GMT"),
					Size:         16545,
					Tags: map[string]string{
						"backup_id":     "01GRRVM0YPT64BBWJPCTZAQA2N",
						"is_basebackup": "false",
						"is_txlog":      "true",
						"namespace":     "development",
						"pod_name":      "mysuperpod-author",
						"variant":       "tx_log_archiving",
					},
					BucketName: "9fe4200c9f3630e1-backup",
				},
				&backupItem{
					Key:          "myprefix/01GRRVM0YPT64BBWJPCTZAQA2N/basebackup/01GRRVMEB93M202P0C3H4VK0GN-meta.tar.gz",
					LastModified: timeMustParse(timeFormatISO8601, "2023-02-08 15:52:13 +0000 GMT"),
					Size:         269,
					Tags: map[string]string{
						"backup_id":          "01GRRVM0YPT64BBWJPCTZAQA2N",
						"is_basebackup_meta": "true",
						"namespace":          "development",
						"pod_name":           "mysuperpod-author",
						"variant":            "tx_log_archiving",
					},
					BucketName: "9fe4200c9f3630e1-backup",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.stop != nil {
				defer tt.stop()
			}
			as := &azureStorage{
				containerName: tt.fields.containerName,
				prefix:        tt.fields.prefix,
				lifecycle:     tt.fields.lifecycle,
				cli:           tt.fields.cli,
			}
			got, err := as.List(tt.args.ctx, tt.args.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("azureStorage.List() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// Remove links because they are autogenerated each time.
			for k, v := range got {
				v.Link = nil
				got[k] = v
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Errorf("azureStorage.List() diff = %v", diff)
			}
		})
	}
}

func Test_toptr(t *testing.T) {
	type args struct {
		in map[string]string
	}
	tests := []struct {
		name string
		args args
		want map[string]*string
	}{
		{
			name: "test",
			args: args{
				in: map[string]string{
					"foo":  "bar",
					"baz":  "qux",
					"quux": "corge",
				},
			},
			want: map[string]*string{
				"foo":  azto.Ptr("bar"),
				"baz":  azto.Ptr("qux"),
				"quux": azto.Ptr("corge"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := toptr(tt.args.in)
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}

func Test_azureStorage_Tag(t *testing.T) {
	cli1, stop1 := getAzureBlobClientWithVCR(t, "tag", nil)

	type fields struct {
		containerName string
		prefix        string
		lifecycle     Lifecycler
		cli           *azblob.Client
	}
	type args struct {
		ctx        context.Context
		objectName string
		tags       map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		stop    recorderStopFunc
		wantErr bool
	}{
		{
			name: "tag is_txlog false",
			fields: fields{
				containerName: "9fe4200c9f3630e1-backup",
				cli:           cli1,
			},
			args: args{
				ctx:        context.Background(),
				objectName: "myprefix/01GRVB2E2CX80AVAYBHFXTB286/txlog/01GRVB2RXVNZ7PNMBZRQ3M6B1X.tar.gz",
				tags: map[string]string{
					"is_txlog": "false",
				},
			},
			stop: stop1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.stop != nil {
				defer tt.stop()
			}
			as := &azureStorage{
				containerName: tt.fields.containerName,
				prefix:        tt.fields.prefix,
				lifecycle:     tt.fields.lifecycle,
				cli:           tt.fields.cli,
			}
			if err := as.Tag(tt.args.ctx, tt.args.objectName, tt.args.tags); (err != nil) != tt.wantErr {
				t.Errorf("azureStorage.Tag() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
