package main

import (
	"archive/tar"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/url"
	"path"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/hashicorp/go-uuid"
	"github.com/klauspost/compress/gzip"
	"github.com/oklog/ulid/v2"
	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/magnolia-backup/tags"
)

const (
	tagKeyIsBaseBackup     = "is_basebackup"
	tagKeyIsBaseBackupMeta = "is_basebackup_meta"
	tagKeyIsTxLog          = "is_txlog"
	tagKeyBackupID         = "backup_id"
	tagKeyExpiry           = "expiry"
	tagKeyNamespace        = "namespace"
	tagKeyPodName          = "pod_name"
)

type backupItemList []*backupItem

// Sort sorts the backup item list according to the given arguments. If the
// given orderBy string is empty, the list is ordered by ULID. If orderBy is
// specified the list is order by the tag of that name. If the given order
// string is empty, no sorting is performed.
func (l *backupItemList) Sort(orderBy, order string) error {
	// If the given order string is empty, we return without sorting and without
	// error.
	if order == "" {
		return nil
	}

	// Return early if an unknown order is requested.
	switch order {
	case sortOrderAscending, sortOrderDescending:
	default:
		return fmt.Errorf("unknown sort order %s requested", order)
	}

	// If no tag to order by is specified, we order by ulid.
	if orderBy == "" {
		switch order {
		case sortOrderAscending:
			sort.Sort(byULID(*l))
		case sortOrderDescending:
			sort.Sort(sort.Reverse(byULID(*l)))
		}
		return nil
	}

	// If a tag is specified, we order by this tag. (Empty strings are always
	// last in the order.)
	sort.Slice(*l, func(i int, j int) bool {
		tagValueI := (*l)[i].Tags[orderBy]
		tagValueJ := (*l)[j].Tags[orderBy]
		// Empty strings are always last in the order.
		if tagValueI == "" {
			return false
		}
		if tagValueJ == "" {
			return true
		}
		switch order {
		case sortOrderAscending:
			return tagValueI < tagValueJ
		case sortOrderDescending:
			return tagValueI > tagValueJ
		default:
			// Should never reach here.
			return false
		}
	})

	return nil
}

// storagePrefix returns the storage prefix (if there is one) or an empty
// string. If there are inconsistent storage prefixes (or some items have a
// storage prefix while others do not) an error is returned.
func (l *backupItemList) storagePrefix() (string, error) {
	outStorName := ""
	for i, elem := range *l {
		storName, _, _ := splitObjectName(elem.Key)
		if i == 0 {
			outStorName = storName
		}
		if outStorName != storName {
			return "", fmt.Errorf("encountered inconsistent storage names: %s and %s", outStorName, storName)
		}
	}
	if outStorName != "" {
		outStorName = outStorName + storageNameSeparator
	}
	return outStorName, nil
}

func (l backupItemList) totalSize() int64 {
	out := int64(0)
	for _, v := range l {
		if v == nil {
			continue
		}
		out = out + v.Size
	}
	return out
}

func (l backupItemList) mergeTags() (map[string]string, error) {
	out := make(map[string]string)
	for i, v := range l {
		if v.Tags[tagKeyIsTxLog] != "true" {
			return nil, fmt.Errorf("list item %d is not a tx log", i)
		}

		// Use the tags of the first list element as tags of the merged item.
		if i == 0 {
			for key, value := range v.Tags {
				out[key] = value
			}
			continue
		}

		// For all further elements check if the base backup id and the expiry
		// are matching.
		if out[tagKeyBackupID] != v.Tags[tagKeyBackupID] {
			return nil, fmt.Errorf("found conflicting base backup ids %d&%d: %s!=%s", 0, i, out[tagKeyBackupID], v.Tags[tagKeyBackupID])
		}
		if out[tagKeyExpiry] != v.Tags[tagKeyExpiry] {
			return nil, fmt.Errorf("found conflicting expiries %d&%d: %s!=%s", 0, i, out[tagKeyExpiry], v.Tags[tagKeyExpiry])
		}
	}
	return out, nil
}

func (l backupItemList) mergeName() (string, error) {
	if len(l) < 1 {
		return "", fmt.Errorf("expected at least one element in backup item list")
	}
	if l[0].Tags[tagKeyIsTxLog] != "true" {
		return "", fmt.Errorf("expected first element of backup item list to be a tx log")
	}

	// Get the ulid from the backup item.
	txlogULID, err := l[0].ULID()
	if err != nil {
		return "", fmt.Errorf("could not get ulid from backup item: %w", err)
	}

	// Get a new ulid referring to the same time as the first item in the backup
	// item list.
	newULID := MustULID(ulid.Time(txlogULID.Time()))

	// Return the merge name. It is the name of the first element in the backup
	// item list, but with a new ulid.
	return filepath.Join(filepath.Dir(l[0].Key), fmt.Sprintf("%s.%s", newULID, fileExtensionTarGz)), nil
}

// backupItem is a single SQL dump on object storage.
type backupItem struct {
	Key          string            `json:"name,omitempty"`
	LastModified time.Time         `json:"last_modified,omitempty"`
	Size         int64             `json:"size,omitempty"`
	Link         *url.URL          `json:"-"`
	Daily        bool              `json:"daily,omitempty"`
	Weekly       bool              `json:"weekly,omitempty"`
	Monthly      bool              `json:"monthly,omitempty"`
	Tags         map[string]string `json:"tags,omitempty"`
	BucketName   string            `json:"-"`
}

func (b backupItem) String() string {
	return fmt.Sprintf("name = %s, date = %s", b.Key, b.LastModified)
}

// ULID returns the ULID in the base of the object's key.
func (b backupItem) ULID() (ulid.ULID, error) {
	return ULID(b.Key)
}

// TimeFromKey returns the time in the base of the object's key if the key is
// not of ULID type.
func (b backupItem) TimeFromKey() (time.Time, error) {
	return TimeFromKey(b.Key)
}

// TimeFromKey returns the time in the base of the object key if the key is
// not of ULID type.
func TimeFromKey(objectKey string) (time.Time, error) {
	// Extract the base of the item key.
	base := path.Base(objectKey)

	// Check whether the base has at least the expected lenght. If it has not,
	// the item name is not as expected.
	timeFormatSuffix := timeFormat + ".gz"
	timeFormatSuffixLen := len(timeFormatSuffix)
	if len(base) < timeFormatSuffixLen {
		return time.Time{}, fmt.Errorf(
			"items key is to short (len=%d) to contain the time format suffix of form %s",
			timeFormatSuffixLen, timeFormatSuffix,
		)
	}

	// Extract the time format suffix (without the file extension).
	gzSuffixLen := len(".gz")
	timeStr := base[len(base)-timeFormatSuffixLen : len(base)-gzSuffixLen]

	// Parse the time format strings .
	return time.Parse(timeFormat, timeStr)
}

func ULID(objectKey string) (ulid.ULID, error) {
	base := path.Base(objectKey)
	if len(base) < 26 {
		return [16]byte{}, fmt.Errorf("could not read ulid from object key, at least 26 chars needed")
	}
	base = base[:26]
	return ulid.ParseStrict(base)
}

func UUID(objectKey string) ([]byte, error) {
	base := path.Base(objectKey)
	if len(base) < 36 {
		return nil, fmt.Errorf("could not read uuid from object key, at least 36 chars needed")
	}
	base = base[:36]
	return uuid.ParseUUID(base)
}

func (b backupItem) BaseBackupULID() (ulid.ULID, error) {
	ulidStr := path.Base(path.Dir(path.Dir(b.Key)))
	if len(ulidStr) < 26 {
		return [16]byte{}, fmt.Errorf("could not read ulid from object key, at least 26 chars needed")
	}
	ulidStr = ulidStr[:26]
	return ulid.ParseStrict(ulidStr)
}

// TimeFromUlidKey returns the timestamp encoded as ulid in the object's name.
func (b backupItem) TimeFromUlidKey() (time.Time, error) {
	id, err := b.ULID()
	if err != nil {
		return time.Time{}, err
	}
	return ulid.Time(id.Time()), nil
}

// byTimeFromKey sorts by the date-time contained in the item name.
type byTimeFromKey []*backupItem

func (a byTimeFromKey) Len() int      { return len(a) }
func (a byTimeFromKey) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a byTimeFromKey) Less(i, j int) bool {
	timeI, err := a[i].TimeFromKey()
	if err != nil {
		return false
	}
	timeJ, err := a[j].TimeFromKey()
	if err != nil {
		return false
	}
	return timeI.Before(timeJ)
}

// Format should contain a ULID in the object's name.
type byULID []*backupItem

func (a byULID) Len() int      { return len(a) }
func (a byULID) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a byULID) Less(i, j int) bool {
	// Extract both backup ids from the backup item keys.
	backupIDi := path.Base(path.Dir(path.Dir(a[i].Key)))
	backupIDj := path.Base(path.Dir(path.Dir(a[j].Key)))

	// If the backup ids are identical ...
	if backupIDi == backupIDj {
		// ... the base of the path (containing the object's ulid) is used to
		// decide the order.
		return path.Base(a[i].Key) < path.Base(a[j].Key)
	}

	// Otherwise the backup ids are used to decide the order.
	return backupIDi < backupIDj
}

func (a byULID) HasBaseBackup() bool {
	type bb struct {
		hasBasebackup bool
		hasMeta       bool
	}
	m := make(map[string]*bb)

	// Loop through all backup items.
	for _, v := range a {
		// Fetch the backup id.
		id, ok := v.Tags[tagKeyBackupID]
		if !ok {
			// Ignore items that don't have a backup id.
			continue
		}

		// Make sure we don't get a nil pointer panic.
		ptr, ok := m[id]
		if !ok || ptr == nil {
			m[id] = &bb{}
		}

		// If we are looking at a meta file ...
		if v.Tags[tagKeyIsBaseBackupMeta] == "true" {
			// ... and have already seen the corresponding basebackup file ...
			if m[id].hasBasebackup {
				// ... we can conclude that there exists a complete basebackup.
				return true
			}
			// Make a note that we have seen the meta file for the current id.
			m[id].hasMeta = true
		}

		// If we are looking at a basebackup file ...
		if v.Tags[tagKeyIsBaseBackup] == "true" {
			// ... and have already seen the corresponding meta file ...
			if m[id].hasMeta {
				// ... we can conclude that there exists a complete basebackup.
				return true
			}
			// Make a note that we have seen the basebackup file for the current
			// id.
			m[id].hasBasebackup = true
		}
	}
	return false
}

// Don't render the complete url.URL object, just the string representation of
// it.
func (b *backupItem) MarshalJSON() ([]byte, error) {
	type alias backupItem
	if b.Link == nil {
		b.Link = &url.URL{}
	}
	return json.Marshal(&struct {
		Link string `json:"link,omitempty"`
		*alias
	}{
		Link:  b.Link.String(),
		alias: (*alias)(b),
	})
}

// UnmarshalJSON handles explicit URLs automatically.
func (b *backupItem) UnmarshalJSON(data []byte) error {
	type Alias backupItem
	type wrap struct {
		*Alias
		Link string `json:"link,omitempty"`
	}
	var it wrap
	if err := json.Unmarshal(data, &it); err != nil {
		return fmt.Errorf("backupItem: %w", err)
	}
	u, err := url.Parse(it.Link)
	if err != nil {
		return fmt.Errorf("backupItem: %w", err)
	}
	it.Alias.Link = u
	*b = backupItem(*it.Alias)
	return nil
}

func (b *backupItem) parseBackupMeta(ctx context.Context, getter Getter) (backupMeta, error) {
	if b == nil {
		return backupMeta{}, fmt.Errorf("backup item cannot be nil")
	}
	if !strings.HasSuffix(b.Key, metaFileSuffix) {
		return backupMeta{}, fmt.Errorf("backup item %s is not a meta item", b.Key)
	}

	// Get the base backup ulid for this item.
	baseULID, err := b.BaseBackupULID()
	if err != nil {
		return backupMeta{}, fmt.Errorf("could not get basebackup ulid: %w", err)
	}

	// Get the content of this object.
	rc, err := getter.Get(ctx, b.Key)
	if err != nil {
		return backupMeta{}, fmt.Errorf("could not get object %s: %w", b.Key, err)
	}

	// Unzip the item (while streaming).
	zr, err := gzip.NewReader(rc)
	if err != nil {
		return backupMeta{}, fmt.Errorf("expected gzip-compressed content in object %s: %w", b.Key, err)
	}
	defer zr.Close()
	tr := tar.NewReader(zr)
	var walSegment string
	for {
		// Expect the next tar header.
		f, err := tr.Next()
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return backupMeta{}, fmt.Errorf("tar error: %w", err)
		}

		// We are only interessted in the backup_label file.
		if f.Name != "backup_label" {
			continue
		}
		// Here we know that we see the backup label file.

		// Read the whole file content.
		content, err := io.ReadAll(tr)
		if err != nil {
			return backupMeta{}, fmt.Errorf("could not read file content: %w", err)
		}

		// Find the line containing the wal segment file name this base backup
		// starts at and extract the file name from the line into the walSegment
		// variable.
		regexStr := `START WAL LOCATION:.*\(file [0-9A-F]{24}\)`
		re := regexp.MustCompile(regexStr)
		matches := re.FindStringSubmatch(string(content))
		if len(matches) != 1 {
			return backupMeta{}, fmt.Errorf(
				"expected exactly 1 match for regex %s, but got %d: %v",
				regexStr, len(matches), matches,
			)
		}
		sep := "(file "
		parts := strings.Split(matches[0], sep)
		if len(parts) != 2 {
			return backupMeta{}, fmt.Errorf(
				"expected exactly 2 parts when splitting string %s with separator %s, but got %d: %v",
				matches[0], sep, len(parts), parts,
			)
		}
		walSegment = strings.TrimSuffix(parts[1], ")")
	}
	rc.Close()

	if walSegment == "" {
		return backupMeta{}, fmt.Errorf("could not extract first wal segment from backup meta file %s", b.Key)
	}

	return backupMeta{
		ulid:            baseULID.String(),
		firstWALSegment: walSegment,
	}, nil
}

var (
	errNotFound     = fmt.Errorf("no item found")
	errOrphanTxLogs = fmt.Errorf("orphaned tx logs found, no basebackup present")
)

// Next returns the next item starting from t (and including t) unless it is a
// base backup. A base backup is only returned if t matches *exactly* the base
// backup's Time().
func (a byULID) Next(t time.Time) (*backupItem, error) {
	sort.Sort(a)
	for _, v := range a {
		vt, err := v.TimeFromUlidKey()
		if err != nil {
			logrus.Warnf("could not get timestamp for object %s: %v", v.Key, err)
			continue
		}
		// If this element's time is before t it could never be the next element
		// on the timeline.
		if vt.Before(t) {
			continue
		}
		// We are either exactly on t or past that.
		if v.Tags[tagKeyIsBaseBackup] == "true" {
			if vt.Equal(t) {
				return v, nil
			}
			return nil, errNotFound
		}
		if v.Tags[tagKeyIsTxLog] == "true" {
			return v, nil
		}
	}
	return nil, errNotFound
}

// Previous returns the item right before t.
func (a byULID) Previous(t time.Time) (*backupItem, error) {
	sort.Sort(a)
	for i := len(a) - 1; i >= 0; i-- {
		v := a[i]
		vt, err := v.TimeFromUlidKey()
		if err != nil {
			logrus.Warnf("could not get timestamp for object %s: %v", v.Key, err)
			continue
		}
		if vt.Before(t) {
			if v.Tags[tagKeyIsBaseBackup] == "true" || v.Tags[tagKeyIsTxLog] == "true" {
				return v, nil
			}
		}
	}
	return nil, errNotFound
}

// Section returns a list of backup items consisting of at least a base backup
// and corresponding transaction logs for a restore to the point in time 't'.
func (a byULID) Section(t time.Time) ([]*backupItem, error) {
	if len(a) == 0 {
		return nil, errNotFound
	}
	var backupID string
	next, err := a.Next(t)
	if err != nil {
		if !errors.Is(err, errNotFound) {
			return nil, fmt.Errorf("starting from %s: could not get next backup item: %w", t.String(), err)
		}
		// Not found, let's look for previous items. Note that this probably
		// means the requested point in time is not available. We can't say now
		// because either nothing happened on the database (which would result
		// in a correct restore) or the last transaction logs are missing (which
		// would mean at least some data has been lost).
		prev, err := a.Previous(t)
		if err != nil {
			return nil, fmt.Errorf("starting from %s: could not get previous backup item: %w", t.String(), err)
		}
		backupID = prev.Tags[tagKeyBackupID]
	} else {
		backupID = next.Tags[tagKeyBackupID]
	}

	list := filterByBackupID(a, backupID)
	out := make([]*backupItem, 0)
	for _, v := range list {
		vt, err := v.TimeFromUlidKey()
		if err != nil {
			logrus.Warnf("could not get timestamp for object %s: %v", v.Key, err)
			continue
		}
		if !vt.After(t) {
			out = append(out, v)
			continue
		}
		if v.Tags[tagKeyIsBaseBackupMeta] == "true" {
			// Include the meta object, regardless of its date.
			out = append(out, v)
		}
	}
	if next != nil {
		vt, err := next.TimeFromUlidKey()
		if err != nil {
			return nil, fmt.Errorf("could not get timestamp for next object %s: %w", next.Key, err)
		}
		if vt.After(t) {
			out = append(out, next)
		}
	}

	if len(out) == 0 {
		return nil, errNotFound
	}
	if out[0].Tags[tagKeyIsBaseBackup] != "true" {
		return nil, errOrphanTxLogs
	}

	return out, nil
}

type backupMetaList struct {
	list []backupMeta
	mu   sync.Mutex
}

func NewBackupMetaList() *backupMetaList {
	return &backupMetaList{
		list: make([]backupMeta, 0),
	}
}

// Sorted returns the backup meta file list sorted by newest ulid.
func (l *backupMetaList) Sorted() []backupMeta {
	l.mu.Lock()
	out := make([]backupMeta, len(l.list))
	copy(out, l.list)
	l.mu.Unlock()
	sort.Sort(byNewestULID(out))
	return out
}

func (l *backupMetaList) Add(m backupMeta) {
	l.mu.Lock()
	defer l.mu.Unlock()
	l.list = append(l.list, m)
}

type backupMeta struct {
	ulid            string // The ULID of the basebackup.
	firstWALSegment string // The first WAL segment which is required for the basebackup.
}

type byNewestULID []backupMeta

func (a byNewestULID) Len() int      { return len(a) }
func (a byNewestULID) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a byNewestULID) Less(i, j int) bool {
	return a[i].ulid > a[j].ulid
}

// filterByBackupID returns the list with items only matching the specified
// backup id.
func filterByBackupID(list []*backupItem, backupID string) []*backupItem {
	match := tags.MustParse(fmt.Sprintf(`{backup_id=%q}`, backupID))
	return filterByMatchers(list, match)
}

// filterByMatchers returns the list with only the matched items in it.
func filterByMatchers(list []*backupItem, matchers tags.Matchers) []*backupItem {
	out := make([]*backupItem, 0)
	for _, v := range list {
		if matchers.Match(v.Tags) {
			out = append(out, v)
		}
	}
	return out
}

func filterTags(list []*backupItem, keep func(key, value string) bool) {
	for _, it := range list {
		for k, v := range it.Tags {
			if !keep(k, v) {
				delete(it.Tags, k)
			}
		}
	}
}

// streamToTar combines backup objects to a single tarball streamed to out.
func streamToTar(ctx context.Context, list byULID, txLogPath string, storage Getter, out io.Writer) error {
	tw := tar.NewWriter(out)
	defer tw.Close()

	copyTar := func(v *backupItem) error {
		reader, err := storage.Get(ctx, v.Key)
		if err != nil {
			logrus.Errorf("error reading tar from object storage: %v", err)
			return err
		}
		defer reader.Close()
		// We expect the input stream to be gzipped.
		gz, err := gzip.NewReader(reader)
		if err != nil {
			logrus.Error(err)
			return err
		}
		defer gz.Close()
		tr := tar.NewReader(gz)
		for {
			hdr, err := tr.Next()
			if err == io.EOF {
				break // End of archive.
			}
			if err != nil {
				return err
			}
			if istxlog := v.Tags[tagKeyIsTxLog]; istxlog == "true" {
				// We assume we're handling a txlog. Fixup the path so we make
				// sure the tx log files do not land in the root directory.
				hdr.Name = path.Join(txLogPath, hdr.Name)
			}
			if err := tw.WriteHeader(hdr); err != nil {
				return err
			}
			if _, err := io.Copy(tw, tr); err != nil {
				return err
			}
		}
		return nil
	}

	sort.Sort(list)
	for _, v := range list {
		if err := copyTar(v); err != nil {
			return err
		}
	}

	return nil
}

// compileSets returns bundle information as a list of backup items that are
// sorted according to their base backup ULIDs.
func compileSets(original []*backupItem) byULID {
	// Make sure we don't alter the input list by copying it here.
	copy := copyBackupItems(original)
	sort.Sort(byULID(copy))

	var bundles = make(byULID, len(copy)) // Worst case: We have only basebackups.
	var n int = -1
	var backupID string
	inconsistentBackupIds := make([]string, 0)
	for _, v := range copy {
		// Ignore objects which are not a base backup, a txlog or a base backup
		// meta file.
		if v.Tags[tagKeyIsBaseBackup] != "true" && v.Tags[tagKeyIsTxLog] != "true" && v.Tags[tagKeyIsBaseBackupMeta] != "true" {
			continue
		}

		haveBackupID, ok := v.Tags[tagKeyBackupID]
		if !ok {
			// Try to recover by fetching the backup id from the file name.
			ulid, err := v.BaseBackupULID()
			haveBackupID = ulid.String()
			if err != nil {
				logrus.Warnf("unexpected empty backup id for item %s", v.Key)
				countNonUlidBackupId(haveBackupID, v.BucketName)
				continue
			}
			logrus.Infof("recovered backup id %s from path since backup id tag is missing for object %s", haveBackupID, v.Key)
		}

		switch {
		// If we encounter a base backup.
		case v.Tags[tagKeyIsBaseBackup] == "true":
			// Initialize the bundle for the current base backup.
			n++
			bundles[n] = &backupItem{
				Key:          path.Dir(path.Dir(v.Key)),
				LastModified: v.LastModified,
				Size:         v.Size,
				Link:         v.Link,
				Tags:         v.Tags,
				BucketName:   v.BucketName,
			}
			backupID = haveBackupID

		// If we encounter non-matching backup ids, log a warning and measure
		// the inconsistency age.
		case haveBackupID != backupID:
			logrus.Warnf("inconsistent backup id for object %s, have %s, want %s", v.Key, haveBackupID, backupID)
			measureInconsistencyAge(haveBackupID, backupID, v.BucketName)
			// Keep book on all inconsistent backup ids encountered during this
			// set compilation run.
			inconsistentBackupIds = append(inconsistentBackupIds, haveBackupID)
			continue

		// If we encountered a txlog or a base backup meta file.
		default: // v.Tags[tagKeyIsTxLog] == "true" || v.Tags[tagKeyIsBaseBackupMeta] == "true"
			if n >= 0 {
				// Sum the sizes of all txlogs (and the base backup meta file).
				bundles[n].Size += v.Size
			}
		}
	}

	// Delete all inconsistency metrics that refer to no longer inconsistent
	// backups.
	deleteNoLongerInconsistent(inconsistentBackupIds)

	return bundles[:n+1]
}

func printList(out io.Writer, list []*backupItem) {
	enc := json.NewEncoder(out)
	enc.SetIndent("", "  ")
	if err := enc.Encode(list); err != nil {
		panic(err)
	}
}

func copyBackupItems(list []*backupItem) []*backupItem {
	if list == nil {
		return nil
	}
	var out = make([]*backupItem, len(list))
	for k, v := range list {
		tagsCopy := make(map[string]string)
		for k, v := range v.Tags {
			tagsCopy[k] = v
		}
		var linkCopy url.URL
		var linkCopyPtr *url.URL
		if v.Link != nil {
			linkCopy = *v.Link
			linkCopyPtr = &linkCopy
		}
		copy := backupItem{
			Key:          v.Key,
			LastModified: v.LastModified,
			Size:         v.Size,
			Link:         linkCopyPtr,
			Daily:        v.Daily,
			Weekly:       v.Daily,
			Monthly:      v.Monthly,
			Tags:         tagsCopy,
			BucketName:   v.BucketName,
		}
		out[k] = &copy
	}
	return out
}
