package main

import (
	_ "net/http/pprof"
	"testing"
)

func Test_firstRequiredSegment(t *testing.T) {
	tests := []struct {
		name      string
		fileNames []string
		want      string
		wantErr   bool
	}{
		{
			name: "success",
			fileNames: []string{
				"000000010000000000000040.00000028.backup",
				"000000010000000000000042.00000028.backup",
			},
			want:    "000000010000000000000042",
			wantErr: false,
		},
		{
			name: "success",
			fileNames: []string{
				"000000010000000000000042.00000028.backup",
				"000000010000000000000040.00000028.backup",
			},
			want:    "000000010000000000000042",
			wantErr: false,
		},
		{
			name: "success",
			fileNames: []string{
				"000000010000000000000042.00000028.backup",
				"000000010000000000000040.00000028.backup",
				"000000010000000000000046.00000028.backup",
			},
			want:    "000000010000000000000046",
			wantErr: false,
		},
		{
			name:      "fail",
			fileNames: []string{},
			want:      "",
			wantErr:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := firstRequiredSegment(tt.fileNames)
			if (err != nil) != tt.wantErr {
				t.Errorf("firstRequiredSegment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("firstRequiredSegment() = %v, want %v", got, tt.want)
			}
		})
	}
}
