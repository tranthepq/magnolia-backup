package main

import (
	"math/rand"
	"testing"
)

func Test_stringFromRange(t *testing.T) {
	type args struct {
		start int
		stop  int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ALPHA upper",
			args: args{
				start: 0x41,
				stop:  0x5a,
			},
			want: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
		},
		{
			name: "ALPHA lower",
			args: args{
				start: 0x61,
				stop:  0x7a,
			},
			want: "abcdefghijklmnopqrstuvwxyz",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := stringFromRange(tt.args.start, tt.args.stop); got != tt.want {
				t.Errorf("stringFromRange() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_stringFromRanges(t *testing.T) {
	type args struct {
		codepoints []int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ALPHA",
			args: args{
				codepoints: []int{0x41, 0x5a, 0x61, 0x7a},
			},
			want: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
		},
		{
			name: "hexdig upper",
			args: args{
				codepoints: []int{0x30, 0x39, 0x41, 0x46},
			},
			want: "0123456789ABCDEF",
		},
		{
			name: "hexdig lower",
			args: args{
				codepoints: []int{0x30, 0x39, 0x61, 0x66},
			},
			want: "0123456789abcdef",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := stringFromRanges(tt.args.codepoints...); got != tt.want {
				t.Errorf("stringFromRanges() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_randString(t *testing.T) {
	rand.Seed(1) // Make sure we always get the same results.
	type args struct {
		n    int
		pool string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "alphanum",
			args: args{
				n:    42,
				pool: alpha + digit,
			},
			want: "bPlNFGdSC2wd8f2QnFhk5A84JJjKWZdKH9H2FHFuvU",
		},
		{
			name: "alpha lower num",
			args: args{
				n:    42,
				pool: alphaLower + digit,
			},
			want: "k1b182tvjzjpezi4hx9gvmkir0xcta0opsb5qipjzb",
		},
		{
			name: "hex",
			args: args{
				n:    42,
				pool: hexDigLower,
			},
			want: "9b9332e8234783de17bd7a25e0a9f6813976eadf26",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := randString(tt.args.n, tt.args.pool); got != tt.want {
				t.Errorf("randString() = %v, want %v", got, tt.want)
			}
		})
	}
}
