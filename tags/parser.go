// Package tags implements a PromQL-style label matcher with parser.

package tags

import (
	"fmt"
	"regexp"
	"strings"
	"text/scanner"
)

type parser struct {
	buf      []rune           // Internal buffer.
	src      *scanner.Scanner // The source tokenizer.
	matchers Matchers         // The resulting matchers.
	err      error            // The last error if anything bad happened.
}

func (p *parser) run() (Matchers, error) {
	p.matchers = nil
	for state := startState; state != nil; {
		state = state(p)
	}
	return p.matchers, p.err
}

// unexpected sets the error and returns a nil state signaling that it's time to
// stop processing.
func (p *parser) unexpected(msg interface{}) state {
	var out string
	switch v := msg.(type) {
	case byte, int, rune:
		k, ok := v.(rune)
		if ok && k == scanner.EOF {
			out = "eof"
			break
		}
		out = string(v.(rune))
	case string, []byte:
		out = v.(string)
	default:
		out = fmt.Sprintf("%v", v)
	}
	pos := p.src.Pos()
	p.err = fmt.Errorf("unexpected: '%s' at line %d, column %d", out, pos.Line, pos.Column)
	return nil
}

// acceptRun eats the next input and stores it in the internal buffer.
func (p *parser) acceptRun(pool string) {
	var r rune
	for {
		r = p.src.Peek()
		if !strings.ContainsRune(pool, r) {
			return
		}
		if int(r) == scanner.EOF {
			return
		}
		p.buf = append(p.buf, p.src.Next())
	}
}

// acceptRunExcept accepts everything not in the pool.
func (p *parser) acceptRunExcept(pool string) {
	var r rune
	for {
		r = p.src.Peek()
		if strings.ContainsRune(pool, r) {
			return
		}
		if int(r) == scanner.EOF {
			return
		}
		p.buf = append(p.buf, p.src.Next())
	}
}

// Matcher matches tags.
type Matcher struct {
	name       string         // Tag name we match (informational).
	negate     bool           // True if match is negated.
	exactMatch bool           // True encapsulates the regex with ^$ and escapes all regex chars to force an exact match.
	re         *regexp.Regexp // We match against this.
}

// Match returns true if the tag referenced by the matcher's name is present and
// matches its regular expression.
func (m *Matcher) Match(tags map[string]string) bool {
	val, ok := tags[m.name]
	if !ok {
		// We require the matched (or non-matched) tag to be present.
		return false
	}
	match := m.re.MatchString(val)
	switch {
	case !match && m.negate:
		return true
	case match && !m.negate:
		return true
	default:
		return false
	}
}

func (m Matcher) String() string {
	var sb strings.Builder
	sb.WriteString(m.name)
	if m.negate {
		sb.WriteString("!=")
	} else {
		sb.WriteString("==")
	}
	sb.WriteString(m.re.String())
	if m.exactMatch {
		sb.WriteString("(exactMatch)")
	}
	return sb.String()
}

// Matchers is a list of Matchers.
type Matchers []*Matcher

// Match returns true if all matchers match for this set of tags.
func (m Matchers) Match(tags map[string]string) bool {
	// Each matcher has to match or we don't have a match.
	for _, matcher := range m {
		if matcher.Match(tags) {
			continue
		}
		return false
	}
	// The zero value always returns true.
	return true
}

func (m Matchers) String() string {
	var sb strings.Builder
	for _, matcher := range m {
		sb.WriteString(matcher.String() + ",")
	}
	return sb.String()
}

// state represents a state in our parser FSM.
type state func(*parser) state

// MustParse panics if a parse error happens.
func MustParse(query string) Matchers {
	mm, err := ParseTagMatcher(query)
	if err != nil {
		panic(err)
	}
	return mm
}

// ParseTagMatcher parses a tag query and returns the matcher list.
func ParseTagMatcher(query string) (Matchers, error) {
	scan := new(scanner.Scanner)
	query = strings.TrimSpace(query)
	scan.Init(strings.NewReader(query))

	p := &parser{
		src: scan,
	}

	scan.Error = func(s *scanner.Scanner, msg string) {
		p.err = fmt.Errorf("error while parsing query: %s", msg)
	}
	return p.run()
}

// We start here.
func startState(p *parser) state {
	next := p.src.Next()
	switch next {
	case startBracket:
		return stateBrackets
	default:
		return p.unexpected(next)
	}
}

// We're inside the brackets {}.
func stateBrackets(p *parser) state {
	next := p.src.Next()
	switch {
	case next == endBracket:
		peek := p.src.Peek()
		if peek != scanner.EOF {
			// We're only done if we are at EOF.
			return p.unexpected(peek)
		}
		return nil // We're done.
	case strings.ContainsRune(tagName, next):
		p.buf = p.buf[0:0]
		p.buf = append(p.buf, next)
		// This is probably a tag identifier, create a new matcher.
		matcher := new(Matcher)
		return stateTagName(matcher)
	default:
		return p.unexpected(next)
	}
}

func stateTagName(match *Matcher) state {
	return func(p *parser) state {
		p.acceptRun(tagName)
		// Now we have the complete tag name or at least a name with one rune.
		match.name = string(p.buf)
		p.buf = p.buf[0:0] // Reset the internal buffer.
		next := p.src.Next()
		switch next {
		case equal:
			return stateMatcher(match)
		case negate:
			return stateNegMatcher(match)
		default:
			return p.unexpected(next)
		}
	}
}

func stateMatcher(match *Matcher) state {
	return func(p *parser) state {
		next := p.src.Next()
		switch next {
		case regex:
			match.exactMatch = false
			// Consume the next char which sould be a quote.
			next = p.src.Next()
			if next != quote {
				return p.unexpected(next)
			}
			return stateRegex(match)
		case quote:
			// Quote is consumed.
			match.exactMatch = true
			return stateRegex(match)
		default:
			return p.unexpected(next)
		}
	}
}

func stateNegMatcher(match *Matcher) state {
	return func(p *parser) state {
		next := p.src.Next()
		switch next {
		case regex:
			match.exactMatch = false
		case equal:
			match.exactMatch = true
		default:
			return p.unexpected(next)
		}
		// Consume the next char which sould be a quote.
		next = p.src.Next()
		if next != quote {
			return p.unexpected(next)
		}
		match.negate = true
		return stateRegex(match)
	}
}

func stateRegex(match *Matcher) state {
	return func(p *parser) state {
		// We should not have any quotes as the next chars.
		p.acceptRunExcept(`"`)
		if len(p.buf) < 1 {
			// This means we have an empty string (""). Override the regex then.
			p.buf = []rune("^$")
			match.exactMatch = false
		}
		expr := string(p.buf)
		if match.exactMatch {
			expr = regexp.QuoteMeta(expr)
			expr = fmt.Sprintf(`^%s$`, expr)
		}
		p.buf = p.buf[0:0] // Reset the internal buffer.
		re, err := regexp.Compile(expr)
		if err != nil {
			p.err = err
			return nil
		}
		match.re = re
		// Consume the closing quote.
		next := p.src.Next()
		if next != quote {
			return p.unexpected(next)
		}
		// The next char should be a comma or a closing bracket. Consume the
		// comma if there is one.
		next = p.src.Peek()
		if next == comma {
			p.src.Next()
		}
		// Append this matcher to the list.
		p.matchers = append(p.matchers, match)
		return stateBrackets
	}
}
