package tags

import (
	"fmt"
	"regexp"
	"testing"
)

func TestParseTagMatcher(t *testing.T) {
	type wantFunc func(Matchers) error
	type args struct {
		query string
	}
	tests := []struct {
		name    string
		args    args
		want    wantFunc
		wantErr bool
	}{
		{
			name: "no closing curly braces",
			args: args{
				query: `{tagname="value"`,
			},
			wantErr: true,
		},
		{
			name: "too many curly braces",
			args: args{
				query: `{tagname="value"}{`,
			},
			wantErr: true,
		},
		{
			name: "missing quote",
			args: args{
				query: `{tagname="value}`,
			},
			wantErr: true,
		},
		{
			name: "all quotes missing",
			args: args{
				query: `{tagname=value}`,
			},
			wantErr: true,
		},
		{
			name: "empty string query",
			args: args{
				query: `{tagname=""}`,
			},
			wantErr: false,
		},
		{
			name: "meta names",
			args: args{
				query: `{__name="test"}`,
			},
			wantErr: false,
			want: func(mm Matchers) error {
				if len(mm) != 1 {
					return fmt.Errorf("wrong number of matchers returned")
				}
				m := mm[0]
				if m.name != "__name" {
					return fmt.Errorf("matcher name = %s, want %s", m.name, "__name")
				}
				return nil
			},
		},
		{
			name: "simple query",
			args: args{
				query: `{tagname="value"}`,
			},
			want: func(mm Matchers) error {
				if len(mm) != 1 {
					return fmt.Errorf("wrong number of matchers returned")
				}
				m := mm[0]
				if m.name != "tagname" {
					return fmt.Errorf("matcher name = %s, want %s", m.name, "tagname")
				}
				if m.exactMatch != true {
					return fmt.Errorf("exactMatch = false, want = true")
				}
				if m.re.String() != "^value$" {
					return fmt.Errorf("got regex = %s, want %s", m.re.String(), "^value$")
				}
				return nil
			},
		},
		{
			name: "simple query multiple tags",
			args: args{
				query: `{tagname="value",tagname2="value"}`,
			},
			want: func(mm Matchers) error {
				if len(mm) != 2 {
					return fmt.Errorf("wrong number of matchers returned")
				}
				m := mm[0]
				if m.name != "tagname" {
					return fmt.Errorf("matcher name = %s, want %s", m.name, "tagname")
				}
				if !m.exactMatch || !mm[1].exactMatch {
					return fmt.Errorf("exactMatch = false, want = true")
				}
				if m.re.String() != "^value$" {
					return fmt.Errorf("got regex = %s, want %s", m.re.String(), "^value$")
				}
				return nil
			},
		},
		{
			name: "more complex query multiple tags",
			args: args{
				query: `{tagname="value",tagname2!="value2",tagname3=~"value3",tagname4!="value4",tagname5!~"value5"}`,
			},
			want: func(mm Matchers) error {
				if len(mm) != 5 {
					return fmt.Errorf("wrong number of matchers returned")
				}
				m := mm[0]
				if m.name != "tagname" {
					return fmt.Errorf("matcher name = %s, want %s", m.name, "tagname")
				}
				if m.re.String() != "^value$" {
					return fmt.Errorf("got regex = %s, want %s", m.re.String(), "^value$")
				}
				if re := mm[2].re.String(); re != `value3` {
					return fmt.Errorf("got regex = %s, want %s", re, "value3")
				}
				if mm[3].negate == false || mm[4].negate == false {
					return fmt.Errorf("matcher should negate but doesn't")
				}
				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseTagMatcher(tt.args.query)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseTagMatcher() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.want != nil {
				if err := tt.want(got); err != nil {
					t.Error(err)
				}
			}
			t.Logf("%v", err)
		})
	}
}

func TestMatcher_Match(t *testing.T) {
	type fields struct {
		name       string
		negate     bool
		exactMatch bool
		re         *regexp.Regexp
	}
	type args struct {
		tags map[string]string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "simple",
			fields: fields{
				name:       "mytag",
				negate:     false,
				exactMatch: true,
				re:         regexp.MustCompile(`^myvalue$`),
			},
			args: args{
				tags: map[string]string{
					"mytag":  "myvalue",
					"mytag2": "myvalue2",
					"mytag3": "myvalue3",
					"mytag4": "myvalue4",
				},
			},
			want: true,
		},
		{
			name: "negated",
			fields: fields{
				name:       "mytag",
				negate:     true,
				exactMatch: true,
				re:         regexp.MustCompile(`^myvalue$`),
			},
			args: args{
				tags: map[string]string{
					"mytag": "wrong value",
				},
			},
			want: true,
		},
		{
			name: "regex",
			fields: fields{
				name:       "mytag",
				negate:     false,
				exactMatch: false,
				re:         regexp.MustCompile(`[a-z].+`),
			},
			args: args{
				tags: map[string]string{
					"mytag": "good value",
				},
			},
			want: true,
		},
		{
			name: "negate regex",
			fields: fields{
				name:       "mytag",
				negate:     true,
				exactMatch: false,
				re:         regexp.MustCompile(`[0-9]`),
			},
			args: args{
				tags: map[string]string{
					"mytag": "bad value 123",
				},
			},
			want: false,
		},
		{
			name: "negate regex",
			fields: fields{
				name:       "mytag",
				negate:     true,
				exactMatch: false,
				re:         regexp.MustCompile(`[0-9]`),
			},
			args: args{
				tags: map[string]string{
					"mytag": "good value",
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matcher{
				name:       tt.fields.name,
				negate:     tt.fields.negate,
				exactMatch: tt.fields.exactMatch,
				re:         tt.fields.re,
			}
			if got := m.Match(tt.args.tags); got != tt.want {
				t.Errorf("Matcher.Match() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatchers_Match(t *testing.T) {
	matchers, err := ParseTagMatcher(`{mytag="myvalue",mytag2!="myvalue"}`)
	if err != nil {
		t.Error(err)
		return
	}
	type args struct {
		tags map[string]string
	}
	tests := []struct {
		name string
		m    Matchers
		args args
		want bool
	}{
		{
			name: "simple",
			m:    matchers,
			args: args{
				tags: map[string]string{
					"mytag":    "myvalue",
					"mytag2":   "myvalue2",
					"name":     "value",
					"supertag": "supervalue",
				},
			},
			want: true,
		},
		{
			name: "simple no match",
			m:    matchers,
			args: args{
				tags: map[string]string{
					"mytag":    "myvalue",
					"mytag2":   "myvalue",
					"name":     "value",
					"supertag": "supervalue",
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.m.Match(tt.args.tags); got != tt.want {
				t.Errorf("Matchers.Match() = %v, want %v", got, tt.want)
			}
		})
	}
}
