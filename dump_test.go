package main

import (
	"bytes"
	"context"
	"os"
	"testing"
)

func TestDump(t *testing.T) {
	// Prepare random environment variable for test.
	ni := "Ni!"
	os.Setenv("WHAT_KNIGHTS_SAY", ni)

	type args struct {
		ctx     context.Context
		command string
		args    []string
	}
	tests := []struct {
		name       string
		args       args
		wantTarget string
		wantErr    bool
	}{
		{
			name: "echo test",
			args: args{
				ctx:     context.TODO(),
				command: "echo",
				args:    []string{"We are the", "Knights who say:", "Ni!"},
			},
			wantTarget: "We are the Knights who say: Ni!\n",
			wantErr:    false,
		},
		{
			name: "echo test with args",
			args: args{
				ctx:     context.TODO(),
				command: "echo",
				args:    []string{"-n", "We are the", "Knights who say:", "Ni!"},
			},
			wantTarget: "We are the Knights who say: Ni!", // Note the missing newline.
			wantErr:    false,
		},
		{
			name: "echo command with non-0 exit status",
			args: args{
				ctx:     context.TODO(),
				command: "false",
				args:    nil,
			},
			wantTarget: "",
			wantErr:    true,
		},
		{
			name: "echo command with non-0 exit status",
			args: args{
				ctx:     context.TODO(),
				command: "echo",
				args:    []string{"We are the Knights who say: $WHAT_KNIGHTS_SAY"},
			},
			wantTarget: "We are the Knights who say: Ni!\n",
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			target := &bytes.Buffer{}
			if err := Dump(tt.args.ctx, target, tt.args.command, tt.args.args...); (err != nil) != tt.wantErr {
				t.Errorf("Dump() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotTarget := target.String(); gotTarget != tt.wantTarget {
				t.Errorf("Dump() = %v, want %v", gotTarget, tt.wantTarget)
			}
		})
	}
}
