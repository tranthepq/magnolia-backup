package main

import (
	"testing"
	"time"
)

func Test_rateString(t *testing.T) {
	type args struct {
		d time.Duration
		n int64
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "1 MiB/s",
			args: args{d: time.Second, n: 1 * Mi},
			want: "1.0 MiB/s",
		},
		{
			name: "1 MiB/s",
			args: args{d: 30 * time.Second, n: 30 * Mi},
			want: "1.0 MiB/s",
		},
		{
			name: "1000.0 KiB/s",
			args: args{d: 30 * time.Millisecond, n: 30720},
			want: "1000.0 KiB/s",
		},
		{
			name: "1000.0 KiB/s",
			args: args{d: time.Millisecond, n: 1024},
			want: "1000.0 KiB/s",
		},
		{
			name: "2.1 MiB/s",
			args: args{d: 30 * time.Millisecond, n: 65535},
			want: "2.1 MiB/s",
		},
		{
			name: "8.4 GiB/s",
			args: args{d: 27 * time.Millisecond, n: 231 * Mi},
			want: "8.4 GiB/s",
		},
		{
			name: "376 B/s",
			args: args{d: time.Hour, n: 1322 * Ki},
			want: "376 B/s",
		},
		{
			name: "13.0 KiB/s",
			args: args{d: 0, n: 13 * Ki},
			want: "13.0 KiB/s",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rateString(tt.args.d, tt.args.n); got != tt.want {
				t.Errorf("rateString() = %v, want %v", got, tt.want)
			}
		})
	}
}
