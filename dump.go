package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

// Dump starts the dump process using the command and writing the output to the
// target. It returns an error if anything bad happens.
func Dump(ctx context.Context, target io.Writer, command string, args ...string) error {
	// Expand env vars in argument list.
	for k, v := range args {
		args[k] = os.ExpandEnv(v)
	}
	logrus.Debugf("executing command: %s %s", command, strings.Join(args, " "))
	src := exec.CommandContext(ctx, command, args...)
	src.Stdout = target
	src.Stderr = os.Stderr
	src.Env = os.Environ()

	if err := src.Run(); err != nil {
		return fmt.Errorf("error running dump cmd: %w", err)
	}

	return nil
}
