package main

import (
	"math/rand"
	"strings"
	"time"
)

func stringFromRange(start, stop int) string {
	n := stop - start
	if n < 0 {
		return ""
	}
	out := make([]rune, n+1)
	for i := 0; i <= n; i++ {
		out[i] = rune(i + start)
	}
	return string(out)
}

func stringFromRanges(codepoints ...int) string {
	if len(codepoints) < 2 || len(codepoints)%2 != 0 {
		panic("even number of range codepoints needed")
	}
	var buf strings.Builder
	for i := 0; i < len(codepoints); i = i + 2 {
		buf.WriteString(stringFromRange(codepoints[i], codepoints[i+1]))
	}
	return buf.String()
}

func init() {
	rand.Seed(time.Now().Unix())
}

// Randomly select n runes from the rune pool.
func randString(n int, pool string) string {
	var out strings.Builder
	for i := 0; i < n; i++ {
		k := rand.Intn(len(pool))
		out.WriteRune(rune(pool[k]))
	}
	return out.String()
}

var (
	// Only ASCII uppercase chars.
	alphaUpper = stringFromRange(0x41, 0x5a)
	// Only ASCII lowercase chars.
	alphaLower = stringFromRange(0x61, 0x7a)
	// Uppercase and lowercase combined.
	alpha = alphaUpper + alphaLower
	// Digits.
	digit = stringFromRanges(0x30, 0x39)
	// Digits for hex notation, lowercase.
	hexDigLower = stringFromRanges(0x30, 0x39, 0x61, 0x66)
)
