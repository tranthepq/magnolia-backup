package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	io_prometheus_client "github.com/prometheus/client_model/go"
	"github.com/sirupsen/logrus"
	"golang.org/x/exp/slices"
)

const (
	labelKeyInterval         = "interval"
	labelKeyMethod           = "method"
	labelKeyVersion          = "version"
	labelKeyBackupID         = "backup_id"
	labelKeyExpectedBackupID = "expected_backup_id"
	labelKeyBucketName       = "bucket_name"
	labelKeyObjectKey        = "object_key"

	metricNameInconsistencyAge = "mgnlbackup_inconsistency_age_seconds"
	metricNameEmptyBackupID    = "mgnlbackup_empty_backup_id_total"
	metricNameNonUlidBackupID  = "mgnlbackup_non_ulid_backup_ids_total"
)

var (
	labelDaily   = prometheus.Labels{labelKeyInterval: "daily"}
	labelWeekly  = prometheus.Labels{labelKeyInterval: "weekly"}
	labelMonthly = prometheus.Labels{labelKeyInterval: "monthly"}
	metricLabels = []string{
		labelKeyInterval,
	}

	labelUpload   = prometheus.Labels{labelKeyMethod: "upload"}
	labelMerge    = prometheus.Labels{labelKeyMethod: "merge"}
	upMergeLabels = []string{
		labelKeyMethod,
	}

	backupIDInfoLabels = []string{
		labelKeyBackupID,
	}

	labelVersion      = prometheus.Labels{labelKeyVersion: version}
	versionInfoLabels = []string{labelKeyVersion}
)

var metricGauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
	Name: "mgnlbackup_backups",
	Help: "The total number of daily backups.",
}, metricLabels)

var metricSizeTotal = promauto.NewGauge(prometheus.GaugeOpts{
	Name: "mgnlbackup_bytes",
	Help: "Total byte size of all backups combined in target storage.",
})

var metricSummary = promauto.NewSummary(prometheus.SummaryOpts{
	Name: "mgnlbackup_seconds",
	Help: "Time taken for backup jobs.",
})

var metricErrors = promauto.NewCounter(prometheus.CounterOpts{
	Name: "mgnlbackup_errors",
	Help: "Number of erroneous, not completed backup jobs.",
})

var metricUpMergeErrors = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: "mgnlbackup_upmerge_errors",
	Help: "Number of errors encountered during upload/merge operations exectuted during a divide upload.",
}, upMergeLabels)

var metricBackupID = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: "mgnlbackup_backup_info",
	Help: "Information about the current backup id.",
}, backupIDInfoLabels)

var metricVersion = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: "mgnlbackup_version_info",
	Help: "Shows the current version of this program.",
}, versionInfoLabels)

var metricInconsistencyAge = promauto.NewGaugeVec(prometheus.GaugeOpts{
	Name: metricNameInconsistencyAge,
	Help: "The age (since the creation of the backup) of an inconsistent backup in seconds.",
}, []string{
	labelKeyBackupID, labelKeyExpectedBackupID, labelKeyBucketName,
})

var metricNonUlidBackupID = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: metricNameNonUlidBackupID,
	Help: "Total number of encountered non ulid backup ids.",
}, []string{
	labelKeyBackupID, labelKeyBucketName,
})

func init() {
	// This is for informational purposes only and should not change during
	// runtime.
	metricVersion.With(labelVersion).Add(1)
}

func countNonUlidBackupId(backupId, bucketName string) {
	labels := prometheus.Labels{
		labelKeyBackupID:   backupId,
		labelKeyBucketName: bucketName,
	}
	counter, err := metricNonUlidBackupID.GetMetricWith(labels)
	if err != nil {
		logrus.Errorf("could not get counter %s for labels %v: %v", metricNameNonUlidBackupID, labels, err)
		return
	}
	counter.Add(1)
}

func deleteNoLongerInconsistent(inconsistentBackupIds []string) {
	// Fetch a list of all labels the inconsistency age metric currently has.
	metricChan := make(chan prometheus.Metric)
	labelsList := make([]prometheus.Labels, 0)
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for metric := range metricChan {
			m := &io_prometheus_client.Metric{}
			metric.Write(m)
			labels := make(prometheus.Labels)
			for _, label := range m.GetLabel() {
				labels[label.GetName()] = label.GetValue()
			}
			labelsList = append(labelsList, labels)
		}
		wg.Done()
	}()
	metricInconsistencyAge.Collect(metricChan)
	close(metricChan)
	wg.Wait()

	// For each labels map ...
	for _, labels := range labelsList {
		// ...  decide whether the inconsisteny measured by the current labels
		// map is still inconsistent. It is only still inconsistent, if the
		// backup id in the label is contained in the list of inconsistent
		// backup ids.
		stillInconsistent := slices.Contains(inconsistentBackupIds, labels[labelKeyBackupID])
		if !stillInconsistent {
			hasDeleted := metricInconsistencyAge.Delete(labels)
			if hasDeleted {
				logrus.Infof("has deleted inconsistency age metric with labels %s", labels)
			}
		}
	}
}

func measureInconsistencyAge(backupId, expectedBackupId, bucketName string) {
	// Make expected backup id label more readable in case it is empty.
	if expectedBackupId == "" {
		expectedBackupId = "empty"
	}
	labels := prometheus.Labels{
		labelKeyBackupID:         backupId,
		labelKeyExpectedBackupID: expectedBackupId,
		labelKeyBucketName:       bucketName,
	}
	gauge, err := metricInconsistencyAge.GetMetricWith(labels)
	if err != nil {
		logrus.Errorf("could not get gauge %s for labels %v: %v", metricNameInconsistencyAge, labels, err)
		return
	}
	t, err := ULIDTime(backupId)
	if err != nil {
		logrus.Errorf("could not get time from ulid backup id %s: %v", backupId, err)
		countNonUlidBackupId(backupId, bucketName)
		return
	}
	gauge.Set(float64(time.Since(t).Seconds()))
}

// Creates a new counter metric with the backup id label set. The counter should
// not increase normally, it is for informational purposes only.
func recordBackupID(cur string) {
	metricBackupID.Reset()
	metricBackupID.With(prometheus.Labels{labelKeyBackupID: cur}).Add(1)
}

func countBackups(list []*backupItem) {
	// Count backups for metrics.
	var gauge struct {
		daily   float64
		weekly  float64
		monthly float64
		size    int64
	}

	for _, v := range list {
		if v == nil {
			continue
		}
		switch {
		case v.Monthly:
			gauge.monthly = gauge.monthly + 1
			fallthrough
		case v.Weekly:
			gauge.weekly = gauge.weekly + 1
			fallthrough
		case v.Daily:
			gauge.daily = gauge.daily + 1
		}
		gauge.size += v.Size
	}

	metricGauge.With(labelDaily).Set(gauge.daily)
	metricGauge.With(labelWeekly).Set(gauge.weekly)
	metricGauge.With(labelMonthly).Set(gauge.monthly)

	metricSizeTotal.Set(float64(gauge.size))

	logrus.Tracef("setting metric gauges to %+v, len(list) = %d", gauge, len(list))
}

func rateString(d time.Duration, n int64) string {
	b := float64(n)
	dur := float64(float64(d) / float64(time.Second))
	if dur == 0 {
		dur = 1
	}
	if n == 0 {
		return fmt.Sprintf("%s/s", ByteCountBinary(0))
	}
	return fmt.Sprintf("%s/s", ByteCountBinary(int64(b/dur)))
}
