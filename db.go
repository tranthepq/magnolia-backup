package main

import (
	"context"
	"database/sql"
	"fmt"
	"net"

	"github.com/sirupsen/logrus"

	_ "github.com/lib/pq"
)

// PostgresStorageConfig is used to configure access to Postgres.
type PostgresStorageConfig struct {
	Address    string // E.g. "localhost:5432"
	DBName     string
	Username   string
	Password   string
	SecureMode bool
}

func openDbConnection(ctx context.Context, cfg PostgresStorageConfig) (*sql.DB, error) {
	host, port, err := net.SplitHostPort(cfg.Address)
	if err != nil {
		return nil, fmt.Errorf("invalid address given: %w", err)
	}

	logrus.Debugf("database host = %s, port = %s", host, port)
	var sslMode string
	switch cfg.SecureMode {
	case true:
		sslMode = "require"
	case false:
		sslMode = "disable"
	}

	conn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s",
		host,
		port,
		cfg.Username,
		cfg.DBName,
		sslMode,
	)
	if cfg.Password != "" {
		conn = fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
			host,
			port,
			cfg.Username,
			cfg.DBName,
			cfg.Password,
			sslMode,
		)
	}

	db, err := sql.Open("postgres", conn)
	if err != nil {
		return nil, fmt.Errorf("could not open SQL connection: %w", err)
	}

	if err := db.PingContext(ctx); err != nil {
		return nil, fmt.Errorf("could not connect to db: %w", err)
	}

	return db, nil
}
