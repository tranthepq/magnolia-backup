package main

import (
	"archive/tar"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/gorilla/mux"
	"github.com/klauspost/compress/gzip"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/magnolia-backup/tags"
)

const (
	keyOrderBy          = "orderby"
	keySortOrder        = "dir"
	keyLimit            = "limit"
	sortOrderAscending  = "asc"
	sortOrderDescending = "desc"

	metaFileSuffix = "-meta." + fileExtensionTarGz
	pgWalDirectory = "pg_wal"

	basebackupSuffix = "basebackup"

	timeFormat = "2006-01-02-150405"
)

// Default is 404.
func (s *server) handleDefault() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	}
}

func (s *server) handleHealthz() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		res := struct {
			Status string
		}{
			Status: "pass",
		}
		if err := json.NewEncoder(w).Encode(res); err != nil {
			logrus.Error(err)
		}
	}
}

func sendError(w http.ResponseWriter, err error, status int) {
	var exp interface{}
	if err, ok := err.(interface{ ExpandedMsg() interface{} }); ok {
		exp = err.ExpandedMsg()
	}
	var msg = struct {
		Status      string      `json:"status,omitempty"`
		StatusCode  int         `json:"status_code,omitempty"`
		Msg         string      `json:"msg,omitempty"`
		ExpandedMsg interface{} `json:"expanded_msg,omitempty"`
	}{
		Status:      http.StatusText(status),
		StatusCode:  status,
		Msg:         err.Error(),
		ExpandedMsg: exp,
	}
	w.Header().Set("Content-type", "application/json")
	if status == http.StatusTooManyRequests {
		w.Header().Set("Retry-After", "30")
	}
	w.WriteHeader(status)
	if err := json.NewEncoder(w).Encode(msg); err != nil {
		logrus.Errorf("could not send error message: %v", err)
	}
}

// handleList returns a list of currently available backups in S3.
func (s *server) handleList() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Parse query and return matcher list.
		err := r.ParseForm()
		if err != nil {
			logrus.Errorf("form: %v", err)
			sendError(w, err, http.StatusBadRequest)
			return
		}
		var matchers tags.Matchers
		if _, ok := r.Form["query"]; ok {
			query := r.FormValue("query")
			matchers, err = tags.ParseTagMatcher(query)
			if err != nil {
				logrus.Errorf("query parse error: %v, original query: %s", err, query)
				sendError(w, fmt.Errorf("query parse error: %w, original query: %s", err, query), http.StatusBadRequest)
				return
			}
			logrus.Debugf("matching against %s", matchers.String())
		}
		list, err := s.storage.List(r.Context())
		if err != nil {
			logrus.Errorf("error requesting list: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		var orderBy, sortOrder string
		if _, ok := r.Form[keyOrderBy]; ok {
			orderBy = r.FormValue(keyOrderBy)
		}
		if _, ok := r.Form[keySortOrder]; ok {
			sortOrder = r.FormValue(keySortOrder)
		}
		limit := 0
		if _, ok := r.Form[keyLimit]; ok {
			limitStr := r.FormValue(keyLimit)
			// Convert limit string to limit integer.
			limit, err = strconv.Atoi(limitStr)
			if err != nil {
				errMsg := fmt.Sprintf("could not convert limit string: %v", err)
				logrus.Error(errMsg)
				w.WriteHeader(http.StatusBadRequest)
				fmt.Fprint(w, errMsg)
				return
			}
		}
		// https://github.com/golang/go/wiki/SliceTricks#filter-in-place
		n := 0
		for _, v := range list {
			// We add the name of the object as a meta tag called `__name` for
			// matching.
			var tags = make(map[string]string)
			for k, v := range v.Tags {
				tags[k] = v
			}
			tags["__name"] = v.Key
			// Filter by the query we got (or don't filter at all).
			if matchers.Match(tags) {
				list[n] = v
				n++
			}
		}
		list = list[:n]
		// Sort the list according to the user defined sort order.
		err = list.Sort(orderBy, sortOrder)
		if err != nil {
			errMsg := fmt.Sprintf("error sorting list: %v", err)
			logrus.Error(errMsg)
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, errMsg)
			return
		}
		if sortOrder != "" {
			logrus.Infof("sorted list according to sort order %s", sortOrder)
		}
		// Limit the list to the user defined number of elements.
		if limit > 0 && len(list) > limit {
			list = list[:limit]
			logrus.Infof("limited list to %d elements", limit)
		}

		switch r.Header.Get("Accept") {
		case "application/json":
			// The clients want JSON, let's give them JSON.
			w.Header().Set("Content-type", "application/json")
			if err := json.NewEncoder(w).Encode(list); err != nil {
				logrus.Errorf("error encoding list to JSON: %s", err)
				return
			}
		default:
			// Default is a simple HTTP output for browser clients.
			tpl, ok := s.templates["list.html"]
			if !ok {
				logrus.Errorf("could not find template %s", "list.html")
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "template not found")
				return
			}
			// Figure out backup sets.
			var bundles byULID
			bundles = compileSets(list)
			keep := func(key, value string) bool {
				switch key {
				case "daily", "weekly", "monthly":
					return false
				default:
					return true
				}
			}
			filterTags(bundles, keep)
			filterTags(list, keep)
			var data = struct {
				Bundles []*backupItem
				List    []*backupItem
			}{
				Bundles: bundles,
				List:    list,
			}
			if err := tpl.Execute(w, data); err != nil {
				logrus.Errorf("could not execute template %s: %s", tpl.Name(), err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			return
		}
	}
}

type objectInfo struct {
	Name string `json:"name,omitempty"`
	Size int64  `json:"size,omitempty"`
}

func (o objectInfo) String() string {
	return fmt.Sprintf("name = %s, size = %s", o.Name, ByteCountBinary(o.Size))
}

// Handle the manual request of a new dump.
func (s *server) handleDump(prefix string, command string, args []string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		done := make(chan *objectInfo, 1) // Allow for one element to be lost.
		errc := make(chan error, 1)
		go func() {
			defer func() {
				close(done)
				close(errc)
			}()
			ctx, cancel := context.WithTimeout(context.Background(), s.dumpTimeout)
			defer cancel()
			info, err := s.getNewDump(ctx, true)
			if err != nil {
				logrus.Errorf("error getting new dump: %s", err)
				errc <- err
				return
			}
			done <- info
		}()
		sync := r.URL.Query().Get("sync")
		switch sync {
		case "", "false", "no":
			w.WriteHeader(http.StatusAccepted)
			return
		default:
			select {
			case info := <-done:
				w.Header().Set("Content-type", "application/json")
				if err := json.NewEncoder(w).Encode(info); err != nil {
					logrus.Error(err)
					sendError(w, fmt.Errorf("could not encode json response"), http.StatusInternalServerError)
					return
				}
			case err := <-errc:
				if errors.Is(err, errBusy) {
					sendError(w, err, http.StatusTooManyRequests)
					return
				}
				sendError(w, err, http.StatusInternalServerError)
				return
			}
		}
	}
}

func (s *server) handleDownload(prefix string, command string, args []string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		// Check whether a meta file is requested.
		if err := r.ParseForm(); err != nil {
			logrus.Errorf("form: %v", err)
			sendError(w, err, http.StatusBadRequest)
			return
		}
		_, ok := r.Form["meta"]

		// If a meta file is requested.
		if ok {
			// Find out for what backup id the meta file requested.
			backupID := r.FormValue("meta")

			// Fetch the buffered meta response for that backup id and respond.
			resp, err := s.bufferedMetaResps.metaResponseFor(backupID)
			if err != nil {
				logrus.Errorf("error fetching buffered meta response for backup with id %s: %v", backupID, err)
				sendError(w, err, http.StatusInternalServerError)
				return
			}
			_, err = resp.respond(w)
			if err != nil {
				logrus.Errorf("error responding via buffered meta response for backup with id %s: %v", backupID, err)
				sendError(w, err, http.StatusInternalServerError)
				return
			}

			// Delete the buffered response.
			fileName, err := fileName(resp.Header())
			if err != nil {
				logrus.Warnf("could not get file name of meta file for backup with id %s: %v", backupID, err)
				return
			}
			err = s.bufferedMetaResps.delete(fileName)
			if err != nil {
				logrus.Warnf("could not delete buffered meta response for backup with id %s: %v", backupID, err)
			}
			return
		}

		// If a basebackup file is requested. Make a new dump into a http downloader.
		downloader := newHTTPDownloader(w)
		dumpCtx, dumpCtxCancel := context.WithTimeout(ctx, s.dumpTimeout)
		defer dumpCtxCancel()
		info, err := s.getNewDump(dumpCtx, false, downloader)
		if err != nil {
			logrus.Errorf("error sending dump to http client: %v", err)
			if errors.Is(err, errBusy) {
				sendError(w, err, http.StatusTooManyRequests)
				return
			}
			sendError(w, err, http.StatusInternalServerError)
			return
		}

		// Add the buffered meta response to the buffered meta response list.
		// Note: The added buffered meta response is deleted at the latest after
		// 24 hours.
		err = s.bufferedMetaResps.add(downloader.fetchMetaResponseBuffer(), 24*time.Hour)
		if err != nil {
			logrus.Errorf("error adding meta file to buffered meta responses: %v", err)
			sendError(w, err, http.StatusInternalServerError)
			return
		}
		logrus.Infof("successfully sent on-demand backup %s (%s) to http client", info.Name, ByteCountBinary(info.Size))
	}
}

func (s *server) uploadMeta(ctx context.Context, labelFile, mapFile []byte, exp ExpirationTimer, upMerge UploadMerger) (int64, error) {
	// Create new tar file with the label and map files in it.
	r, w := io.Pipe()
	errc := make(chan error, 1)

	go func() {
		defer func() {
			if err := w.Close(); err != nil {
				errc <- err
			}
			close(errc)
		}()
		tw := tar.NewWriter(w)
		files := []struct {
			Name string
			Body []byte
		}{
			{Name: labelFileName, Body: labelFile},
			{Name: mapFileName, Body: mapFile},
		}
		for _, file := range files {
			hdr := &tar.Header{
				Name:    file.Name,
				Mode:    0644,
				Size:    int64(len(file.Body)),
				ModTime: time.Now(),
			}
			if err := tw.WriteHeader(hdr); err != nil {
				errc <- fmt.Errorf("could not write to tar: %w", err)
				return
			}
			if _, err := tw.Write(file.Body); err != nil {
				errc <- fmt.Errorf("could not write to tar: %w", err)
				return
			}
		}
		if err := tw.Close(); err != nil {
			errc <- fmt.Errorf("could not write to tar: %w", err)
			return
		}
	}()

	cur := currentBackupID.Get()
	expt, err := expiryString(exp, cur)
	if err != nil {
		return 0, err
	}
	tags := map[string]string{
		tagKeyIsBaseBackupMeta: "true",
		tagKeyBackupID:         cur,
		tagKeyExpiry:           expt,
	}

	backupName, err := newBackupName(s.prefix, "basebackup")
	if err != nil {
		return 0, err
	}

	n, err := upMerge.Upload(ctx, backupName+metaFileSuffix, newGzipReader(r, s.compressionLevel), tags)
	if err != nil {
		return 0, fmt.Errorf("could not upload additional backup files: %w", err)
	}

	if err = <-errc; err != nil {
		logrus.Errorf("pg_wal: error while sending additional backup files to object storage: %v", err)
		return 0, fmt.Errorf("could not upload additional backup files: %w", err)
	}

	return n, nil
}

// walkDirNonRecursive walks a given directory without recursing into
// subdirectories.
func walkDirNonRecursive(dirPath string, walkFunc filepath.WalkFunc) error {
	return filepath.Walk(dirPath, walkFuncDirNonRecursive(filepath.Base(dirPath), walkFunc))
}

func walkFuncDirNonRecursive(dirName string, walkFunc filepath.WalkFunc) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("error accessing path %q: %w", path, err)
		}
		if info.IsDir() && info.Name() != dirName {
			logrus.Debugf("skipping dir %q", info.Name())
			return filepath.SkipDir
		}
		return walkFunc(path, info, err)
	}
}

// getNewDump retrieves a new backup from the data base container and uploads it
// directly to object storage while compressing the stream in transit.
func (s *server) getNewDump(ctx context.Context, retry bool, upMerge ...UploadMerger) (*objectInfo, error) {
	// Make sure only one getNewDump() is active at a time.
	switch s.commandSem.TryAcquire(1) {
	case true:
		defer func() {
			// After we're done release the lock.
			s.commandSem.Release(1)
		}()
	default:
		// In case we are busy, respond with 429.
		logrus.Warnf("getNewDump: too many requests, throttling")
		return nil, errBusy
	}

	// Only start a new backup id, if the dump operation is retried.
	startNewBackupID := false
	if retry {
		startNewBackupID = true
	}

	// Prepare the storage (i.e. upload merger) which should be used in the dump
	// call.
	storage := UploadMerger(s.storage)
	switch len(upMerge) {
	case 0:
		// Nothing to do. Just use the server's storage.
	case 1:
		// Use the given (UploadMerger) storage.
		storage = upMerge[0]
	default:
		// Wrong input. Just use the server's storage.
		logrus.Errorf("getNewDump() was given %d upload-mergers, expected 0 or 1: using storage defined for server", len(upMerge))
	}

	// Prepare the (exponential) backoff policy and the info object.
	bo := backoff.WithContext(backoff.NewExponentialBackOff(), ctx)
	var info *objectInfo

	// Retry dump until successful (or permanently backoffed).
	err := backoff.Retry(func() error {
		var err error
		info, err = s.dump(ctx, startNewBackupID, storage)
		if err != nil && retry {
			logrus.Warnf("error creating dump, retrying ... %v", err)
			return err
		}
		return backoff.Permanent(err)
	}, bo)

	// Return the results.
	if err != nil {
		logrus.Errorf("error getting new dump: %s", err)
		return nil, err
	}
	return info, nil
}

func (s *server) dump(ctx context.Context, startNewBackupID bool, upMerge UploadMerger) (info *objectInfo, err error) {
	timer := prometheus.NewTimer(metricSummary)
	defer func() {
		if err != nil {
			// Do not process duration in case of an error.
			return
		}
		duration := timer.ObserveDuration()
		if info != nil {
			speed := rateString(duration, info.Size)
			logrus.Infof("uploaded %s in %s (%s), name = %s", ByteCountBinary(info.Size), duration, speed, info.Name)
			return
		}
		logrus.Infof("job processed in %s", duration)
	}()

	defer func() {
		if err != nil {
			metricErrors.Inc()
		}
	}()

	if s.usePgWAL {
		// We use the pg_wal method. We send postgres a command to checkpoint
		// and then upload all its files directly to object storage. In case we
		// have output from the job and debug logging is on, capture this.

		// After the pg_basebackup is done, copy over the files into the
		// archive dir for collection.
		logrus.Infof("uploading base backup ...")
		cfg := PostgresStorageConfig{
			Address:    s.pgHostName,
			DBName:     s.pgDbName,
			Username:   s.pgUserName,
			Password:   s.pgPassword,
			SecureMode: false,
		}

		// Open a new db connection.
		db, err := openDbConnection(ctx, cfg)
		if err != nil {
			return nil, err
		}
		defer db.Close()

		// Signal to the db that a new backup is started. Note: Here we receive
		// a return value which contains the name of the first wal file that is
		// required to restore the backup that was just started. We will use
		// that value later on to determine which wal segment files should be
		// included into the backup tarball.
		conn, firstWalFileName, err := startPgBackup(ctx, db, true)
		if err != nil {
			return nil, fmt.Errorf("could not start pg backup: %w", err)
		}
		defer conn.Close()

		// Write new backup id to a local file.
		cur := currentBackupID.Get()
		if startNewBackupID {
			cur = currentBackupID.Set(MustNewULID())
			if err := writeBackupID(s.pgDataPath, cur); err != nil {
				return nil, fmt.Errorf("could not write backup id to local file: %w", err)
			}
			if !s.readOnly {
				if s.backupMetas == nil {
					return nil, fmt.Errorf("backup meta file list cannot be nil")
				}
				s.backupMetas.Add(backupMeta{
					ulid:            cur,
					firstWALSegment: firstWalFileName,
				})
			}
			recordBackupID(cur)
		}

		// Get a new backup name.
		backupName, err := newBackupName(s.prefix, basebackupSuffix)
		if err != nil {
			return nil, err
		}

		// Fetch lifecycler.
		lifecycler, err := s.storage.Lifecycle()
		if err != nil {
			return nil, fmt.Errorf("could not get lifecycler: %w", err)
		}
		exp, ok := lifecycler.(ExpirationTimer)
		if !ok {
			return nil, fmt.Errorf("lifecycler not supported %T", lifecycler)
		}

		// Initialize channels and the object info object that is going to be
		// returned.
		list := make(chan finfo)
		errc := make(chan error, 1) // Caution: If you increase this, also think about returning all of them somehow.
		var info = new(objectInfo)

		// Prepare tags.
		expt, err := expiryString(exp, cur)
		if err != nil {
			return nil, err
		}
		tags := map[string]string{
			tagKeyIsBaseBackup: "true",
			tagKeyBackupID:     cur,
			tagKeyExpiry:       expt,
		}

		syncCtx, cancelSyncCtx := context.WithCancel(ctx)
		go func() {
			// Sync the pg data path directory inside a go-routine. Note that we
			// don't close the list channel in this routine. This is because we
			// want to sync further files into the same tarball (which is
			// created by doSyncFiles) later on.
			var err error
			defer close(errc)
			defer cancelSyncCtx()
			logrus.Tracef("syncing directory %s ...", s.pgDataPath)
			info, err = doSyncFiles(
				syncCtx,
				list,
				s.pgDataPath,
				backupName, // .tar.gz will be added automatically later.
				upMerge,
				s.compressionLevel,
				false, // Do not delete anything after upload.
				tags,
				s.uploadReadLimit,
			)
			if err != nil {
				errc <- err
				return
			}
		}()

		// Walk the pg data path directory and send all relevant files to the
		// list channel, where they will be synced on reception.
		err = filepath.Walk(s.pgDataPath, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				if os.IsNotExist(err) {
					// We can ignore this event. See
					// https://www.postgresql.org/message-id/55759B98.3090609%40aklaver.com
					// for further discussion and the Postgres docs about
					// copying files while the database is running:
					// https://www.postgresql.org/docs/11/continuous-archiving.html#BACKUP-LOWLEVEL-BASE-BACKUP-DATA
					logrus.Warnf("file %s no longer exists: %v", path, err)
					return nil
				}
				return fmt.Errorf("error accessing path %q: %w", path, err)
			}
			if filepath.Clean(path) == filepath.Clean(s.pgDataPath) {
				return nil // Skip our own directory.
			}
			// Skip the backup ID marker file.
			if filepath.Base(path) == backupIdFileName {
				return nil
			}
			// Skip files that don't need to be included in a basebackup.
			rel, err := filepath.Rel(s.pgDataPath, path)
			if err != nil {
				return fmt.Errorf("error getting relative path of %s to %s: %w", path, s.pgDataPath, err)
			}
			if !includeInBasebackup(rel) {
				return nil
			}
			select {
			case list <- finfo{path: path, info: info}:
			case <-syncCtx.Done():
				return syncCtx.Err()
			}
			return nil
		})
		if err != nil {
			return nil, fmt.Errorf("error walking the path %q: %w", s.pgDataPath, err)
		}

		// Close the db conncetion. The close method also signals to the db that
		// the backup is done now. This will allow to determine the last wal
		// file that is required to restore the backup below.
		if err := conn.Close(); err != nil {
			return nil, fmt.Errorf("could not close backup cleanly: %w", err)
		}

		// Upload the metadata which is required to restore the backup to a
		// separate storage object.
		n, err := s.uploadMeta(ctx, conn.LabelFile(), conn.MapFile(), exp, upMerge)
		if err != nil {
			return nil, fmt.Errorf("could not upload meta file: %w", err)
		}

		// Determine the last wal file that is required to restore the backup.
		lastWalFileName := conn.LastRequiredFile()

		// Define the walk function that decides whether we need to backup a
		// certain pg wal file.
		var walkFunc = func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return fmt.Errorf("error accessing path %q: %w", path, err)
			}
			base := filepath.Base(path)
			if base < firstWalFileName {
				return nil
			}
			if base > lastWalFileName {
				return nil
			}
			targetPath := filepath.Join(pgWalDirectory, base)
			select {
			case list <- finfo{path: path, info: info, targetPath: targetPath}:
			case <-ctx.Done():
				return ctx.Err()
			}
			return nil
		}

		// Walk the pg wal directory and upload all pg_wal files which were
		// created during the base backup process.
		if err = walkDirNonRecursive(filepath.Join(s.pgDataPath, pgWalDirectory), walkFunc); err != nil {
			return nil, fmt.Errorf("error walking %s (non-recursively) after basebackup: %w", pgWalDirectory, err)
		}

		// If wal logs are archived *between* pg_start_backup and
		// pg_stop_backup, they are no longer located in the pg wal directory.
		// Thus we need to include them from the archive directory here.
		if err = walkDirNonRecursive(s.syncDirPath, walkFunc); err != nil {
			return nil, fmt.Errorf("error walking %s (non-recursively) after basebackup: %w", s.syncDirPath, err)
		}

		// Close the channel that recieves files which must be synced into the
		// backup tarball and wait for possible errors returned by the syncing
		// routine (or until the error channel is closed without error).
		close(list)
		if err := <-errc; err != nil {
			return nil, err
		}

		info.Size += n
		logrus.Infof("base backup complete: %s (%s)", info.Name, ByteCountBinary(info.Size))

		logrus.Debugf("first wal file name is %s", firstWalFileName)
		return info, nil
	}

	// Otherwise, execute the command and capture its output from stdout which
	// is then uploaded to object storage.
	r, w := io.Pipe()
	errc := make(chan error, 3) // We could have 3 errors at a time in the chan.

	go func() {
		defer func() {
			if err := w.Close(); err != nil {
				errc <- err
			}
			close(errc)
		}()
		logrus.Infof("dumping now")
		if err := Dump(ctx, w, s.command, s.args...); err != nil {
			errc <- err
			return
		}
	}()

	objectName := fmt.Sprintf("%s%s.gz", s.prefix, time.Now().Format(timeFormat))
	if s.usePgWAL {
		objectName, err = newBackupName(s.prefix, "")
		if err != nil {
			return nil, fmt.Errorf("could not get new backup name: %w", err)
		}
		objectName = objectName + ".gz"
	}

	var n int64
	n, err = upMerge.Upload(ctx, objectName, newGzipReader(r, s.compressionLevel), nil)
	if err != nil {
		return nil, fmt.Errorf("error uploading backup: %w", err)
	}

	if err = <-errc; err != nil {
		// Error out quickly, on first error.
		logrus.Debugf("error while capturing dump: %s", err)
		return nil, fmt.Errorf("received error during/after upload: %w", err)
	}

	info = &objectInfo{
		Name: objectName,
		Size: n,
	}

	return info, nil
}

// handleBundle prepares and replies with a list of objects forming a basebackup
// + txlogs at least up to the point in time given.
func (s *server) handleBundle() http.HandlerFunc {
	sem := make(chan struct{}, 1) // Allow this many requests at the same time.
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		select {
		case sem <- struct{}{}:
			defer func() { <-sem }() // Release lock when we're done.
		case <-ctx.Done():
			logrus.Infof("prepare request cancelled: %v", ctx.Err())
			sendError(rw, fmt.Errorf("request cancelled"), http.StatusNoContent)
			return
		default:
			logrus.Warnf("too many requests: %v", r.RequestURI)
			sendError(rw, fmt.Errorf("too many requests, try again later"), http.StatusTooManyRequests)
			return
		}
		vars := mux.Vars(r)
		var target time.Time
		target, err := parseTimeOrNow(vars["pointInTime"])
		if err != nil {
			logrus.Warnf("could not parse input time %s: %v", target, err)
			sendError(rw, err, http.StatusBadRequest)
			return
		}
		// Allow for a query to make sure we retrieve a set of tx logs which
		// belong to the correct base backup.
		if err := r.ParseForm(); err != nil {
			logrus.Errorf("form: %v", err)
			sendError(rw, err, http.StatusBadRequest)
			return
		}
		var matchers tags.Matchers
		if _, ok := r.Form["query"]; ok {
			query := r.FormValue("query")
			matchers, err = tags.ParseTagMatcher(query)
			if err != nil {
				logrus.Errorf("query parse error: %v, original query: %s", err, query)
				sendError(rw, fmt.Errorf("query parse error: %w, original query: %s", err, query), http.StatusBadRequest)
				return
			}
			logrus.Debugf("matching against %s", matchers.String())
		}
		// Get a list of all the backups.
		list, err := s.storage.List(ctx)
		if err != nil {
			logrus.Errorf("could not get list of backups: %v", err)
			sendError(rw, fmt.Errorf(""), http.StatusInternalServerError)
			return
		}
		// https://github.com/golang/go/wiki/SliceTricks#filter-in-place
		n := 0
		for _, v := range list {
			// We add the name of the object as a meta tag called `__name` for
			// matching.
			var tags = make(map[string]string)
			for k, v := range v.Tags {
				tags[k] = v
			}
			tags["__name"] = v.Key
			// Filter by the query we got (or don't filter at all).
			if matchers.Match(tags) {
				list[n] = v
				n++
			}
		}
		list = list[:n]
		// Select all backup items that belong to the section of the target
		// time.
		backups, err := byULID(list).Section(target)
		if err != nil {
			logrus.Errorf("could not get backup items that belong to the section of target time %s: %v", target, err)
			if errors.Is(err, errNotFound) {
				sendError(rw, fmt.Errorf("no backup items found that belong to the section of target time %s", target), http.StatusNotFound)
				return
			}
			sendError(rw, err, http.StatusInternalServerError)
			return
		}
		if len(backups) < 1 {
			logrus.Debugf("no backups found for bundle request")
			sendError(rw, fmt.Errorf("no backups found"), http.StatusNotFound)
			return
		}

		// First let's find a convenient name for the download. Using ULIDs we
		// prevent collisions effectively.
		name := MustULID(target)
		name = name + "-bundle"

		// Send back to client if no streaming transfer has been requested.
		v := r.URL.Query()
		if _, ok := v["download"]; !ok {
			rw.Header().Set("Content-type", "application/json")

			var buf bytes.Buffer
			mode := v.Get("mode")
			if mode == "restore" {
				meta := PointInTimeRestore{
					BackupList:  backups,
					PointInTime: target,
				}
				// Send the meta information in case the client requested it.
				if err := json.NewEncoder(&buf).Encode(meta); err != nil {
					logrus.Errorf("error marshaling restore metadata file: %v", err)
					sendError(rw, fmt.Errorf(""), http.StatusInternalServerError)
					return
				}
			} else {
				if err := json.NewEncoder(&buf).Encode(backups); err != nil {
					logrus.Errorf("error marshaling backup list to JSON: %v", err)
					sendError(rw, fmt.Errorf(""), http.StatusInternalServerError)
					return
				}
			}

			if _, ok := v["upload"]; ok {
				// In this case we want to upload the metadata bundle to the
				// object storage and return a presigned link we can reuse later
				// on to restore.
				name = fmt.Sprintf("%s.json", strings.Join([]string{s.prefix, name}, "-"))
				// Prepend the storage prefix to the bundle object name. Note:
				// The storage prefix is an empty string for s3 and gcs
				// storages, but a non-empty string for multisource storages.
				storName, err := (*backupItemList)(&backups).storagePrefix()
				if err != nil {
					logrus.Errorf("could not infer storage prefix from backup item list: %v", err)
					sendError(rw, fmt.Errorf(""), http.StatusInternalServerError)
					return
				}
				name = storName + name
				if _, err := s.storage.Upload(ctx, name, &buf, map[string]string{
					"is_bundle": "true",
				}); err != nil {
					logrus.Errorf("error uploading backup bundle metadata to object storage: %v", err)
					sendError(rw, fmt.Errorf(""), http.StatusInternalServerError)
					return
				}
				link, err := s.storage.Link(ctx, name)
				if err != nil {
					logrus.Errorf("error creating download link for bundle metadata: %v", err)
					sendError(rw, fmt.Errorf(""), http.StatusInternalServerError)
					return
				}
				data := struct {
					Link string `json:"link"`
				}{
					Link: link,
				}
				buf.Reset()
				if err := json.NewEncoder(&buf).Encode(data); err != nil {
					logrus.Errorf("error marshaling backup list to JSON: %v", err)
					sendError(rw, fmt.Errorf(""), http.StatusInternalServerError)
					return
				}
			}

			// As a default, simply send the list back to the requester.
			if _, err := io.Copy(rw, &buf); err != nil {
				logrus.Error(err)
			}
			return
		}

		// Else download each tar file from source and send it to the client as
		// a single tar stream.
		// Then inform the HTTP client about it.
		rw.Header().Add("Content-disposition", fmt.Sprintf(`attachment; filename="%s.%s"`, name, fileExtensionTarGz))
		rw.Header().Add("Content-type", "application/x-tar")
		gzrw := gzip.NewWriter(rw)
		defer gzrw.Close()
		if err := streamToTar(ctx, backups, s.txLogPath, s.storage, gzrw); err != nil {
			logrus.Errorf("error writing to http client: %v", err)
			return
		}
	}
}

func parseTimeOrNow(target string) (time.Time, error) {
	if target == "" || target == "now" {
		return time.Now(), nil
	}
	t, err := time.Parse(time.RFC3339, target)
	if err != nil {
		return time.Time{}, err
	}
	return t, nil
}

// Handle cache resets.
func (s *server) handleCacheClearing() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		storage, ok := s.storage.(interface{ ClearCache() })
		if !ok {
			logrus.Debug("object storage implementation does not support ClearCache()")
			return
		}
		storage.ClearCache()
		logrus.Info("cleared list cache successfully")
	}
}
