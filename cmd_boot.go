package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mironet/magnolia-backup/api"
)

func initTLSConfig(caCertFile, certFile, keyFile string) (*tls.Config, error) {
	caCert, err := os.ReadFile(caCertFile)
	if err != nil {
		return nil, fmt.Errorf("could not read caCert file: %w", err)
	}
	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(caCert)

	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, fmt.Errorf("could not load client certificate: %w", err)
	}
	cfg := &tls.Config{
		RootCAs:      pool,
		Certificates: []tls.Certificate{cert},
		MinVersion:   tls.VersionTLS12,
	}
	return cfg, nil
}

var bootCmd = &cobra.Command{
	Use:   "boot",
	Short: "Lock and fetch content from other public instances.",
	RunE: func(cmd *cobra.Command, _ []string) error {
		vip := vipers["boot"]
		hostname := vip.GetString("hostname")

		// We require TLS.
		tlsCfg, err := initTLSConfig(
			vip.GetString("tls-ca-cert"),
			vip.GetString("tls-cert"),
			vip.GetString("tls-key"),
		)
		if err != nil {
			return fmt.Errorf("error loading TLS config: %w", err)
		}
		httpClient := &http.Client{
			Transport: &http.Transport{
				Dial: (&net.Dialer{
					Timeout:   30 * time.Second,
					KeepAlive: 30 * time.Second,
				}).Dial,
				TLSHandshakeTimeout:   30 * time.Second,
				ResponseHeaderTimeout: 15 * time.Minute,
				ExpectContinueTimeout: 1 * time.Second,
				IdleConnTimeout:       15 * time.Minute, // 90s is the default.
				TLSClientConfig:       tlsCfg,
			},
		}
		cli, err := api.New(
			api.WithHTTPClient(httpClient),
		)
		if err != nil {
			return fmt.Errorf("error creating HTTP client: %w", err)
		}

		var dataSourceURL *url.URL
		if v := vip.GetString("datasource"); v != "" {
			dataSourceURL, err = url.ParseRequestURI(v)
			if err != nil {
				return fmt.Errorf("could not parse url --datasource: %w", err)
			}
		}

		boot := &bootProcess{
			ctx:            context.Background(),
			hostname:       hostname,
			cli:            cli,
			markerFilePath: filepath.Clean(vip.GetString("data-dir") + string(filepath.Separator) + vip.GetString("markerfile")),
			serviceName:    vip.GetString("service-name"),
			dataDir:        vip.GetString("data-dir"),
			dataSourceURL:  dataSourceURL,
		}
		for state := stateInit; state != nil; {
			state = state(boot)
		}
		return boot.Err
	},
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["boot"] = v

	flags := bootCmd.Flags()

	// General flags.
	flags.String("hostname", "", "Override hostname, used for testing.")
	flags.String("data-dir", "", "Where to extract the source data to (data base data directory).")
	flags.String("markerfile", ".mgnl-backup-bootstrapped", "Path to markerfile used for checking if we already ran.")

	// Flags when syncing content from another instance.
	flags.String("service-name", "", "The other instance's data source service name.")

	// "Restore from configured source" mode flags.
	flags.String("datasource", "", "URL to metadata (bundle) file (in JSON format) for txlog restoration.")

	// TLS flags. This is needed for the /download endpoint (mTLS, client
	// authentication) in case we sync content from another instance.
	flags.String("tls-ca-cert", "/opt/tls/ca.crt", "Path to CA cert PEM file.")
	flags.String("tls-cert", "/opt/tls/tls.crt", "Path to server certificate PEM file.")
	flags.String("tls-key", "/opt/tls/tls.key", "Path to private key PEM file (unencrypted).")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(timeCmdMiddleware(bootCmd))
}
