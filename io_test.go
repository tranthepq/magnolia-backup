package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"testing"

	"github.com/klauspost/compress/gzip"
)

type errReader struct{}

func (e errReader) Read(b []byte) (int, error) {
	return 0, fmt.Errorf("reader always returns error")
}

func Test_newGzipReader(t *testing.T) {
	type wantFunc func(in io.Reader) error
	type args struct {
		in    io.Reader
		level int
	}
	tests := []struct {
		name string
		args args
		want wantFunc
	}{
		{
			name: "simple gzip",
			args: args{
				in:    bytes.NewBufferString(strings.Repeat("abc", 100)),
				level: gzip.BestSpeed,
			},
			want: func(in io.Reader) error {
				n, err := io.Copy(io.Discard, in)
				if err != nil {
					return err
				}
				if n >= 100 {
					return fmt.Errorf("read %d bytes, want less than %d", n, 100)
				}
				return nil
			},
		},
		{
			name: "error reader",
			args: args{
				in:    errReader{},
				level: gzip.BestCompression,
			},
			want: func(in io.Reader) error {
				_, err := io.Copy(io.Discard, in)
				if err != nil {
					t.Log("expected error:", err)
					return nil
				}
				return fmt.Errorf("got no error, but wanted one")
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gz := newGzipReader(tt.args.in, tt.args.level)
			if tt.want != nil {
				if err := tt.want(gz); err != nil {
					t.Errorf("%s", err)
				}
			}
		})
	}
}

func Test_isEmpty(t *testing.T) {
	emptyDir, err := os.MkdirTemp("", "magnolia")
	if err != nil {
		t.Error(err)
		return
	}
	defer os.RemoveAll(emptyDir)

	nonEmptyDir, err := os.MkdirTemp("", "magnolia")
	if err != nil {
		t.Error(err)
		return
	}
	defer os.RemoveAll(nonEmptyDir)

	_, err = os.MkdirTemp(nonEmptyDir, "file")
	if err != nil {
		t.Error(err)
		return
	}

	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "is empty",
			args: args{
				name: emptyDir,
			},
			want: true,
		},
		{
			name: "not empty",
			args: args{
				name: nonEmptyDir,
			},
			want: false,
		},
		{
			name: "non-existing",
			args: args{
				name: "/this/directory/probably/does/not/exist",
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := isEmpty(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("isEmpty() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("isEmpty() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_divideReader(t *testing.T) {
	testBytes := []byte(longTestString)
	limit := 5
	divideReader := newDivideReader(&mockReader{
		bytes: testBytes,
	}, limit)
	numParts := ceilFloat64ToInt(t, float64(len(testBytes))/float64(limit))

	for i := 0; i < numParts; i++ {
		if !divideReader.hasNext() {
			t.Fatalf("divide reader should have part %d, but hasNext() returns false", i+1)
		}
		b, err := io.ReadAll(divideReader.next())
		if err != nil {
			t.Fatalf("could not read from divide reader part %d: %v", i+1, err)
		}
		if i != numParts-1 && len(b) != limit {
			t.Fatalf("part %d (which is not the last part) has only size %d, but should have size %d", i+1, len(b), limit)
		}
		s1 := string(b)
		s2 := longTestString[i*5:]
		if i != numParts-1 {
			s2 = longTestString[i*5 : (i+1)*5]
		}
		if s1 != s2 {
			t.Fatalf("expected part %d to contain string %q, but have %q", i+1, s2, s1)
		}
	}
}

func Test_newRange(t *testing.T) {
	type args struct {
		total     int64
		chunkSize int64
	}
	tests := []struct {
		name string
		args args
		want []byteRange
	}{
		{
			name: "simple range of 2",
			args: args{total: 2 * Mi, chunkSize: 1 * Mi},
			want: []byteRange{
				{
					offset: 0,
					length: 1 * Mi,
				},
				{
					offset: 1 * Mi,
					length: 1 * Mi,
				},
			},
		},
		{
			name: "zero size",
			args: args{total: 0 * Mi, chunkSize: 1 * Mi},
			want: []byteRange{},
		},
		// TODO: do test and recover from panic, i.e. also test the panic :)
		// {
		// 	name: "negative size",
		// 	args: args{total: -1 * Mi, chunkSize: 1 * Mi},
		// 	want: []byteRange{},
		// },
		// {
		// 	name: "zero chunk size",
		// 	args: args{total: 2 * Mi, chunkSize: 0},
		// 	want: []byteRange{},
		// },
		{
			name: "remainder only",
			args: args{total: 744 * Ki, chunkSize: 1 * Mi},
			want: []byteRange{
				{
					offset: 0,
					length: 744 * Ki,
				},
			},
		},
		{
			name: "range with remainder",
			args: args{total: 2754 * Ki, chunkSize: 1 * Mi},
			want: []byteRange{
				{
					offset: 0,
					length: 1 * Mi,
				},
				{
					offset: 1 * Mi,
					length: 1 * Mi,
				},
				{
					offset: 2 * Mi,
					length: (2754 - 2048) * Ki,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newRange(tt.args.total, tt.args.chunkSize); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newRange() = %v, want %v", got, tt.want)
			}
		})
	}
}
