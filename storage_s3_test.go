package main

import (
	"testing"
)

func Test_fileExtensionFromObjKey(t *testing.T) {
	type args struct {
		k string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "without prefix, without part number",
			args: args{
				k: "test.part",
			},
			want:    "part",
			wantErr: false,
		},
		{
			name: "without prefix, with part number",
			args: args{
				k: "test.part12m",
			},
			want:    "part12m",
			wantErr: false,
		},
		{
			name: "with prefix, without part number",
			args: args{
				k: "my/prefix/test.part",
			},
			want:    "part",
			wantErr: false,
		},
		{
			name: "with prefix, with part number",
			args: args{
				k: "my/prefix/test.part12",
			},
			want:    "part12",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := fileExtensionFromObjKey(tt.args.k)
			if (err != nil) != tt.wantErr {
				t.Errorf("fileExtensionFromObjKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("fileExtensionFromObjKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_partName(t *testing.T) {
	type args struct {
		prefix string
		uid    string
		i      int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "empty prefix",
			args: args{
				prefix: "",
				uid:    "test",
				i:      42,
			},
			want: "parts/test_42.part",
		},
		{
			name: "flat prefix",
			args: args{
				prefix: "prefix",
				uid:    "test",
				i:      42,
			},
			want: "parts/prefix/test_42.part",
		},
		{
			name: "hierarchical prefix",
			args: args{
				prefix: "dir/prefix",
				uid:    "test",
				i:      42,
			},
			want: "dir/parts/prefix/test_42.part",
		},
		{
			name: "hierarchical prefix",
			args: args{
				prefix: "a/b/c",
				uid:    "test",
				i:      42,
			},
			want: "a/b/parts/c/test_42.part",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := partName(tt.args.prefix, tt.args.uid, tt.args.i); got != tt.want {
				t.Errorf("partName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_partPrefix(t *testing.T) {
	tests := []struct {
		name   string
		prefix string
		want   string
	}{
		{
			name:   "empty prefix",
			prefix: "",
			want:   "parts",
		},
		{
			name:   "flat prefix",
			prefix: "prefix",
			want:   "parts/prefix",
		},
		{
			name:   "hierarchical prefix",
			prefix: "dir/prefix",
			want:   "dir/parts/prefix",
		},
		{
			name:   "hierarchical prefix",
			prefix: "a/b/c",
			want:   "a/b/parts/c",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := partPrefix(tt.prefix); got != tt.want {
				t.Errorf("partPrefix() = %v, want %v", got, tt.want)
			}
		})
	}
}
