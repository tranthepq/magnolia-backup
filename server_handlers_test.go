package main

import (
	"fmt"
	"io/fs"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"golang.org/x/sync/semaphore"
)

func Test_handleDownload_of_metafile(t *testing.T) {
	prefixTestFileName := strings.ReplaceAll(fmt.Sprintf("prefix/test_basebackup_id/basebackup/second_id%s", metaFileSuffix), "/", httpDownloaderFileNameSeparator)
	testFileName := strings.ReplaceAll(fmt.Sprintf("test_basebackup_id2/basebackup/second_id2%s", metaFileSuffix), "/", httpDownloaderFileNameSeparator)
	s := &server{
		bufferedMetaResps: &metaResponseList{
			l: []*metaResponseBuffer{
				{
					header: http.Header{
						headerKeyContentDisposition: []string{fmt.Sprintf("%s%s", headerValueContentDispositionPrefix, prefixTestFileName)},
					},
					content: []byte("test1"),
				},
				{
					header: http.Header{
						headerKeyContentDisposition: []string{fmt.Sprintf("%s%s", headerValueContentDispositionPrefix, testFileName)},
					},
					content: []byte("test2"),
				},
			},
		},
		commandSem: semaphore.NewWeighted(1),
	}

	handle := s.handleDownload("", "", nil)

	type args struct {
		backupID    string
		withoutForm bool
	}
	tests := []struct {
		name       string
		args       args
		want       string
		wantHeader string
	}{
		{
			name:       "test1",
			args:       args{backupID: "test_basebackup_id"},
			want:       "test1",
			wantHeader: `attachment; filename=prefix_test_basebackup_id_basebackup_second_id-meta.tar.gz`,
		},
		{
			name:       "test2",
			args:       args{backupID: "test_basebackup_id2"},
			want:       "test2",
			wantHeader: `attachment; filename=test_basebackup_id2_basebackup_second_id2-meta.tar.gz`,
		},
		{
			name: "test no item found",
			args: args{backupID: "blabla"},
			want: "{\"status\":\"Internal Server Error\",\"status_code\":500,\"msg\":\"no item found\"}",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			query := make(url.Values)
			query.Add("meta", tt.args.backupID)
			raw := query.Encode()
			if tt.args.withoutForm {
				raw = ""
			}
			r := &http.Request{
				Method: http.MethodGet,
				URL: &url.URL{
					Path:     "/download",
					RawQuery: raw,
				},
			}
			b := &metaResponseBuffer{}
			handle(b, r)

			if !strings.EqualFold(strings.TrimSpace(string(b.content)), tt.want) {
				t.Errorf("expected content %s, but got %s", tt.want, string(b.content))
			}
			if tt.wantHeader != "" {
				hdr := b.header.Get(headerKeyContentDisposition)
				if hdr != tt.wantHeader {
					t.Errorf("unexpected header, got = %s, want = %s", hdr, tt.wantHeader)
				}
			}
		})
	}
}

func Test_walkDirNonRecursive(t *testing.T) {
	// Get project's directory.
	path, err := os.Getwd()
	if err != nil {
		t.Fatal("could not get rooted path name of current directory")
	}
	// Name of the testdata subdirectory.
	testdataDir := "testdata"

	errorOnNonTestdata := func(path string, info fs.FileInfo, err error) error {
		// Log every file name we see.
		t.Logf("seeing file %q", path)

		switch info.IsDir() {
		// If we see a directory ...
		case true:
			// ... which is not the testdata directory, something is wrong.
			if info.Name() != testdataDir {
				return fmt.Errorf("unexpected directory %q came through", path)
			}
		// If we see a file ...
		case false:
			// ... in a directory which is not the testdata directory, something
			// is wrong.
			if filepath.Base(filepath.Dir(path)) != testdataDir {
				return fmt.Errorf("unexpected file %q came through", path)
			}
		}

		return nil
	}

	t.Logf("walking path %q", path)
	err = walkDirNonRecursive(filepath.Join(path, testdataDir), errorOnNonTestdata)
	if err != nil {
		t.Fatal(err)
	}
}
