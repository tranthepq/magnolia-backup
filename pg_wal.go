package main

import (
	"context"
	"database/sql"
	"fmt"
	"sync"
)

type connectionCloser struct {
	ctx  context.Context // When this closes, cancel everything.
	conn *sql.Conn       // An open sql connection.
	// These byte slices will be filled with all the fields the closing command
	// returns. Postgres for example needs this information to be stored in
	// files for a restore to happen correctly. See
	// https://www.postgresql.org/docs/12/continuous-archiving.html#BACKUP-LOWLEVEL-BASE-BACKUP
	// for details.
	lastRequiredFile string
	labelfile        []byte
	mapfile          []byte
	sync.Once
}

const (
	pgWalFileQuery = `SELECT * FROM pg_walfile_name($1)`
)

// This needs to happen on the same connection. This is why it is encapsulated
// here.
func (c *connectionCloser) Close() error {
	var err error
	c.Do(func() {
		const query = `SELECT * FROM pg_stop_backup(false, true);` // Hardcoded for now.
		var lsn string
		defer c.conn.Close()
		row := c.conn.QueryRowContext(c.ctx, query)
		err = row.Scan(&lsn, &c.labelfile, &c.mapfile)
		if err != nil {
			err = fmt.Errorf("error calling pg_stop_backup: %w", err)
			return
		}
		err = c.conn.QueryRowContext(c.ctx, pgWalFileQuery, lsn).Scan(&c.lastRequiredFile)
		if err != nil {
			err = fmt.Errorf("error calling pg_walfile_name(%s): %w", lsn, err)
			return
		}
	})
	return err
}

func (c *connectionCloser) LastRequiredFile() string {
	return c.lastRequiredFile
}

func (c *connectionCloser) LabelFile() []byte {
	return c.labelfile
}

func (c *connectionCloser) MapFile() []byte {
	return c.mapfile
}

type MapAndLabelFiler interface {
	LabelFile() []byte
	MapFile() []byte
}

const (
	// The filenames for the label and map file are fixed, see here for details:
	// https://www.postgresql.org/docs/12/continuous-archiving.html#BACKUP-LOWLEVEL-BASE-BACKUP-NONEXCLUSIVE
	labelFileName = "backup_label"
	mapFileName   = "tablespace_map"
)

// Starts a backup and returns an opaque io.Closer to close the connection as
// soon as the backup has been done completely.
func startPgBackup(ctx context.Context, db *sql.DB, immediate bool) (*connectionCloser, string, error) {
	const label = "startPgBackup"
	query := fmt.Sprintf(`SELECT pg_start_backup('%s', %t, %t);`, label, immediate, false)
	conn, err := db.Conn(ctx)
	if err != nil {
		return nil, "", fmt.Errorf("could not get a db connection: %w", err)
	}

	var lsn string
	row := conn.QueryRowContext(ctx, query)
	if err := row.Scan(&lsn); err != nil {
		return nil, "", fmt.Errorf("error starting pg backup: %w", err)
	}
	var walFileName string
	if err := conn.QueryRowContext(ctx, pgWalFileQuery, lsn).Scan(&walFileName); err != nil {
		return nil, "", fmt.Errorf("could not find wal file name for lsn %s: %w", lsn, err)
	}

	closer := &connectionCloser{
		ctx:  ctx,
		conn: conn,
	}
	return closer, walFileName, nil
}
